package com.smart.school.parent.adaptersP;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.parent.activityP.ImportantLinksP;
import com.smart.school.teacher.responsesT.ClassListResponse;
import com.smart.school.utils.Constants;

import java.util.List;


/**
 * Created by Manoj Singh Deopa.
 */

public class ClassListAdapter extends RecyclerView.Adapter<ClassListAdapter.ViewHolder> {

    private List<ClassListResponse.Datum> list;
    private Context context;

    public ClassListAdapter(List<ClassListResponse.Datum> data) {
        this.list = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.subject_list_adapter, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int i) {
        holder.subject.setText(list.get(i).className);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView subject;

        public ViewHolder(View view) {
            super(view);
            subject = view.findViewById(R.id.subject);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    context.startActivity(new Intent(context, ImportantLinksP.class).putExtra(Constants.TYPE, Constants.TYPE_STUDY_MATERIAL_GRID));
                }
            });

        }
    }
}