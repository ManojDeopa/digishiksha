package com.smart.school.parent.fragmentsP;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.smart.school.R;
import com.smart.school.parent.adaptersP.TimeTableAdapter;
import com.smart.school.parent.responsesP.TimeTableTabResponse;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.ConstantURL;
import com.smart.school.utils.Constants;
import com.smart.school.utils.LoginPreferences;
import com.smart.school.utils.ProgressD;
import com.smart.school.utils.VolleySingleton;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.smart.school.parent.fragmentsP.TimeTableTabFragment.teacherName;


/**
 * Created by Manoj Singh Deopa on 2/4/18.
 */

@SuppressLint("ValidFragment")

public class TimeTableFragP extends Fragment {

    RecyclerView recycler_view;
    private String day_name;
    private Context context;
    private View view;

    public TimeTableFragP(String day_name) {
        this.day_name = day_name;
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.timetable_fragment, container, false);

        context = getActivity();

        callApi();

        return view;

    }

    private void callApi() {
        final String appBaseUrl = LoginPreferences.getActiveInstance(context).getUserBaseUrl();
        Log.e("URL--", appBaseUrl);
        final ProgressD pd = ProgressD.show(context, context.getResources().getString(R.string.connecting), true);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, appBaseUrl + ConstantURL.APP_API,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.dismiss();
                        Log.e("Response--", response);

                        Gson gson = new Gson();
                        TimeTableTabResponse result = gson.fromJson(String.valueOf(response), TimeTableTabResponse.class);

                        if (result.result) {
                            teacherName.setText(String.format("%s : %s", result.classTeacher.get(0).key, result.classTeacher.get(0).value));
                            setList(result.timeTable);

                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                        Log.e("error--", error.toString());
                        Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Log.e("DayName--", day_name);
                Map<String, String> params = new HashMap<>();
                params.put("api_page", ConstantURL.TIME_TABLE);
                params.put("session_id", LoginPreferences.getActiveInstance(context).getSessionId());
                params.put("school_id", LoginPreferences.getActiveInstance(context).getSchoolId());
                params.put("branch_id", LoginPreferences.getActiveInstance(context).getBranchId());
                params.put("student_id", LoginPreferences.getActiveInstance(context).getId());
                params.put("class_id", LoginPreferences.getActiveInstance(context).getClassId());
                params.put("section_id", LoginPreferences.getActiveInstance(context).getSectionId());
                params.put("day_name", day_name);
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }

    private void setList(List<TimeTableTabResponse.TimeTable> timeTable) {

        if (timeTable.size() == 0) {
            LinearLayout linearLayout = view.findViewById(R.id.empty_layout);
            CommonMethod.showEmpty(linearLayout);
            return;
        }

        recycler_view = view.findViewById(R.id.recycler_view);
        CommonMethod.setRecyclerView(recycler_view, context);
        recycler_view.setAdapter(new TimeTableAdapter(timeTable));
    }

}
