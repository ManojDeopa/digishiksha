package com.smart.school.parent.activityP;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.smart.school.R;
import com.smart.school.parent.adaptersP.ChildListAdapterP;
import com.smart.school.parent.fragmentsP.DashBoardP;
import com.smart.school.parent.responsesP.LoginResponseP;
import com.smart.school.teacher.adaptersT.ExpandableListAdapterT;
import com.smart.school.teacher.interfacesT.DashboardBottomItemClickListener;
import com.smart.school.teacher.responsesT.MenuModel;
import com.smart.school.utils.AppData;
import com.smart.school.utils.AppUtils;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.Constants;
import com.smart.school.utils.LoginPreferences;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ParentDashboard extends AppCompatActivity implements DashboardBottomItemClickListener {

    private static final int TIME_INTERVAL = 2000;
    public TextView tv_title;
    boolean flag = false;
    ExpandableListAdapterT expandableListAdapterT;
    ExpandableListView expandableListView;
    List<MenuModel> headerList = new ArrayList<>();
    HashMap<MenuModel, List<MenuModel>> childList = new HashMap<>();
    private Toolbar toolbar;
    private ParentDashboard context;
    private long mBackPressed;
    private DrawerLayout drawer;
    private int lastExpandedPosition = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.parent_dashboard);

        initializeViews();

        setDrawerLayout();

    }

    private void initializeViews() {
        context = ParentDashboard.this;

        TextView app_version = findViewById(R.id.app_version);
        CommonMethod.setVersionName(app_version);

        toolbar = findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        setSupportActionBar(toolbar);

        ImageView logout = findViewById(R.id.logout);
        logout.setImageResource(R.drawable.ic_dots);

        findViewById(R.id.logout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAlert();
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void setDrawerLayout() {
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.white));

        NavigationView navigationView = findViewById(R.id.nav_view);
        LinearLayout header_layout = navigationView.getHeaderView(0).findViewById(R.id.header_layout);
        header_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethod.startIntent(context, ProfileActivityP.class);
            }
        });


        TextView user_name = navigationView.getHeaderView(0).findViewById(R.id.user_name);
        ImageView user_image = navigationView.getHeaderView(0).findViewById(R.id.user_image);

        user_name.setText(LoginPreferences.getActiveInstance(context).getFullname() +
                "\n" + LoginPreferences.getActiveInstance(context).getClassSection() +
                "\n" + LoginPreferences.getActiveInstance(context).getAdmissionNo());
        String userImage = LoginPreferences.getActiveInstance(context).getUserImage();
        Picasso.get().load(userImage).placeholder(R.drawable.shool_logo).into(user_image);

        Log.e("Image---", userImage);

        expandableListView = findViewById(R.id.expandableListView);

        _callHomeFragment();

        prepareMenuData();

        populateExpandableList();

    }


    private void prepareMenuData() {

        MenuModel menuModel = new MenuModel(Constants.HOME, true, false);
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }


        List<MenuModel> childModelsList;
        MenuModel childModel;


        menuModel = new MenuModel(Constants.QUICK_ACTION, true, true);
        headerList.add(menuModel);

        childModelsList = new ArrayList<>();
        childModel = new MenuModel(Constants.TYPE_ATTENDANCE, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.TYPE_TIME_TABLE, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.TYPE_NOTICE, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.TYPE_FEES, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.TYPE_EXAM_TIMETABLE, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.TYPE_HOMEWORK, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.TYPE_EXAM_RESULT, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.TYPE_LEAVE_REQUEST, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.TYPE_CLASSTEST_RESULT, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.TYPE_LIBRARY, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.TYPE_BUS_ROUTE, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.TYPE_MY_FEEDBACK, false, false);
        childModelsList.add(childModel);


        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }


        childModelsList = new ArrayList<>();
        menuModel = new MenuModel(Constants.IMPORTANT_LINK, true, true);
        headerList.add(menuModel);

        childModel = new MenuModel(Constants.TYPE_CALLENDER, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.TYPE_ANNOUNCEMENT, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.TYPE_GALLERY, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.TYPE_STYDY_MATERIAL, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.TYPE_MY_TEACHER, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.TYPE_MY_CLASSMATES, false, false);
        childModelsList.add(childModel);


        childModel = new MenuModel(Constants.TYPE_MY_ACHIEVEMENTS, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.TYPE_SERVICE_REQUEST, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.TYPE_COMPLAINTS, false, false);
        childModelsList.add(childModel);


        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }


        childModelsList = new ArrayList<>();
        menuModel = new MenuModel(Constants.TYPE_ABOUT_SCHOOL, true, true);
        headerList.add(menuModel);

        childModel = new MenuModel(Constants.TYPE_ABOUT_SCHOOL, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.TYPE_SCHOOL_RULE_REGULATIONS, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.TYPE_STUDENT_GUIDANCE, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.TYPE_PARENT_GUIDANCE, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.TYPE_SCHOOL_STAFF, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.TYPE_CONTACT_US, false, false);
        childModelsList.add(childModel);


        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }

        menuModel = new MenuModel(Constants.SHARE, true, false);
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }

        menuModel = new MenuModel(Constants.PRIVACY_POLICY, true, false);
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }


        menuModel = new MenuModel(Constants.ABOUT, true, false);
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }

        menuModel = new MenuModel(Constants.APP_TUTORIAL, true, false);
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }

        menuModel = new MenuModel(Constants.LOG_OUT, true, false);
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }

    }


    private void populateExpandableList() {

        expandableListAdapterT = new ExpandableListAdapterT(this, headerList, childList);
        expandableListView.setAdapter(expandableListAdapterT);

        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                if (headerList.get(groupPosition).isGroup) {

                    if (!headerList.get(groupPosition).hasChildren) {
                        switch (headerList.get(groupPosition).menuName) {
                            case Constants.HOME:
                                _callHomeFragment();
                                break;
                            case Constants.LOG_OUT:
                                showLogoutDialog();
                                break;
                            case Constants.SHARE:
                                CommonMethod._shareIntent(context);
                                break;

                            case Constants.ABOUT:
                                CommonMethod.callBrowserIntent(context, Constants.DIGI_SHIKSHA_URL);
                                break;

                            case Constants.PRIVACY_POLICY:
                                CommonMethod.callBrowserIntent(context, Constants.PRIVACY_POLICY_URL);
                                break;
                        }

                        drawer.closeDrawer(GravityCompat.START);
                    }
                }

                return false;
            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                if (childList.get(headerList.get(groupPosition)) != null) {
                    MenuModel model = childList.get(headerList.get(groupPosition)).get(childPosition);
                    String type = model.menuName;

                    switch (headerList.get(groupPosition).menuName) {
                        case Constants.QUICK_ACTION:
                            startActivity(new Intent(context, QuickActionDetailsP.class).putExtra(Constants.TYPE, type));
                            break;
                        case Constants.IMPORTANT_LINK:
                            startActivity(new Intent(context, ImportantLinksP.class).putExtra(Constants.TYPE, type));
                            break;
                        case Constants.TYPE_ABOUT_SCHOOL:
                            AppData.getInstance().setfrom(Constants.TYPE_TEACHER);
                            context.startActivity(new Intent(context, AboutSchoolP.class).putExtra(Constants.TYPE, type));
                            break;
                    }

                }

                return false;
            }
        });

        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                if (lastExpandedPosition != -1
                        && groupPosition != lastExpandedPosition) {
                    expandableListView.collapseGroup(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;
            }
        });

    }


    private void _callHomeFragment() {
        tv_title.setText(String.format("%s | %s", LoginPreferences.getActiveInstance(context).getFullname(), LoginPreferences.getActiveInstance(context).getClassSection()));
        DashBoardP dashBoardFragment = new DashBoardP();
        AppUtils.setFragment(dashBoardFragment, true, ParentDashboard.this, R.id.container);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0)
                getSupportFragmentManager().popBackStackImmediate();
            else if (flag) {
                flag = false;
                _callHomeFragment();
            } else {
                if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
                    moveTaskToBack(true);
                    return;
                } else {
                    Toast.makeText(getBaseContext(), "Press Again to Exit", Toast.LENGTH_SHORT).show();
                }
                mBackPressed = System.currentTimeMillis();
            }
        }
    }


    @Override
    public void bottomItemClicked(int position) {
        flag = true;
    }

    public void logout() {
        showLogoutDialog();
    }

    private void showLogoutDialog() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

        alertDialog.setTitle(getString(R.string.sure_to_logout));

        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finishAffinity();
                LoginPreferences.getActiveInstance(context).logOut();
                startActivity(new Intent(context, ChildAccounts.class));
            }
        });

        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alertDialog.show();

    }


    private void showAlert() {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.switch_account_dialog, viewGroup, false);

        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setView(dialogView);

        final android.support.v7.app.AlertDialog alertDialog = builder.create();

        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams wmlp = alertDialog.getWindow().getAttributes();

        wmlp.gravity = Gravity.TOP | Gravity.LEFT;
        wmlp.x = 100;   //x position
        wmlp.y = 100;

        alertDialog.show();

        ArrayList<LoginResponseP.Datum> childList = getChildData();

        if (childList.size() != 0) {
            String id = childList.get(0).studentId;
            for (int i = 1; i < childList.size(); i++) {
                if (id.equals(childList.get(i).studentId)) {
                    new ChildListDatabase(context).delete(childList.get(i).id);
                    childList.remove(i);
                }
            }

            RecyclerView recycler_view = dialogView.findViewById(R.id.recycler_view);
            CommonMethod.setRecyclerView(recycler_view, context);
            recycler_view.setAdapter(new ChildListAdapterP(childList, "inside"));
        }

        dialogView.findViewById(R.id.add_account).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(context, ChildAccounts.class));
                alertDialog.dismiss();
            }
        });


        dialogView.findViewById(R.id.logout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                logout();
            }
        });

    }

    private ArrayList<LoginResponseP.Datum> getChildData() {
        ChildListDatabase mDatabase = new ChildListDatabase(context);
        ArrayList<LoginResponseP.Datum> list = new ArrayList<LoginResponseP.Datum>();
        Cursor cursor = mDatabase.getAllData();
        if (cursor.moveToFirst()) {
            do {
                list.add(new LoginResponseP.Datum(
                        cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getString(5),
                        cursor.getString(6),
                        cursor.getString(7),
                        cursor.getString(8)));
            } while (cursor.moveToNext());
        }
        return list;
    }

}
