package com.smart.school.parent.fragmentsP;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.smart.school.R;
import com.smart.school.parent.activityP.QuickActionDetailsP;
import com.smart.school.teacher.adaptersT.SubjectListAdapter;
import com.smart.school.teacher.responsesT.SubjectListResponse;
import com.smart.school.teacher.responsesT.ViewHwResponse;
import com.smart.school.utils.AppData;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.ConstantURL;
import com.smart.school.utils.Constants;
import com.smart.school.utils.LoginPreferences;
import com.smart.school.utils.ProgressD;
import com.smart.school.utils.VolleySingleton;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Manoj Singh Deopa on 2/4/18.
 */

@SuppressLint("ValidFragment")

public class ViewHWP extends Fragment {

    TextView date, subject_name;
    private Context context;
    private View view;
    private List<SubjectListResponse.Datum> subjectList = new ArrayList<>();

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.view_hw_parent, container, false);

        initViews();

        setCurrentDate();

        onclicks();

        callSubjectApi();

        return view;

    }

    private void validate() {
        if (TextUtils.isEmpty(subject_name.getText().toString().trim())) {
            Toast.makeText(context, "Please Select Subject", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(date.getText().toString().trim())) {
            Toast.makeText(context, "Please Select Date", Toast.LENGTH_SHORT).show();
        } else {
            callApi();
        }
    }

    private void initViews() {
        context = getActivity();
        date = view.findViewById(R.id.date);
        subject_name = view.findViewById(R.id.subject_name);
    }

    private void onclicks() {

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethod.showDatePicker(context, date);
            }
        });

        subject_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSubjectDialog();
            }
        });


        view.findViewById(R.id.btn_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate();
            }
        });


    }


    public void showSubjectDialog() {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.list_dialog_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;


        RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view);
        CommonMethod.setRecyclerView(recyclerView, context);


        TextView btn_submit = dialog.findViewById(R.id.btn_submit);
        TextView btn_cancel = dialog.findViewById(R.id.btn_cancel);

        TextView tvtitle = dialog.findViewById(R.id.title);
        tvtitle.setText("Select Subject");

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        recyclerView.setAdapter(new SubjectListAdapter(dialog, subjectList, subject_name));


        dialog.show();
        /* FOR SETTING CUSTOM HIGHT AND WIDTH */
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(dialog.getWindow().getAttributes());
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes(layoutParams);

    }

    private void setCurrentDate() {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(c);
        date.setText(formattedDate);
    }

    private void callApi() {
        final String appBaseUrl = LoginPreferences.getActiveInstance(context).getUserBaseUrl();
        Log.e("URL--", appBaseUrl);
        final ProgressD pd = ProgressD.show(context, context.getResources().getString(R.string.connecting), true);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, appBaseUrl + ConstantURL.APP_API,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.dismiss();
                        Log.e("Response--", response);
                        Gson gson = new Gson();
                        ViewHwResponse result = gson.fromJson(String.valueOf(response), ViewHwResponse.class);

                        if (result.result) {
                            if (result.data.size() != 0) {
                                AppData.getInstance().setHwDetail(result.data);
                                startActivity(new Intent(context, QuickActionDetailsP.class).putExtra(Constants.TYPE, Constants.VIEW_HW_DETAIL));
                            } else {
                                CommonMethod.showAlert("Homework Not Available", context);
                            }
                        } else {
                            CommonMethod.showAlert(result.message, context);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                        Log.e("error--", error.toString());
                        CommonMethod.showAlert(Constants.SERVER_ERROR, context);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("api_page", ConstantURL.HOME_WORK);
                params.put("session_id", LoginPreferences.getActiveInstance(context).getSessionId());
                params.put("school_id", LoginPreferences.getActiveInstance(context).getSchoolId());
                params.put("branch_id", LoginPreferences.getActiveInstance(context).getBranchId());
                params.put("student_id", LoginPreferences.getActiveInstance(context).getId());
                params.put("class_id", LoginPreferences.getActiveInstance(context).getClassId());
                params.put("section_id", LoginPreferences.getActiveInstance(context).getSectionId());
                params.put("subject_id", AppData.getInstance().getSubjectId());
                params.put("date", date.getText().toString().trim());
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }


    private void callSubjectApi() {
        final String appBaseUrl = LoginPreferences.getActiveInstance(context).getUserBaseUrl();
        Log.e("URL--", appBaseUrl);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, appBaseUrl + ConstantURL.APP_API,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response--", response);
                        Gson gson = new Gson();
                        SubjectListResponse result = gson.fromJson(String.valueOf(response), SubjectListResponse.class);

                        if (result.result) {
                            if (result.data.size() != 0) {

                                subjectList = result.data;

                                SubjectListResponse.Datum datum = new SubjectListResponse.Datum();
                                datum.subjectName = "All Subject";
                                datum.subjectId = "0";
                                subjectList.add(0, datum);
                                subject_name.setText(subjectList.get(0).subjectName);
                                AppData.getInstance().setSubjectId(subjectList.get(0).subjectId);

                            } else {
                                CommonMethod.showAlert(result.message, context);
                            }

                        } else {
                            CommonMethod.showAlert(result.message, context);
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("error--", error.toString());
                        CommonMethod.showAlert(Constants.SERVER_ERROR, context);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("api_page", ConstantURL.SUBJECT_LIST);
                params.put("session_id", LoginPreferences.getActiveInstance(context).getSessionId());
                params.put("school_id", LoginPreferences.getActiveInstance(context).getSchoolId());
                params.put("branch_id", LoginPreferences.getActiveInstance(context).getBranchId());
                params.put("student_id", LoginPreferences.getActiveInstance(context).getId());
                params.put("class_id", LoginPreferences.getActiveInstance(context).getClassId());
                params.put("section_id", LoginPreferences.getActiveInstance(context).getSectionId());
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }


}
