package com.smart.school.parent.activityP;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.smart.school.R;
import com.smart.school.parent.responsesP.LoginResponseP;
import com.smart.school.utils.ConstantURL;
import com.smart.school.utils.AppData;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.LoginPreferences;
import com.smart.school.utils.ProgressD;
import com.smart.school.utils.VolleySingleton;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

public class VerifyParent extends AppCompatActivity {

    String type;
    Context context;
    private String search_by;
    private EditText edit_text;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_parent);
        getSupportActionBar().hide();
        type = getIntent().getStringExtra("Type");
        context = VerifyParent.this;

        ImageView logo = findViewById(R.id.logo);
        TextView header_txt = findViewById(R.id.header_txt);
        TextView txt = findViewById(R.id.txt);
        edit_text = findViewById(R.id.edit_text);
        TextView school_code = findViewById(R.id.school_code);

        if (!TextUtils.isEmpty(LoginPreferences.getActiveInstance(context).getSchoolName())) {
            school_code.setText(LoginPreferences.getActiveInstance(context).getSchoolName());
        }

        String image = LoginPreferences.getActiveInstance(context).getSchoolLogo();
        if (!TextUtils.isEmpty(image)) {
            Picasso.get().load(image).placeholder(R.drawable.place_holder).into(logo);
        }


        if (type.equals("A")) {
            txt.setVisibility(View.GONE);
            header_txt.setText("Enter Admission Number");
            edit_text.setHint("Enter here");
            edit_text.setInputType(InputType.TYPE_CLASS_TEXT);
            search_by = "T1.admission_no";

        } else {
            search_by = "T3.sms_mobile_no";
        }


        findViewById(R.id.btn_continue).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(edit_text.getText().toString().trim())) {
                    edit_text.requestFocus();
                    edit_text.setError("This field is required...");
                } else {
                    if (CommonMethod.isOnline(context)) {
                        callApi();
                    } else {
                        CommonMethod.showNetworkAlert(context);
                    }


                }

            }
        });

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });


    }


    private void callApi() {
        final String appBaseUrl = LoginPreferences.getActiveInstance(context).getUserBaseUrl();
        /* String appBaseUrl = AppData.getInstance().getResponse().appApiUrl + "/parent";*/

        Log.e("URL--", appBaseUrl);
        final ProgressD pd = ProgressD.show(context, context.getResources().getString(R.string.connecting), true);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, appBaseUrl + ConstantURL.APP_API,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.dismiss();

                        Log.e("Response--", response);

                        Gson gson = new Gson();
                        LoginResponseP result = gson.fromJson(String.valueOf(response), LoginResponseP.class);
                        AppData.getInstance().setChildDataP(result.data);

                        if (result.result) {
                            startActivity(new Intent(VerifyParent.this, VerifyNumber2.class)
                                    .putExtra("otp_code", result.otpCode));

                        } else {
                            CommonMethod.showAlert(result.message, context);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                        Log.e("error--", error.toString());
                        Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("api_page", "student_login");
                params.put("session_id", LoginPreferences.getActiveInstance(context).getSessionId());
                params.put("school_id", LoginPreferences.getActiveInstance(context).getSchoolId());
                params.put("branch_id", LoginPreferences.getActiveInstance(context).getBranchId());
                params.put("search_by", search_by);
                params.put("key_value", edit_text.getText().toString().trim());
                return params;
            }
        };
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }


}
