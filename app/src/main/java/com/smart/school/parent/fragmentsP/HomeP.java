package com.smart.school.parent.fragmentsP;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.smart.school.R;
import com.smart.school.parent.adaptersP.AboutSchoolAdapterP;
import com.smart.school.parent.adaptersP.ImpActionAdapter;
import com.smart.school.parent.adaptersP.QuickActionAdapterP;
import com.smart.school.parent.adaptersP.TopGridAdapterP;
import com.smart.school.parent.responsesP.DashboardTopResponse;
import com.smart.school.utils.ConstantURL;
import com.smart.school.utils.AppData;
import com.smart.school.utils.LoginPreferences;
import com.smart.school.utils.ProgressD;
import com.smart.school.utils.VolleySingleton;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by Manoj Singh Deopa
 */

@SuppressLint("ValidFragment")

public class HomeP extends Fragment {

    private RecyclerView rv_top, rv_quick_action, rv_1, rv_about_school;
    private Context context;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.home_fragment_p, container, false);
        context = getActivity();
        rv_top = view.findViewById(R.id.rv_top);
        RecyclerView.LayoutManager layoutManager1 = new GridLayoutManager(context, 2);
        rv_top.setLayoutManager(layoutManager1);
        rv_top.setItemAnimator(new DefaultItemAnimator());

        if (AppData.getInstance().getDashboardTopResponse() != null) {
            rv_top.setAdapter(new TopGridAdapterP(AppData.getInstance().getDashboardTopResponse()));
        } else {
            callApi();
        }

        rv_quick_action = view.findViewById(R.id.rv_quick_action);
        RecyclerView.LayoutManager layoutManager2 = new GridLayoutManager(context, 3);
        rv_quick_action.setLayoutManager(layoutManager2);
        rv_quick_action.setItemAnimator(new DefaultItemAnimator());
        rv_quick_action.setAdapter(new QuickActionAdapterP(getActivity()));


        rv_1 = view.findViewById(R.id.rv_1);
        RecyclerView.LayoutManager layoutManager3 = new GridLayoutManager(context, 3);
        rv_1.setLayoutManager(layoutManager3);
        rv_1.setItemAnimator(new DefaultItemAnimator());
        rv_1.setAdapter(new ImpActionAdapter(getActivity()));


        rv_about_school = view.findViewById(R.id.rv_about_school);
        RecyclerView.LayoutManager layoutManager4 = new GridLayoutManager(context, 3);
        rv_about_school.setLayoutManager(layoutManager4);
        rv_about_school.setItemAnimator(new DefaultItemAnimator());
        rv_about_school.setAdapter(new AboutSchoolAdapterP());


        rv_top.setNestedScrollingEnabled(false);
        rv_quick_action.setNestedScrollingEnabled(false);
        rv_1.setNestedScrollingEnabled(false);
        rv_about_school.setNestedScrollingEnabled(false);

        return view;
    }


    private void callApi() {
        final String appBaseUrl = LoginPreferences.getActiveInstance(context).getUserBaseUrl();
        /* String appBaseUrl = AppData.getInstance().getResponse().appApiUrl + "/parent";*/

        Log.e("URL--", appBaseUrl);
        final ProgressD pd = ProgressD.show(context, context.getResources().getString(R.string.connecting), true);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, appBaseUrl + ConstantURL.APP_API,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.dismiss();
                        Log.e("Response--", response);

                        Gson gson = new Gson();
                        DashboardTopResponse result = gson.fromJson(String.valueOf(response), DashboardTopResponse.class);

                        if (result.result) {
                            AppData.getInstance().setDashboardTopResponse(result.data);
                            rv_top.setAdapter(new TopGridAdapterP(result.data));
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                        Log.e("error--", error.toString());
                        Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("api_page", "my_index");
                params.put("session_id", LoginPreferences.getActiveInstance(context).getSessionId());
                params.put("school_id", LoginPreferences.getActiveInstance(context).getSchoolId());
                params.put("branch_id", LoginPreferences.getActiveInstance(context).getBranchId());
                params.put("student_id", LoginPreferences.getActiveInstance(context).getId());
                params.put("class_id", LoginPreferences.getActiveInstance(context).getClassId());
                params.put("section_id", LoginPreferences.getActiveInstance(context).getSectionId());
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }


}
