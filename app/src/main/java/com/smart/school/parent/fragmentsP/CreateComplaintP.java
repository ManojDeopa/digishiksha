package com.smart.school.parent.fragmentsP;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.smart.school.R;
import com.smart.school.parent.adaptersP.MyTeacherAdapter;
import com.smart.school.parent.responsesP.MyTeacherResponse;
import com.smart.school.teacher.responsesT.CommonResponse;
import com.smart.school.utils.ConstantURL;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.Constants;
import com.smart.school.utils.LoginPreferences;
import com.smart.school.utils.ProgressD;
import com.smart.school.utils.VolleySingleton;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Manoj Singh Deopa on 2/4/18.
 */

@SuppressLint("ValidFragment")

public class CreateComplaintP extends Fragment {

    TextView complaint_to_name;
    private Context context;
    private View view;
    private EditText et_subject, et_description;
    private List<MyTeacherResponse.Datum> teacherList;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.create_complaint, container, false);

        initViews();

        setCurrentDate();

        onclicks();

        callApiTeacher();

        return view;

    }

    private void validate() {
        if (TextUtils.isEmpty(et_subject.getText().toString().trim())) {
            Toast.makeText(context, "Please Enter Subject", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(et_description.getText().toString().trim())) {
            Toast.makeText(context, "Please Enter Description", Toast.LENGTH_SHORT).show();
        } else {
            callApi();
        }
    }

    private void initViews() {
        context = getActivity();
        et_subject = view.findViewById(R.id.et_subject);
        et_description = view.findViewById(R.id.et_description);
        complaint_to_name = view.findViewById(R.id.complaint_to_name);
    }

    private void onclicks() {


        complaint_to_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSubjectDialog();
            }
        });

        view.findViewById(R.id.btn_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate();
            }
        });

    }


    private void callApiTeacher() {
        final String appBaseUrl = LoginPreferences.getActiveInstance(context).getUserBaseUrl();
        /* String appBaseUrl = AppData.getInstance().getResponse().appApiUrl + "/parent";*/

        Log.e("URL--", appBaseUrl);
        final ProgressD pd = ProgressD.show(context, context.getResources().getString(R.string.connecting), true);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, appBaseUrl + ConstantURL.APP_API,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.dismiss();
                        Log.e("Response--", response);

                        Gson gson = new Gson();
                        MyTeacherResponse result = gson.fromJson(String.valueOf(response), MyTeacherResponse.class);

                        if (result.result) {

                            if (result.data != null) {
                                complaint_to_name.setText(result.data.get(0).teacherName);
                                teacherList = result.data;
                            }
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                        Log.e("error--", error.toString());
                        Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("api_page", ConstantURL.TEACHERS);
                params.put("session_id", LoginPreferences.getActiveInstance(context).getSessionId());
                params.put("school_id", LoginPreferences.getActiveInstance(context).getSchoolId());
                params.put("branch_id", LoginPreferences.getActiveInstance(context).getBranchId());
                params.put("student_id", LoginPreferences.getActiveInstance(context).getId());
                params.put("class_id", LoginPreferences.getActiveInstance(context).getClassId());
                params.put("section_id", LoginPreferences.getActiveInstance(context).getSectionId());
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }


    public void showSubjectDialog() {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.list_dialog_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;


        RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view);
        CommonMethod.setRecyclerView(recyclerView, context);

        TextView btn_cancel = dialog.findViewById(R.id.btn_cancel);

        TextView tvtitle = dialog.findViewById(R.id.title);
        tvtitle.setText("Select");

        recyclerView.setAdapter(new MyTeacherAdapter(teacherList, dialog, complaint_to_name, Constants.TYPE_COMPLAINTS));

        dialog.show();


        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        /* FOR SETTING CUSTOM HIGHT AND WIDTH */
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(dialog.getWindow().getAttributes());
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes(layoutParams);

    }

    private void setCurrentDate() {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(c);
        Log.e("Date--", formattedDate);
    }

    private void callApi() {
        final String appBaseUrl = LoginPreferences.getActiveInstance(context).getUserBaseUrl();
        Log.e("URL--", appBaseUrl);
        final ProgressD pd = ProgressD.show(context, context.getResources().getString(R.string.connecting), true);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, appBaseUrl + ConstantURL.APP_API,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.dismiss();
                        Log.e("Response--", response);
                        Gson gson = new Gson();
                        CommonResponse result = gson.fromJson(String.valueOf(response), CommonResponse.class);
                        if (result.result) {
                            Toast.makeText(context, result.message, Toast.LENGTH_SHORT).show();
                        } else {
                            CommonMethod.showAlert(result.message, context);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                        Log.e("error--", error.toString());
                        Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("api_page", "leave_request");
                params.put("session_id", LoginPreferences.getActiveInstance(context).getSessionId());
                params.put("school_id", LoginPreferences.getActiveInstance(context).getSchoolId());
                params.put("branch_id", LoginPreferences.getActiveInstance(context).getBranchId());
                params.put("student_id", LoginPreferences.getActiveInstance(context).getId());
                params.put("class_id", LoginPreferences.getActiveInstance(context).getClassId());
                params.put("section_id", LoginPreferences.getActiveInstance(context).getSectionId());
                params.put("description", et_description.getText().toString().trim());
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }


}
