package com.smart.school.parent.responsesP;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by manoj on 23/3/19.
 */
public class GalleryResponse {

    @SerializedName("result")
    @Expose
    public Boolean result;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;

    public class Datum {
        @SerializedName("media_id")
        @Expose
        public Integer mediaId;
        @SerializedName("album_id")
        @Expose
        public Integer albumId;
        @SerializedName("media_name")
        @Expose
        public String mediaName;
        @SerializedName("alias")
        @Expose
        public String alias;
        @SerializedName("bg_img")
        @Expose
        public String bgImg;
        @SerializedName("type")
        @Expose
        public String type;
        @SerializedName("video_url")
        @Expose
        public String video_url;
    }

}
