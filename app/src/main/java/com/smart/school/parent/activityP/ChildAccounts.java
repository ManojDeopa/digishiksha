package com.smart.school.parent.activityP;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.VerifySchoolActivity;
import com.smart.school.parent.adaptersP.ChildListAdapterP;
import com.smart.school.parent.responsesP.LoginResponseP;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.LoginPreferences;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ChildAccounts extends AppCompatActivity {
    Context context;
    private ArrayList<LoginResponseP.Datum> childList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.child_accounts);
        context = ChildAccounts.this;
        getSupportActionBar().hide();

        configureView();

    }

    private void configureView() {
        TextView app_version = findViewById(R.id.app_version);
        CommonMethod.setVersionName(app_version);

        ImageView iv_school_logo = findViewById(R.id.iv_school_logo);
        TextView school_code = findViewById(R.id.school_code);

        String image = LoginPreferences.getActiveInstance(context).getSchoolLogo();
        if (!TextUtils.isEmpty(image)) {
            Picasso.get().load(image).placeholder(R.drawable.place_holder).into(iv_school_logo);
        }
        if (!TextUtils.isEmpty(LoginPreferences.getActiveInstance(context).getSchoolName())) {
            school_code.setText(LoginPreferences.getActiveInstance(context).getSchoolName());
        }


        findViewById(R.id.btn_add_account).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectTypeDialog();

            }
        });


        getChildData();

        if (childList.size() != 0) {
            String id = childList.get(0).studentId;
            for (int i = 1; i < childList.size(); i++) {
                if (id.equals(childList.get(i).studentId)) {
                    new ChildListDatabase(context).delete(childList.get(i).id);
                    childList.remove(i);
                }
            }

            RecyclerView recycler_view = findViewById(R.id.recycler_view);
            CommonMethod.setRecyclerView(recycler_view, context);
            recycler_view.setAdapter(new ChildListAdapterP(childList, "outside"));
        }
    }


    private void getChildData() {
        ChildListDatabase mDatabase = new ChildListDatabase(context);
        childList = new ArrayList<LoginResponseP.Datum>();
        Cursor cursor = mDatabase.getAllData();
        if (cursor.moveToFirst()) {
            do {
                childList.add(new LoginResponseP.Datum(
                        cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getString(5),
                        cursor.getString(6),
                        cursor.getString(7),
                        cursor.getString(8)));
            } while (cursor.moveToNext());
        }
    }


    private void selectTypeDialog() {

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(context);
        bottomSheetDialog.setContentView(R.layout.select_add_type);
        bottomSheetDialog.setCancelable(true);
        bottomSheetDialog.show();

        TextView with_admission = bottomSheetDialog.findViewById(R.id.with_admission);
        TextView with_mobile = bottomSheetDialog.findViewById(R.id.with_mobile);


        with_mobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
                startActivity(new Intent(context, VerifyParent.class).putExtra("Type", "M"));
            }
        });

        with_admission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
                startActivity(new Intent(context, VerifyParent.class).putExtra("Type", "A"));
            }
        });

    }


    public void anotherSchool(View view) {
        ChildListDatabase mDatabase = new ChildListDatabase(ChildAccounts.this);
        mDatabase.getWritableDatabase().execSQL("delete from " + ChildListDatabase.TABLE_NAME);
        LoginPreferences.getActiveInstance(context).setType("");
        finish();
        CommonMethod.startIntent(this, VerifySchoolActivity.class);

    }
}
