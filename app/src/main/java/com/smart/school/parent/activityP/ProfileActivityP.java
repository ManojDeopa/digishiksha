package com.smart.school.parent.activityP;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.smart.school.R;
import com.smart.school.parent.adaptersP.ProfileAdapter;
import com.smart.school.parent.responsesP.ProfileResponseP;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.ConstantURL;
import com.smart.school.utils.LoginPreferences;
import com.smart.school.utils.ProgressD;
import com.smart.school.utils.VolleySingleton;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProfileActivityP extends AppCompatActivity {

    private static final String TAG = "ProfileActivityP";
    TextView name, class_section;
    ImageView image;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_profile_parent);
        context = ProfileActivityP.this;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Profile");

        setHeader();

        callApi();
    }

    private void setHeader() {
        name = findViewById(R.id.name);
        class_section = findViewById(R.id.class_section);
        image = findViewById(R.id.image);
        name.setText(LoginPreferences.getActiveInstance(context).getFullname());
        class_section.setText(LoginPreferences.getActiveInstance(context).getClassSection());
        String userImage = LoginPreferences.getActiveInstance(context).getUserImage();
        Picasso.get().load(userImage).placeholder(R.drawable.shool_logo).into(image);
    }

    private void setRecyclerViewData(List<ProfileResponseP.Datum> data) {
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        CommonMethod.setRecyclerView(recyclerView, context);
        recyclerView.setAdapter(new ProfileAdapter(data));
    }


    private void callApi() {
        final String appBaseUrl = LoginPreferences.getActiveInstance(context).getUserBaseUrl();
        /* String appBaseUrl = AppData.getInstance().getResponse().appApiUrl + "/parent";*/

        Log.e("URL--", appBaseUrl);
        final ProgressD pd = ProgressD.show(context, context.getResources().getString(R.string.connecting), true);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, appBaseUrl + ConstantURL.APP_API,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.dismiss();
                        Log.e("Response--", response);

                        Gson gson = new Gson();
                        ProfileResponseP result = gson.fromJson(String.valueOf(response), ProfileResponseP.class);

                        if (result.result) {
                            setRecyclerViewData(result.data);
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                        Log.e("error--", error.toString());
                        Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("api_page", ConstantURL.STUDENT_PROFILE);
                params.put("session_id", LoginPreferences.getActiveInstance(context).getSessionId());
                params.put("school_id", LoginPreferences.getActiveInstance(context).getSchoolId());
                params.put("branch_id", LoginPreferences.getActiveInstance(context).getBranchId());
                params.put("student_id", LoginPreferences.getActiveInstance(context).getId());
                return params;
            }
        };
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        finish();
    }


}
