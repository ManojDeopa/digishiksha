package com.smart.school.parent.fragmentsP;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.smart.school.R;
import com.smart.school.teacher.activityT.QuickActionDetails;
import com.smart.school.teacher.adaptersT.SubjectListAdapter;
import com.smart.school.teacher.responsesT.SubjectListResponse;
import com.smart.school.teacher.responsesT.ViewHwResponse;
import com.smart.school.utils.ConstantURL;
import com.smart.school.utils.AppData;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.Constants;
import com.smart.school.utils.LoginPreferences;
import com.smart.school.utils.ProgressD;
import com.smart.school.utils.VolleySingleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Manoj Singh Deopa on 2/4/18.
 */

@SuppressLint("ValidFragment")

public class ServiceRequestP extends Fragment {

    TextView request_type;
    private Context context;
    private View view;
    private List<SubjectListResponse.Datum> subjectList = new ArrayList<>();

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.service_request, container, false);

        initViews();

        onclicks();

        callExamTimeApi();

        return view;

    }

    private void validate() {
        if (TextUtils.isEmpty(request_type.getText().toString().trim())) {
            Toast.makeText(context, "Please Select Request Type", Toast.LENGTH_SHORT).show();
        } else {
            callApi();
        }
    }

    private void initViews() {
        context = getActivity();
        request_type = view.findViewById(R.id.request_type);
    }

    private void onclicks() {

        request_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
            }
        });


        view.findViewById(R.id.btn_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // validate();
            }
        });


    }


    public void showDialog() {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.list_dialog_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;


        RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view);
        CommonMethod.setRecyclerView(recyclerView, context);


        TextView btn_submit = dialog.findViewById(R.id.btn_submit);
        TextView btn_cancel = dialog.findViewById(R.id.btn_cancel);

        TextView tvtitle = dialog.findViewById(R.id.title);
        tvtitle.setText("Select Request Type");

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        recyclerView.setAdapter(new SubjectListAdapter(dialog, subjectList, request_type));


        dialog.show();
        /* FOR SETTING CUSTOM HIGHT AND WIDTH */
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(dialog.getWindow().getAttributes());
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes(layoutParams);

    }


    private void callApi() {
        final String appBaseUrl = LoginPreferences.getActiveInstance(context).getUserBaseUrl();
        Log.e("URL--", appBaseUrl);
        final ProgressD pd = ProgressD.show(context, context.getResources().getString(R.string.connecting), true);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, appBaseUrl + ConstantURL.APP_API,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.dismiss();
                        Log.e("Response--", response);
                        Gson gson = new Gson();
                        ViewHwResponse result = gson.fromJson(String.valueOf(response), ViewHwResponse.class);

                        if (result.result) {
                            if (result.data.size() != 0) {
                                AppData.getInstance().setHwDetail(result.data);
                                startActivity(new Intent(context, QuickActionDetails.class).putExtra(Constants.TYPE, Constants.VIEW_HW_DETAIL));
                            }

                        } else {
                            CommonMethod.showAlert(result.message, context);
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                        Log.e("error--", error.toString());
                        Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("api_page", "view_exam_time_table");
                params.put("session_id", LoginPreferences.getActiveInstance(context).getSessionId());
                params.put("school_id", LoginPreferences.getActiveInstance(context).getSchoolId());
                params.put("branch_id", LoginPreferences.getActiveInstance(context).getBranchId());
                params.put("student_id", LoginPreferences.getActiveInstance(context).getId());
                params.put("class_id", LoginPreferences.getActiveInstance(context).getClassId());
                params.put("section_id", LoginPreferences.getActiveInstance(context).getSectionId());

                params.put("exam_time_table", "");
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }


    private void callExamTimeApi() {
        final String appBaseUrl = LoginPreferences.getActiveInstance(context).getUserBaseUrl();
        Log.e("URL--", appBaseUrl);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, appBaseUrl + ConstantURL.APP_API,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response--", response);
                        Gson gson = new Gson();
                        SubjectListResponse result = gson.fromJson(String.valueOf(response), SubjectListResponse.class);

                        if (result.result) {
                            if (result.data.size() != 0) {
                                AppData.getInstance().setSubjectId(result.data.get(0).subjectId);
                                subjectList = result.data;
                            } else {
                                Toast.makeText(context, "No Data Available", Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            CommonMethod.showAlert(result.message, context);
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("error--", error.toString());
                        CommonMethod.showAlert(Constants.SERVER_ERROR, context);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("api_page", "exam_time_table");
                params.put("session_id", LoginPreferences.getActiveInstance(context).getSessionId());
                params.put("school_id", LoginPreferences.getActiveInstance(context).getSchoolId());
                params.put("branch_id", LoginPreferences.getActiveInstance(context).getBranchId());
                params.put("student_id", LoginPreferences.getActiveInstance(context).getId());
                params.put("class_id", LoginPreferences.getActiveInstance(context).getClassId());
                params.put("section_id", LoginPreferences.getActiveInstance(context).getSectionId());
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }


}
