package com.smart.school.parent.adaptersP;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.parent.responsesP.NoticeResponse;
import com.smart.school.utils.CommonMethod;

import java.util.List;


/**
 * Created by Manoj Singh Deopa.
 */

public class FileAdapter extends RecyclerView.Adapter<FileAdapter.ViewHolder> {

    private List<NoticeResponse.Datum.File> list;
    private Context context;

    public FileAdapter(List<NoticeResponse.Datum.File> file) {
        this.list = file;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.file_adapter, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.attachment.setText(list.get(position).file);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView attachment;

        public ViewHolder(View view) {
            super(view);
            attachment = view.findViewById(R.id.attachment);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CommonMethod.callBrowserIntent(context, list.get(getAdapterPosition()).file_path);
                }
            });

        }
    }
}