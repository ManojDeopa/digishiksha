package com.smart.school.parent.fragmentsP;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.parent.adaptersP.SchoolFeeAdapter;
import com.smart.school.parent.responsesP.SchoolFeeResponse;
import com.smart.school.utils.AppData;
import com.smart.school.utils.CommonMethod;


/**
 * Created by Manoj Singh Deopa on 2/4/18.
 */

@SuppressLint("ValidFragment")

public class SchoolFeeP extends Fragment {

    RecyclerView recycler_view;
    TextView total_fee;
    private Context context;
    private View view;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.school_fee, container, false);
        context = getActivity();

        SchoolFeeResponse response = AppData.getInstance().getFeeResponse();

        total_fee = view.findViewById(R.id.total_fee);
        recycler_view = view.findViewById(R.id.recycler_view);
        CommonMethod.setRecyclerView(recycler_view, context);

        recycler_view.setAdapter(new SchoolFeeAdapter(response.feesList));
        total_fee.setText(response.feesTotal.get(0).totalBalance);

        return view;

    }


}
