package com.smart.school.parent.activityP;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.smart.school.R;
import com.smart.school.parent.fragmentsP.AboutSchoolFragP;
import com.smart.school.utils.AppUtils;
import com.smart.school.utils.Constants;

public class AboutSchoolP extends AppCompatActivity {

    private static final String TAG = "AboutSchoolFragP";
    private Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quick_action_details);
        context = AboutSchoolP.this;

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String type = getIntent().getStringExtra(Constants.TYPE);

        getSupportActionBar().setTitle(type);

        setFragment(type);

    }

    private void setFragment(String type) {
        Fragment fragment = new AboutSchoolFragP(type);
        AppUtils.setFragment(fragment, true, AboutSchoolP.this, R.id.container);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        finish();
    }


}
