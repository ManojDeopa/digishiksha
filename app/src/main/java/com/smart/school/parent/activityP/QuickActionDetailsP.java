package com.smart.school.parent.activityP;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.smart.school.R;
import com.smart.school.parent.fragmentsP.AssignmentTabFragment;
import com.smart.school.parent.fragmentsP.AttendanceFragP;
import com.smart.school.parent.fragmentsP.BusRouteP;
import com.smart.school.parent.fragmentsP.ExamResultsP;
import com.smart.school.parent.fragmentsP.ExamTimeTableP;
import com.smart.school.parent.fragmentsP.FeeTabFragment;
import com.smart.school.parent.fragmentsP.FeedsFragment;
import com.smart.school.parent.fragmentsP.LeaveTabFragment;
import com.smart.school.parent.fragmentsP.LibraryTabFragment;
import com.smart.school.parent.fragmentsP.NoticeFragP;
import com.smart.school.parent.fragmentsP.TimeTableTabFragment;
import com.smart.school.parent.fragmentsP.ViewHWP;
import com.smart.school.teacher.fragmentsT.ViewHwDetail;
import com.smart.school.utils.AppUtils;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.Constants;

public class QuickActionDetailsP extends AppCompatActivity {

    private static final String TAG = "QuickActionDetailsP";
    private Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quick_action_details);
        context = QuickActionDetailsP.this;

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String type = getIntent().getStringExtra(Constants.TYPE);

        getSupportActionBar().setTitle(type);

        setFragment(type);

    }

    private void setFragment(String type) {

        Fragment fragment = new Fragment();

        switch (type) {
            case Constants.TYPE_ATTENDANCE:
                fragment = new AttendanceFragP();
                break;

            case Constants.TYPE_TIME_TABLE:
                fragment = new TimeTableTabFragment();
                break;

            case Constants.TYPE_NOTICE:
                fragment = new NoticeFragP();
                break;

            case Constants.TYPE_EXAM_RESULT:
                fragment = new ExamResultsP();
                break;

            case Constants.TYPE_CLASSTEST_RESULT:
                fragment = new ExamResultsP();
                break;

            case Constants.TYPE_FEES:
                fragment = new FeeTabFragment();
                break;

            case Constants.TYPE_HOMEWORK:
                fragment = new ViewHWP();
                break;

            case Constants.TYPE_BUS_ROUTE:
                fragment = new BusRouteP();
                break;

            case Constants.TYPE_LEAVE_REQUEST:
                fragment = new LeaveTabFragment();
                break;

            case Constants.TYPE_EXAM_TIMETABLE:
                fragment = new ExamTimeTableP();
                break;

            case Constants.TYPE_LIBRARY:
                fragment = new LibraryTabFragment();
                break;

            case Constants.VIEW_HW_DETAIL:
                fragment = new ViewHwDetail();
                break;

            case Constants.TYPE_ASSIGNMENT:
                fragment = new AssignmentTabFragment();
                break;

            case Constants.FEED:
                fragment = new FeedsFragment();
                break;

            case Constants.ACTIVITY:
                fragment = new FeedsFragment();
                break;

            default:
                CommonMethod.showFinishAlert(Constants.SERVER_ERROR, this);
                break;
        }

        AppUtils.setFragment(fragment, true, QuickActionDetailsP.this, R.id.container);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
