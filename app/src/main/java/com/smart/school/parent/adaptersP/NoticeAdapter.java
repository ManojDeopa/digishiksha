package com.smart.school.parent.adaptersP;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.parent.responsesP.NoticeResponse;
import com.smart.school.utils.CommonMethod;

import java.util.List;


/**
 * Created by Manoj Singh Deopa.
 */

public class NoticeAdapter extends RecyclerView.Adapter<NoticeAdapter.ViewHolder> {

    private List<NoticeResponse.Datum> list;
    private Context context;

    public NoticeAdapter(List<NoticeResponse.Datum> data) {
        this.list = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notice_adapter, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int i) {
        holder.title.setText(list.get(i).title);
        holder.date.setText(list.get(i).created);
        holder.detail.setText(list.get(i).notice);
        if (list.get(i).file != null) {
            CommonMethod.setRecyclerView(holder.rv_doc, context);
            holder.rv_doc.setAdapter(new FileAdapter(list.get(i).file));
        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView title, date, detail;
        RecyclerView rv_doc;

        public ViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.title);
            date = view.findViewById(R.id.date);
            detail = view.findViewById(R.id.detail);
            rv_doc = view.findViewById(R.id.rv_doc);
        }
    }
}