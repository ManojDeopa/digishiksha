package com.smart.school.parent.fragmentsP;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.smart.school.R;
import com.smart.school.teacher.responsesT.CommonResponse;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.ConstantURL;
import com.smart.school.utils.Constants;
import com.smart.school.utils.LoginPreferences;
import com.smart.school.utils.ProgressD;
import com.smart.school.utils.VolleySingleton;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by Manoj Singh Deopa on 2/4/18.
 */

@SuppressLint("ValidFragment")

public class LeaveRequestP extends Fragment {

    TextView start_date, end_date;
    String leave_type = "Casual";
    private Context context;
    private View view;
    private EditText leave_reason;
    private RadioGroup radio_group;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.leave_request, container, false);

        initViews();

        setCurrentDate();

        onclicks();

        return view;

    }

    private void validate() {
        if (TextUtils.isEmpty(leave_reason.getText().toString().trim())) {
            Toast.makeText(context, "Please Enter Leave Reason", Toast.LENGTH_SHORT).show();
        } else if (leave_type.equals("")) {
            Toast.makeText(context, "Please Select Leave Type", Toast.LENGTH_SHORT).show();
        } else {
            callApi();
        }
    }

    private void initViews() {
        context = getActivity();
        leave_reason = view.findViewById(R.id.leave_reason);
        start_date = view.findViewById(R.id.start_date);
        end_date = view.findViewById(R.id.end_date);


        radio_group = view.findViewById(R.id.radio_group);

    }

    private void onclicks() {

        radio_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                                   @Override
                                                   public void onCheckedChanged(RadioGroup group, int checkedId) {
                                                       RadioButton radioButton = (RadioButton) group.findViewById(checkedId);
                                                       leave_type= String.valueOf(radioButton.getText());
                                                   }
                                               }
        );


        start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethod.showDatePicker(context, start_date);
            }
        });

        end_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethod.showDatePicker(context, end_date);
            }
        });


        view.findViewById(R.id.btn_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate();
            }
        });


    }


    private void setCurrentDate() {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(c);
        start_date.setText(formattedDate);
        end_date.setText(formattedDate);
    }

    private void callApi() {
        final String appBaseUrl = LoginPreferences.getActiveInstance(context).getUserBaseUrl();
        Log.e("URL--", appBaseUrl);
        final ProgressD pd = ProgressD.show(context, context.getResources().getString(R.string.connecting), true);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, appBaseUrl + ConstantURL.APP_API,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.dismiss();
                        Log.e("Response--", response);

                        try {
                            Gson gson = new Gson();
                            CommonResponse result = gson.fromJson(String.valueOf(response), CommonResponse.class);
                            if (result.result) {
                                CommonMethod.showFinishAlert(result.message, getActivity());

                            } else {
                                CommonMethod.showAlert(result.message, context);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            CommonMethod.showAlert(Constants.SERVER_ERROR, context);
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                        Log.e("error--", error.toString());
                        CommonMethod.showAlert(Constants.SERVER_ERROR, context);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                Log.e("leave_type----", leave_type);

                Map<String, String> params = new HashMap<>();
                params.put("api_page", "leave_request");
                params.put("session_id", LoginPreferences.getActiveInstance(context).getSessionId());
                params.put("school_id", LoginPreferences.getActiveInstance(context).getSchoolId());
                params.put("branch_id", LoginPreferences.getActiveInstance(context).getBranchId());
                params.put("student_id", LoginPreferences.getActiveInstance(context).getId());
                params.put("class_id", LoginPreferences.getActiveInstance(context).getClassId());
                params.put("section_id", LoginPreferences.getActiveInstance(context).getSectionId());

                params.put("leave_reason", leave_reason.getText().toString().trim());
                params.put("leave_type", leave_type);
                params.put("start_date", start_date.getText().toString().trim());
                params.put("end_date", end_date.getText().toString().trim());
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }

}
