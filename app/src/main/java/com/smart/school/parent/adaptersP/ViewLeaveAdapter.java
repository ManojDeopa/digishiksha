package com.smart.school.parent.adaptersP;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.parent.responsesP.ViewLeaveResponse;

import java.util.List;


/**
 * Created by Manoj Singh Deopa.
 */

public class ViewLeaveAdapter extends RecyclerView.Adapter<ViewLeaveAdapter.ViewHolder> {

    private List<ViewLeaveResponse.Datum> list;
    private Context context;

    public ViewLeaveAdapter(List<ViewLeaveResponse.Datum> data) {
        this.list = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_leave_adapter, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int i) {

        holder.leave_type.setText(list.get(i).leaveType);

        holder.created_at.setText(list.get(i).created);
        holder.reason.setText(list.get(i).reason);
        holder.to_date.setText("To" + "\n" + list.get(i).toDate);
        holder.from_date.setText("From" + "\n" + list.get(i).fromDate);

        String status = list.get(i).status;

        switch (status) {
            case "Pending":
                holder.status.setTextColor(context.getResources().getColor(R.color.pending_color));
                break;
            case "Reject":
                holder.status.setTextColor(context.getResources().getColor(R.color.reject_color));
                break;
            default:
                holder.status.setTextColor(context.getResources().getColor(R.color.approved_color));
                break;
        }
        holder.status.setText(status);


    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView leave_type, created_at, reason, to_date, from_date, status;

        public ViewHolder(View view) {
            super(view);
            leave_type = view.findViewById(R.id.leave_type);
            created_at = view.findViewById(R.id.created_at);
            reason = view.findViewById(R.id.reason);
            to_date = view.findViewById(R.id.to_date);
            from_date = view.findViewById(R.id.from_date);
            status = view.findViewById(R.id.status);

        }
    }
}