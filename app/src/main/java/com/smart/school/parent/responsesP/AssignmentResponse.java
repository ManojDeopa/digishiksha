package com.smart.school.parent.responsesP;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by manoj on 25/5/19.
 */
public class AssignmentResponse {
    @SerializedName("result")
    @Expose
    public Boolean result;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;

    public class Datum {

        @SerializedName("assignment_id")
        @Expose
        public String assignmentId;
        @SerializedName("assignment_title")
        @Expose
        public String assignmentTitle;
        @SerializedName("status")
        @Expose
        public String status;
        @SerializedName("updated_on")
        @Expose
        public String updatedOn;
        @SerializedName("assigned_on")
        @Expose
        public String assignedOn;
        @SerializedName("submitted_on")
        @Expose
        public String submittedOn;
        @SerializedName("file_path")
        @Expose
        public String filePath;
        @SerializedName("updated_by")
        @Expose
        public String updated_by;
        @SerializedName("user_img")
        @Expose
        public String user_img;
        @SerializedName("description")
        @Expose
        public String description;




    }

}
