package com.smart.school.parent.responsesP;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by manoj on 24/2/19.
 */
public class BusRouteResponse {

    @SerializedName("result")
    @Expose
    public Boolean result;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public List<BusRouteResponse.Datum> data = null;


    public class Datum {

        @SerializedName("title_id")
        @Expose
        public String titleId;
        @SerializedName("title_val")
        @Expose
        public String titleVal;

    }

}
