package com.smart.school.parent.adaptersP;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.parent.activityP.ImportantLinksP;
import com.smart.school.utils.Constants;
import com.squareup.picasso.Picasso;


/**
 * Created by Manoj Singh Deopa .
 */

public class ImpActionAdapter extends RecyclerView.Adapter<ImpActionAdapter.ViewHolder> {
    private FragmentActivity context;
    private String[] list = {
            Constants.TYPE_CALLENDER,
            Constants.TYPE_ANNOUNCEMENT,
            Constants.TYPE_GALLERY,
            Constants.TYPE_STYDY_MATERIAL,
            Constants.TYPE_MY_TEACHER,
            Constants.TYPE_MY_CLASSMATES,
            Constants.TYPE_MY_ACHIEVEMENTS,
            Constants.TYPE_SERVICE_REQUEST,
            Constants.TYPE_COMPLAINTS,
    };

    private int[] list_image = {
            R.drawable.calander_p,
            R.drawable.announcement_p,
            R.drawable.gallary_p,
            R.drawable.study_material_p,
            R.drawable.my_teacher_p,
            R.drawable.my_classmate_p,
            R.drawable.my_achievement_p,
            R.drawable.service_request_p,
            R.drawable.complaints_p
    };

    public ImpActionAdapter(FragmentActivity activity) {
        this.context = activity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dashboard_item_layout, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.tv_titles.setText(list[position]);
        Picasso.get().load(list_image[position]).into(holder.iv_image);

    }

    @Override
    public int getItemCount() {
        return list.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_titles;
        private ImageView iv_image;
        private LinearLayout parent;

        public ViewHolder(View view) {
            super(view);
            iv_image = view.findViewById(R.id.iv_image);
            tv_titles = view.findViewById(R.id.tv_title);
            parent = view.findViewById(R.id.parent);

            parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String type = list[getAdapterPosition()].trim();
                    context.startActivity(new Intent(context, ImportantLinksP.class).putExtra(Constants.TYPE, type));
                }
            });

        }
    }

}