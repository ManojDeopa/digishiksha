package com.smart.school.parent.responsesP;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by manoj on 16/3/19.
 */
public class ViewLeaveResponse {

    @SerializedName("result")
    @Expose
    public Boolean result;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;


    public class Datum {

        @SerializedName("leave_id")
        @Expose
        public String leaveId;
        @SerializedName("leave_type")
        @Expose
        public String leaveType;
        @SerializedName("reason")
        @Expose
        public String reason;
        @SerializedName("from_date")
        @Expose
        public String fromDate;
        @SerializedName("to_date")
        @Expose
        public String toDate;
        @SerializedName("created")
        @Expose
        public String created;
        @SerializedName("status")
        @Expose
        public String status;
        @SerializedName("file")
        @Expose
        public List<File> file = null;

    }

    public class File {

        @SerializedName("file")
        @Expose
        public String file;

    }


}
