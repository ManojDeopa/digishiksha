package com.smart.school.parent.responsesP;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by manoj on 27/1/19.
 */
public class NoticeResponse {
    @SerializedName("result")
    @Expose
    public Boolean result;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;

    public class Datum {

        @SerializedName("notice_id")
        @Expose
        public String noticeId;
        @SerializedName("title")
        @Expose
        public String title;
        @SerializedName("notice")
        @Expose
        public String notice;
        @SerializedName("created")
        @Expose
        public String created;
        @SerializedName("file")
        @Expose
        public List<File> file = null;

        public class File {

            @SerializedName("file")
            @Expose
            public String file;

            @SerializedName("file_path")
            @Expose
            public String file_path;

        }


    }
}
