package com.smart.school.parent.responsesP;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by manoj on 24/3/19.
 */
public class AchievementResponse {

    @SerializedName("result")
    @Expose
    public Boolean result;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;


    public class Datum {

        @SerializedName("achievement_id")
        @Expose
        public Integer achievementId;
        @SerializedName("achievement_title")
        @Expose
        public String achievementTitle;
        @SerializedName("files")
        @Expose
        public String files;
        @SerializedName("created_at")
        @Expose
        public String createdAt;
        @SerializedName("bg_img")
        @Expose
        public String bg_img;

    }

}
