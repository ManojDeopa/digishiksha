package com.smart.school.parent.adaptersP;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.parent.responsesP.MyTeacherResponse;
import com.smart.school.utils.Constants;
import com.smart.school.utils.LoginPreferences;
import com.squareup.picasso.Picasso;

import java.util.List;


/**
 * Created by Manoj Singh Deopa.
 */

public class MyTeacherAdapter extends RecyclerView.Adapter<MyTeacherAdapter.ViewHolder> {

    private String type;
    private TextView complaint_to_name;
    private Dialog dialog;
    private List<MyTeacherResponse.Datum> list;
    private Context context;


    public MyTeacherAdapter(List<MyTeacherResponse.Datum> data, String type) {
        this.list = data;
        this.type = type;
    }

    public MyTeacherAdapter(List<MyTeacherResponse.Datum> teacherList, Dialog dialog, TextView complaint_to_name,
                            String type) {
        this.list = teacherList;
        this.dialog = dialog;
        this.complaint_to_name = complaint_to_name;
        this.type = type;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_teacher_adapter, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int i) {

        holder.name.setText(list.get(i).teacherName);
        holder.subject.setText(list.get(i).subject_name);
        holder.designation.setText(list.get(i).designation_name);
        String image = LoginPreferences.getActiveInstance(context).getImgBaseUrl() +
                list.get(i).teacherPhoto;
        Picasso.get().load(image).placeholder(R.drawable.shool_logo).into(holder.image);

        Log.e("ImageUrl--", image);

    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name, designation, subject;
        ImageView image;

        public ViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            subject = view.findViewById(R.id.subject);
            designation = view.findViewById(R.id.designation);
            image = view.findViewById(R.id.image);


            if (type.equals(Constants.TYPE_COMPLAINTS)) {

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        complaint_to_name.setText(list.get(getAdapterPosition()).teacherName);
                        dialog.dismiss();

                    }
                });


            }

        }
    }
}