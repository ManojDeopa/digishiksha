package com.smart.school.parent.adaptersP;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.parent.activityP.ImportantLinksP;
import com.smart.school.parent.activityP.QuickActionDetailsP;
import com.smart.school.parent.responsesP.DashboardTopResponse;
import com.smart.school.utils.Constants;
import com.smart.school.utils.LoginPreferences;
import com.squareup.picasso.Picasso;

import java.util.List;


/**
 * Created by Manoj Singh Deopa .
 */

public class TopGridAdapterP extends RecyclerView.Adapter<TopGridAdapterP.ViewHolder> {
    private List<DashboardTopResponse.Datum> list;
    private Context context;

    public TopGridAdapterP(List<DashboardTopResponse.Datum> data) {
        this.list = data;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_item_adapter, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.key.setText(list.get(position).key);
        holder.value.setText(list.get(position).value);
        Picasso.get()
                .load(LoginPreferences.getActiveInstance(context).getImgBaseUrl()
                        + list.get(position).icon)
                .placeholder(R.drawable.shool_logo).into(holder.image);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView key, value;
        ImageView image;

        public ViewHolder(View view) {
            super(view);
            key = view.findViewById(R.id.key);
            value = view.findViewById(R.id.value);
            image = view.findViewById(R.id.image);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    switch (getAdapterPosition()) {

                        case 0:
                            context.startActivity(new Intent(context, QuickActionDetailsP.class).putExtra(Constants.TYPE, Constants.TYPE_FEES));
                            break;

                        case 1:
                            context.startActivity(new Intent(context, QuickActionDetailsP.class).putExtra(Constants.TYPE, Constants.TYPE_FEES));
                            break;

                        case 2:
                            context.startActivity(new Intent(context, QuickActionDetailsP.class).putExtra(Constants.TYPE, Constants.TYPE_ATTENDANCE));
                            break;

                        case 3:
                            context.startActivity(new Intent(context, ImportantLinksP.class).putExtra(Constants.TYPE, Constants.TYPE_MY_CLASSMATES));
                            break;


                    }


                }
            });
        }
    }

}