package com.smart.school.parent.responsesP;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by manoj on 27/1/19.
 */
public class TimeTableTabResponse {
    @SerializedName("result")
    @Expose
    public Boolean result;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("class_teacher")
    @Expose
    public List<ClassTeacher> classTeacher = null;

    @SerializedName("time_table")
    @Expose
    public List<TimeTable> timeTable = null;

    public class ClassTeacher {

        @SerializedName("key")
        @Expose
        public String key;
        @SerializedName("value")
        @Expose
        public String value;

    }

    public class TimeTable {

        @SerializedName("time")
        @Expose
        public String time;
        @SerializedName("subject")
        @Expose
        public String subject;
        @SerializedName("teacher")
        @Expose
        public String teacher;

    }

}
