package com.smart.school.parent.adaptersP;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.parent.responsesP.SchoolFeeResponse;

import java.util.List;


/**
 * Created by Manoj Singh Deopa.
 */

public class SchoolFeeAdapter extends RecyclerView.Adapter<SchoolFeeAdapter.ViewHolder> {

    private List<SchoolFeeResponse.FeesList> list;
    private Context context;

    public SchoolFeeAdapter(List<SchoolFeeResponse.FeesList> data) {
        this.list = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.school_fee_adapter, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int i) {
        holder.name.setText(list.get(i).feesName + " (" + list.get(i).feesAmt + ")");
        holder.month.setText("(" + list.get(i).feesMonth + ")");
        holder.amount.setText(list.get(i).totalFees);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name, month, amount;

        public ViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            month = view.findViewById(R.id.month);
            amount = view.findViewById(R.id.amount);
        }
    }
}