package com.smart.school.parent.fragmentsP;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.parent.adaptersP.AttendanceDataAdapter;
import com.smart.school.parent.responsesP.AttendanceResponse;
import com.smart.school.utils.AppData;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.Constants;

import java.util.List;


/**
 * Created by Manoj Singh Deopa on 2/4/18.
 */

@SuppressLint("ValidFragment")

public class AttendanceListP extends Fragment {

    RecyclerView recycler_view;
    TextView total_present, total_percent;
    private Context context;
    private View view;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.attendance_list, container, false);
        context = getActivity();
        total_present = view.findViewById(R.id.total_present);
        total_percent = view.findViewById(R.id.total_percent);


        setAttendanceList(AppData.getInstance().getAttendanceData().data);

        total_present.setText(AppData.getInstance().getAttendanceData().totalData.get(0).present);
        total_percent.setText(AppData.getInstance().getAttendanceData().totalData.get(0).percantage);

        return view;

    }

    private void setAttendanceList(List<AttendanceResponse.Datum> data) {

        if (data.size() == 0) {
            LinearLayout linearLayout = view.findViewById(R.id.empty_layout);
            CommonMethod.showEmpty(linearLayout);
            return;
        }

        recycler_view = view.findViewById(R.id.recycler_view);
        CommonMethod.setRecyclerView(recycler_view, context);
        recycler_view.setAdapter(new AttendanceDataAdapter(data));
    }


}
