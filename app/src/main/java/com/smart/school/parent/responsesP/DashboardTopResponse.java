package com.smart.school.parent.responsesP;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by manoj on 27/1/19.
 */
public class DashboardTopResponse {
    @SerializedName("result")
    @Expose
    public Boolean result;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;

    public class Datum {

        @SerializedName("key")
        @Expose
        public String key;
        @SerializedName("value")
        @Expose
        public String value;
        @SerializedName("icon")
        @Expose
        public String icon;

    }
}
