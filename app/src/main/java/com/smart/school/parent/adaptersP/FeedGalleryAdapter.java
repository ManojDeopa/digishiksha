package com.smart.school.parent.adaptersP;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smart.school.R;


/**
 * Created by Manoj Singh Deopa.
 */

public class FeedGalleryAdapter extends RecyclerView.Adapter<FeedGalleryAdapter.ViewHolder> {

    private Context context;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.feed_image_adapter, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int i) {

        if (i == 3) {
            holder.image.setAlpha((float) .5);
            holder.tv_more.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public int getItemCount() {
        return 4;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_more;
        ImageView image;

        public ViewHolder(View view) {
            super(view);
            tv_more = view.findViewById(R.id.tv_more);
            image = view.findViewById(R.id.image);
        }
    }
}