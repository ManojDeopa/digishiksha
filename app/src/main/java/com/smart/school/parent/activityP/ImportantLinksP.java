package com.smart.school.parent.activityP;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.smart.school.R;
import com.smart.school.parent.fragmentsP.AchievementsFragP;
import com.smart.school.parent.fragmentsP.AnnouncementFragP;
import com.smart.school.parent.fragmentsP.CalanderFragP;
import com.smart.school.parent.fragmentsP.ComplaintTabFragment;
import com.smart.school.parent.fragmentsP.GallaryFragP;
import com.smart.school.parent.fragmentsP.MyClassmatesP;
import com.smart.school.parent.fragmentsP.ServiceRequestP;
import com.smart.school.parent.fragmentsP.StudyMaterialFragP;
import com.smart.school.parent.fragmentsP.StudyMaterialGrid;
import com.smart.school.parent.fragmentsP.SubGallaryFrag;
import com.smart.school.utils.AppUtils;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.Constants;

public class ImportantLinksP extends AppCompatActivity {

    private static final String TAG = "ImportantLinksP";
    public static ImportantLinksP baseactivty;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quick_action_details);
        context = ImportantLinksP.this;
        baseactivty = ImportantLinksP.this;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String type = getIntent().getStringExtra(Constants.TYPE);

        setTitle(type);

        setFragment(type);

    }

    public void setTitle(String type) {
        getSupportActionBar().setTitle(type);
    }

    private void setFragment(String type) {
        Fragment fragment = new Fragment();
        switch (type) {
            case Constants.TYPE_MY_CLASSMATES:
                fragment = new MyClassmatesP(Constants.TYPE_MY_CLASSMATES);
                break;

            case Constants.TYPE_MY_TEACHER:
                fragment = new MyClassmatesP(Constants.TYPE_MY_TEACHER);
                break;

            case Constants.TYPE_CALLENDER:
                fragment = new CalanderFragP();
                break;


            case Constants.TYPE_ANNOUNCEMENT:
                fragment = new AnnouncementFragP();
                break;


            case Constants.TYPE_GALLERY:
                fragment = new GallaryFragP(Constants.TYPE_GALLERY);
                break;

            case Constants.TYPE_SUB_GALLERY:
                fragment = new GallaryFragP(Constants.TYPE_SUB_GALLERY);
                break;


            case Constants.TYPE_ALL_GALLERY:
                fragment = new SubGallaryFrag();
                break;

            case Constants.TYPE_SERVICE_REQUEST:
                fragment = new ServiceRequestP();
                break;

            case Constants.TYPE_COMPLAINTS:
                fragment = new ComplaintTabFragment();
                break;


            case Constants.TYPE_STYDY_MATERIAL:
                fragment = new StudyMaterialFragP();
                break;

            case Constants.TYPE_STUDY_MATERIAL_GRID:
                fragment = new StudyMaterialGrid();
                break;

            case Constants.TYPE_MY_ACHIEVEMENTS:
                fragment = new AchievementsFragP();
                break;


            default:
                CommonMethod.showFinishAlert(Constants.SERVER_ERROR, this);
                break;


        }
        AppUtils.setFragment(fragment, true, ImportantLinksP.this, R.id.container);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        finish();
    }


}
