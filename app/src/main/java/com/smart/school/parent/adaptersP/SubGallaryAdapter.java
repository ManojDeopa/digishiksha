package com.smart.school.parent.adaptersP;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.smart.school.R;
import com.smart.school.parent.responsesP.GalleryResponse;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.LoginPreferences;

import java.util.List;


/**
 * Created by Manoj Singh Deopa.
 */

public class SubGallaryAdapter extends RecyclerView.Adapter<SubGallaryAdapter.ViewHolder> {

    private List<GalleryResponse.Datum> list;
    private Context context;

    public SubGallaryAdapter(List<GalleryResponse.Datum> data) {
        this.list = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.gallary_image_adapter, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int i) {

        if (list.get(i).type.equals("video")) {
            /*holder.image.setAlpha(.5f);*/
            holder.btn_play.setVisibility(View.VISIBLE);
        } else {
            holder.btn_play.setVisibility(View.GONE);
        }
        CommonMethod.loadImage(holder.image,
                LoginPreferences.getActiveInstance(context).getImgBaseUrl()
                        + list.get(i).bgImg);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image, btn_play;

        public ViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            btn_play = view.findViewById(R.id.btn_play);
            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (list.get(getAdapterPosition()).type.equals("video")) {
                        CommonMethod.callBrowserIntent(context, list.get(getAdapterPosition()).video_url);
                    } else {
                        CommonMethod.zoomImage(context, LoginPreferences.getActiveInstance(context).getImgBaseUrl() +
                                list.get(getAdapterPosition()).bgImg);
                    }
                }
            });
        }
    }
}