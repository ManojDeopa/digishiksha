package com.smart.school.parent.adaptersP;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.parent.responsesP.SearchBookResponse;

import java.util.List;


/**
 * Created by Manoj Singh Deopa.
 */

public class SearchBookAdapter extends RecyclerView.Adapter<SearchBookAdapter.ViewHolder> {

    private List<SearchBookResponse.Datum> list;
    private Context context;

    public SearchBookAdapter(List<SearchBookResponse.Datum> data) {
        this.list = data;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_book_adapter, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int i) {
        holder.copy.setText("No of Copy  : " + list.get(i).copy);
        holder.isbn_no.setText("Isbn No  : " + list.get(i).isbnNo);
        holder.book.setText("Book : " + list.get(i).bookName);
        holder.alias.setText(list.get(i).alias);

    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView alias, isbn_no, book, copy;

        public ViewHolder(View view) {
            super(view);
            copy = view.findViewById(R.id.copy);
            isbn_no = view.findViewById(R.id.isbn_no);
            book = view.findViewById(R.id.book);
            alias = view.findViewById(R.id.alias);


        }
    }
}