package com.smart.school.parent.responsesP;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by manoj on 24/3/19.
 */
public class SearchBookResponse {
    @SerializedName("result")
    @Expose
    public Boolean result;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;

    public class Datum {

        @SerializedName("book_id")
        @Expose
        public String bookId;
        @SerializedName("isbn_no")
        @Expose
        public String isbnNo;
        @SerializedName("book_name")
        @Expose
        public String bookName;
        @SerializedName("alias")
        @Expose
        public String alias;
        @SerializedName("copy")
        @Expose
        public String copy;

    }
}
