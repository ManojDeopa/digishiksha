package com.smart.school.parent.activityP;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.smart.school.R;
import com.smart.school.parent.responsesP.LoginResponseP;
import com.smart.school.utils.AppData;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.LoginPreferences;

import java.util.List;

public class VerifyNumber2 extends AppCompatActivity {
    String otp_code;
    EditText et_otp;
   /* private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            Log.e("Action--", intent.getAction());
            Log.e("Message--", intent.getStringExtra("message"));

            if (intent.getAction().equalsIgnoreCase("otp")) {
                final String message = intent.getStringExtra("message").trim();
                String otp = message.substring(0, Math.min(message.length(), 6));
                Log.e("Message--", message);
                Log.e("OTP--", otp);
                et_otp.setText(otp);
            }
        }
    };*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.verify_number);
        otp_code = getIntent().getStringExtra("otp_code");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        et_otp = findViewById(R.id.et_otp);
        findViewById(R.id.btn_verify).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(et_otp.getText().toString().trim())) {
                    et_otp.requestFocus();
                    et_otp.setError(getString(R.string.field_required));

                } else if (et_otp.getText().toString().trim().equalsIgnoreCase(otp_code)) {
                    storeData(AppData.getInstance().getChildDataP());
                    startActivity(new Intent(VerifyNumber2.this, ChildAccounts.class));
                    finishAffinity();

                } else {
                    CommonMethod.showAlert("Invalid OTP", VerifyNumber2.this);
                }
            }
        });

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        et_otp.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {

                Log.e("CHAR--", String.valueOf(s));
                if (et_otp.getText().toString().trim().equals(otp_code)) {
                    storeData(AppData.getInstance().getChildDataP());
                    startActivity(new Intent(VerifyNumber2.this, ChildAccounts.class));
                    finishAffinity();
                } else {
                    Log.e("Char:No Match", String.valueOf(s));
                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
            }
        });

    }

    private void storeData(List<LoginResponseP.Datum> childlist) {
        ChildListDatabase mDatabase = new ChildListDatabase(VerifyNumber2.this);
        /*mDatabase.getWritableDatabase().execSQL("delete from " + ChildListDatabase.TABLE_NAME);*/
        for (int i = 0; i < childlist.size(); i++) {
            mDatabase.storeData(
                    childlist.size(),
                    childlist.get(i).studentName,
                    childlist.get(i).studentId,
                    childlist.get(i).admissionNo,
                    childlist.get(i).classId,
                    childlist.get(i).sectionId,
                    childlist.get(i).className,
                    childlist.get(i).sectionName,
                    LoginPreferences.getActiveInstance(VerifyNumber2.this)
                            .getImgBaseUrl()
                            + childlist.get(i).studentPhoto);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

   /* @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }*/

}
