package com.smart.school.parent.responsesP;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by manoj on 26/1/19.
 */
public class MyClassMateResponse {

    @SerializedName("result")
    @Expose
    public Boolean result;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;

    public class Datum {

        @SerializedName("student_name")
        @Expose
        public String studentName;
        @SerializedName("class_name")
        @Expose
        public String className;
        @SerializedName("section_name")
        @Expose
        public String sectionName;
        @SerializedName("student_photo")
        @Expose
        public String studentPhoto;

    }

}
