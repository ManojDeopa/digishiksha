package com.smart.school.parent.adaptersP;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.parent.responsesP.ExamResultResponse;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.LoginPreferences;

import java.util.List;


/**
 * Created by Manoj Singh Deopa.
 */

public class ExamResultAdapter extends RecyclerView.Adapter<ExamResultAdapter.ViewHolder> {

    private List<ExamResultResponse.Datum> list;
    private Context context;

    public ExamResultAdapter(List<ExamResultResponse.Datum> data) {
        this.list = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.exam_result_adapter, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int i) {
        holder.title.setText(list.get(i).examTitle);
        if (list.get(i).status.equals("1")) {
            holder.new_ic.setVisibility(View.VISIBLE);
        } else {
            holder.new_ic.setVisibility(View.GONE);
        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        ImageView file;
        ImageView new_ic;
        String BaseUrl = LoginPreferences.getActiveInstance(context).getImgBaseUrl();

        public ViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.title);
            file = view.findViewById(R.id.file);
            new_ic = view.findViewById(R.id.new_ic);

            file.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CommonMethod.callBrowserIntent(context, BaseUrl + list.get(getAdapterPosition()).fileLink);
                }
            });
        }
    }
}