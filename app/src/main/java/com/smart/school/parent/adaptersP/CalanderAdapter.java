package com.smart.school.parent.adaptersP;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.teacher.responsesT.CalenderResponse;

import java.util.List;


/**
 * Created by Manoj Singh Deopa.
 */

public class CalanderAdapter extends RecyclerView.Adapter<CalanderAdapter.ViewHolder> {

    private List<CalenderResponse.Datum> list;
    private Context context;

    public CalanderAdapter(List<CalenderResponse.Datum> data) {
        this.list = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.holiday_list_adapter, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int i) {
        holder.title.setText(list.get(i).hlTitle);
        holder.description.setText(list.get(i).hlDescription);
        holder.to_date.setText("To" + "\n" + list.get(i).toDate);
        holder.from_date.setText("From" + "\n" + list.get(i).fromDate);


        String status = list.get(i).hlStatus;

        switch (status) {
            case "Holiday":
                holder.status.setBackgroundResource(R.drawable.back_bordor_red);
                break;
            case "":
                holder.status.setVisibility(View.GONE);
                break;
            default:
                holder.status.setBackgroundResource(R.drawable.back_bordor_blue);
                break;
        }
        holder.status.setText(status);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView to_date, from_date, description, title, status;

        public ViewHolder(View view) {
            super(view);
            to_date = view.findViewById(R.id.to_date);
            from_date = view.findViewById(R.id.from_date);
            description = view.findViewById(R.id.description);
            title = view.findViewById(R.id.title);
            status = view.findViewById(R.id.status);

        }
    }
}