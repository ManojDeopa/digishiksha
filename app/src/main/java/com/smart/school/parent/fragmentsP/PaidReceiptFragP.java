package com.smart.school.parent.fragmentsP;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.smart.school.R;
import com.smart.school.parent.adaptersP.ReceiptAdapter;
import com.smart.school.parent.responsesP.SchoolFeeResponse;
import com.smart.school.utils.AppData;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.Constants;

import java.util.List;


/**
 * Created by Manoj Singh Deopa on 2/4/18.
 */

@SuppressLint("ValidFragment")

public class PaidReceiptFragP extends Fragment {

    RecyclerView recycler_view;
    private Context context;
    private View view;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.common_recycler_view, container, false);

        context = getActivity();

        setList(AppData.getInstance().getFeeResponse().paidReceipts);

        return view;

    }

    private void setList(List<SchoolFeeResponse.PaidReceipt> paidReceipts) {

        if (paidReceipts.size() == 0) {
            LinearLayout linearLayout = view.findViewById(R.id.empty_layout);
            CommonMethod.showEmpty(linearLayout);
            return;
        }

        recycler_view = view.findViewById(R.id.recycler_view);
        CommonMethod.setRecyclerView(recycler_view, context);
        recycler_view.setAdapter(new ReceiptAdapter(paidReceipts));
    }

}
