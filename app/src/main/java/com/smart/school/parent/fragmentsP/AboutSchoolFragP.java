package com.smart.school.parent.fragmentsP;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.smart.school.R;
import com.smart.school.parent.responsesP.AboutSchoolResponse;
import com.smart.school.utils.AppData;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.ConstantURL;
import com.smart.school.utils.Constants;
import com.smart.school.utils.LoginPreferences;
import com.smart.school.utils.ProgressD;
import com.smart.school.utils.VolleySingleton;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by Manoj Singh Deopa on 2/4/18.
 */

@SuppressLint("ValidFragment")

public class AboutSchoolFragP extends Fragment {

    WebView text;
    private Context context;
    private View view;
    private String page_id;

    public AboutSchoolFragP(String type) {
        switch (type) {
            case Constants.TYPE_ABOUT_SCHOOL:
                page_id = "1";
                break;
            case Constants.TYPE_SCHOOL_RULE_REGULATIONS:
                page_id = "2";
                break;
            case Constants.TYPE_STUDENT_GUIDANCE:
                page_id = "3";
                break;
            case Constants.TYPE_PARENT_GUIDANCE:
                page_id = "4";
                break;
            case Constants.TYPE_SCHOOL_STAFF:
                page_id = "5";
                break;
            case Constants.TYPE_CONTACT_US:
                page_id = "6";
                break;
            case Constants.TYPE_PAY_SLIP_TC:
                page_id = "7";
                break;
            case Constants.TYPE_TEACHER_GUIDANCE:
                page_id = "8";
                break;
        }
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.about_school_fragment, container, false);
        context = getActivity();
        text = view.findViewById(R.id.text);
        callApi();
        return view;

    }


    private void callApi() {
        String appBaseUrl;
        appBaseUrl = LoginPreferences.getActiveInstance(context).getBASE_URL();
        CommonMethod.print("AboutSchoolFragP---" + appBaseUrl);
        final ProgressD pd = ProgressD.show(context, context.getResources().getString(R.string.connecting), true);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, appBaseUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.dismiss();
                        Log.e("Response--", response);

                        Gson gson = new Gson();
                        AboutSchoolResponse result = gson.fromJson(String.valueOf(response), AboutSchoolResponse.class);

                        if (result.result) {
                            text.loadData(result.data, "text/html", "UTF-8");
                            text.clearCache(true);
                            text.clearHistory();
                            text.getSettings().setJavaScriptEnabled(true);
                            text.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                        Log.e("error--", error.toString());
                        Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Log.e("Page_id--", page_id);
                Map<String, String> params = new HashMap<>();
                params.put("api_page", ConstantURL.ABOUT_SCHOOL);
                params.put("page_id", page_id);
                params.put("school_id", LoginPreferences.getActiveInstance(context).getSchoolId());
                params.put("branch_id", LoginPreferences.getActiveInstance(context).getBranchId());
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }


}
