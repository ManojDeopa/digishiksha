package com.smart.school.parent.adaptersP;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.parent.activityP.ChildListDatabase;
import com.smart.school.parent.activityP.ParentDashboard;
import com.smart.school.parent.responsesP.LoginResponseP;
import com.smart.school.utils.AppData;
import com.smart.school.utils.Constants;
import com.smart.school.utils.LoginPreferences;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


/**
 * Created by Manoj Singh Deopa .
 */

public class ChildListAdapterP extends RecyclerView.Adapter<ChildListAdapterP.ViewHolder> {
    private ArrayList<LoginResponseP.Datum> list;
    private String from;
    private Context context;

    public ChildListAdapterP(ArrayList<LoginResponseP.Datum> childList, String from) {
        this.list = childList;
        this.from = from;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.child_item, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.name.setText(list.get(position).studentName);
        holder.class_name.setText(list.get(position).className + " | " + list.get(position).sectionName);
        Picasso.get().load(list.get(position).studentPhoto).placeholder(R.drawable.shool_logo).into(holder.image);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private void saveData(LoginResponseP.Datum datum) {
        LoginPreferences.getActiveInstance(context).setIsLoggedIn(true);
        LoginPreferences.getActiveInstance(context).setType(Constants.TYPE_PARENT);
        LoginPreferences.getActiveInstance(context).setId(datum.studentId);
        LoginPreferences.getActiveInstance(context).setFullname(datum.studentName);
        LoginPreferences.getActiveInstance(context).setUserImage(datum.studentPhoto);
        LoginPreferences.getActiveInstance(context).setClassSection(datum.className + " (" + datum.sectionName + ")");
        LoginPreferences.getActiveInstance(context).setAdmissionNo(datum.admissionNo);
        LoginPreferences.getActiveInstance(context).setClassId(datum.classId);
        LoginPreferences.getActiveInstance(context).setSectionId(datum.sectionId);


    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image, delete;
        TextView name, class_name;
        LinearLayout child_layout;

        public ViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            delete = view.findViewById(R.id.delete);
            name = view.findViewById(R.id.name);
            class_name = view.findViewById(R.id.class_name);
            child_layout = view.findViewById(R.id.child_layout);

            if (from.equals("inside"))
                delete.setVisibility(View.GONE);

            child_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    saveData(list.get(getAdapterPosition()));
                    AppData.getInstance().setDashboardTopResponse(null);
                    context.startActivity(new Intent(context, ParentDashboard.class));
                }
            });

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("Are you sure to Delete ?");
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            new ChildListDatabase(context).delete(list.get(getAdapterPosition()).id);
                            list.remove(getAdapterPosition());
                            notifyItemRemoved(getAdapterPosition());
                            notifyItemRangeChanged(getAdapterPosition(), list.size());
                        }
                    });
                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            });
        }
    }
}