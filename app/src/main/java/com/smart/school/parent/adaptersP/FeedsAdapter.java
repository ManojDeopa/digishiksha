package com.smart.school.parent.adaptersP;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.utils.CommonMethod;


/**
 * Created by Manoj Singh Deopa.
 */

public class FeedsAdapter extends RecyclerView.Adapter<FeedsAdapter.ViewHolder> {

    private Context context;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.feeds_adapter, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int i) {
        CommonMethod.setGridRecyclerview(context, holder.recycler_view, 2);
        holder.recycler_view.setAdapter(new FeedGalleryAdapter());

    }


    @Override
    public int getItemCount() {
        return 20;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout share, comment_layout, like_layout;
        ImageView image, iv_more, like;

        TextView name, time, description;
        RecyclerView recycler_view;

        public ViewHolder(View view) {
            super(view);
            share = view.findViewById(R.id.share);
            comment_layout = view.findViewById(R.id.comment_layout);
            like_layout = view.findViewById(R.id.like_layout);
            image = view.findViewById(R.id.image);
            iv_more = view.findViewById(R.id.iv_more);

            name = view.findViewById(R.id.name);
            time = view.findViewById(R.id.time);
            description = view.findViewById(R.id.description);
            like = view.findViewById(R.id.like);

            recycler_view = view.findViewById(R.id.recycler_view);


            like_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (like.getTag().equals("inactive")) {
                        like.setImageResource(R.drawable.ic_like_true);
                        like.setTag("active");
                    } else {
                        like.setImageResource(R.drawable.ic_like_false);
                        like.setTag("inactive");
                    }
                }
            });

            share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CommonMethod._shareIntent(context);
                }
            });

        }
    }
}