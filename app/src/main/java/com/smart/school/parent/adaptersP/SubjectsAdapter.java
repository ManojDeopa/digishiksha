package com.smart.school.parent.adaptersP;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.parent.responsesP.AssignmentResponse;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.DirectoryHelper;
import com.smart.school.utils.DownloadService;
import com.smart.school.utils.LoginPreferences;

import java.util.List;


/**
 * Created by Manoj Singh Deopa.
 */

public class SubjectsAdapter extends RecyclerView.Adapter<SubjectsAdapter.ViewHolder> {

    private List<AssignmentResponse.Datum> list;
    private Context context;

    public SubjectsAdapter(List<AssignmentResponse.Datum> data) {
        this.list = data;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.subject_adapter, parent, false);
        context = view.getContext();

        return new ViewHolder(view);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int i) {
        holder.title.setText(list.get(i).assignmentTitle);
        holder.updated_at.setText("Updated On : " + list.get(i).updatedOn);
        holder.assigned_on.setText("Assigned On : " + list.get(i).assignedOn);
        holder.submitted_on.setText("To be Submitted : " + list.get(i).submittedOn);
        holder.updated_by.setText("Updated By : \n" + list.get(i).updated_by);
        holder.description.setText("Description : " + list.get(i).description);

        String image = list.get(i).user_img;
        if (!TextUtils.isEmpty(image)) {
            CommonMethod.loadResizeImage(holder.image, LoginPreferences.getActiveInstance(context).getImgBaseUrl() + image);
        }

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    private void openFile(String filePath) {

        if (TextUtils.isEmpty(filePath)) {
            CommonMethod.showAlert("No File Found", context);
            return;
        }

        CommonMethod.callBrowserIntent(context, filePath);
    }

    private void downloadFile(String filePath, String assignmentTitle, String description) {


        if (TextUtils.isEmpty(filePath)) {
            filePath = "https://image.flaticon.com/icons/png/512/38/38002.png";
        }


        context.startService(DownloadService.getDownloadService(context, filePath, DirectoryHelper.ROOT_DIRECTORY_NAME.concat("/")));


    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView title, updated_at, assigned_on, submitted_on, updated_by, tv_view, tv_download, description;
        ImageView image;

        public ViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.title);
            updated_at = view.findViewById(R.id.updated_at);
            assigned_on = view.findViewById(R.id.assigned_on);
            submitted_on = view.findViewById(R.id.submitted_on);
            tv_view = view.findViewById(R.id.tv_view);
            tv_download = view.findViewById(R.id.tv_download);
            updated_by = view.findViewById(R.id.updated_by);
            image = view.findViewById(R.id.image);
            description = view.findViewById(R.id.description);


            tv_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openFile(list.get(getAdapterPosition()).filePath);
                }
            });

            tv_download.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openFile(list.get(getAdapterPosition()).filePath);
                 /*   downloadFile(list.get(getAdapterPosition()).filePath, list.get(getAdapterPosition()).assignmentTitle,
                            list.get(getAdapterPosition()).description);*/

                }
            });
        }
    }


}