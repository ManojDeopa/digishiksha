package com.smart.school.parent.responsesP;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by manoj on 24/3/19.
 */
public class Libraryresponse {
    @SerializedName("result")
    @Expose
    public Boolean result;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;

    public class Datum {

        @SerializedName("sw_id")
        @Expose
        public String swId;
        @SerializedName("isbn_no")
        @Expose
        public String isbnNo;
        @SerializedName("book")
        @Expose
        public String book;
        @SerializedName("issue_date")
        @Expose
        public String issueDate;
        @SerializedName("due_date")
        @Expose
        public String dueDate;
        @SerializedName("days")
        @Expose
        public String days;
        @SerializedName("late_charge")
        @Expose
        public String lateCharge;
        @SerializedName("renew_date")
        @Expose
        public String renew_date;
        @SerializedName("no_of_renew")
        @Expose
        public String no_of_renew;
        @SerializedName("status")
        @Expose
        public String status;

    }
}
