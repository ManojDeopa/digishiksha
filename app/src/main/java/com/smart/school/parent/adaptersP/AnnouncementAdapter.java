package com.smart.school.parent.adaptersP;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.parent.responsesP.AnnouncementResponse;

import java.util.List;


/**
 * Created by Manoj Singh Deopa.
 */

public class AnnouncementAdapter extends RecyclerView.Adapter<AnnouncementAdapter.ViewHolder> {

    private List<AnnouncementResponse.Datum> list;
    private Context context;

    public AnnouncementAdapter(List<AnnouncementResponse.Datum> data) {
        this.list = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.announcement_adapter, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int i) {
        holder.title.setText(list.get(i).title);
        holder.description.setText(list.get(i).description);
        holder.tag.setText(list.get(i).tagName);
        holder.created_at.setText("Created At:" + list.get(i).createdAt);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView title, description, tag, created_at;

        public ViewHolder(View view) {
            super(view);
            tag = view.findViewById(R.id.tag);
            created_at = view.findViewById(R.id.created_at);
            title = view.findViewById(R.id.title);
            description = view.findViewById(R.id.description);
        }
    }
}