package com.smart.school.parent.responsesP;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by manoj on 16/3/19.
 */
public class ExamResultResponse {
    @SerializedName("result")
    @Expose
    public Boolean result;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;

    public class Datum {

        @SerializedName("exam_title")
        @Expose
        public String examTitle;
        @SerializedName("file_link")
        @Expose
        public String fileLink;

        @SerializedName("new")
        @Expose
        public String status;

    }


}
