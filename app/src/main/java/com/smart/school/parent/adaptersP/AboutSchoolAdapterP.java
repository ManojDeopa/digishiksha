package com.smart.school.parent.adaptersP;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.parent.activityP.AboutSchoolP;
import com.smart.school.utils.AppData;
import com.smart.school.utils.Constants;
import com.squareup.picasso.Picasso;


/**
 * Created by Manoj Singh Deopa .
 */

public class AboutSchoolAdapterP extends RecyclerView.Adapter<AboutSchoolAdapterP.ViewHolder> {
    private Context context;
    private String[] list = {
            Constants.TYPE_ABOUT_SCHOOL,
            Constants.TYPE_SCHOOL_RULE_REGULATIONS,
            Constants.TYPE_STUDENT_GUIDANCE,
            Constants.TYPE_PARENT_GUIDANCE,
            Constants.TYPE_SCHOOL_STAFF,
            Constants.TYPE_CONTACT_US,
    };

    private int[] list_image = {
            R.drawable.about_school_p,
            R.drawable.school_rule_regulations,
            R.drawable.student_guidence,
            R.drawable.parent_guidance_p,
            R.drawable.school_staff_p,
            R.drawable.contact_us_p
    };

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dashboard_item_layout, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.tv_title.setText(list[position]);
        Picasso.get().load(list_image[position]).into(holder.iv_image);

    }

    @Override
    public int getItemCount() {
        return list.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_title;
        private ImageView iv_image;
        private LinearLayout parent;

        public ViewHolder(View view) {
            super(view);
            iv_image = view.findViewById(R.id.iv_image);
            tv_title = view.findViewById(R.id.tv_title);
            parent = view.findViewById(R.id.parent);

            parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String type = list[getAdapterPosition()].trim();
                    context.startActivity(new Intent(context, AboutSchoolP.class).putExtra(Constants.TYPE, type));
                }
            });

        }
    }

}