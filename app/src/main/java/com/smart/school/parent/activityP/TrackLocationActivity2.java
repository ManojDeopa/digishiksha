package com.smart.school.parent.activityP;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.smart.school.R;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.DrawPath;

public class TrackLocationActivity2 extends AppCompatActivity implements OnMapReadyCallback {
    public static final String TAG = TrackLocationActivity2.class.getSimpleName();
    public static TrackLocationActivity2 activity;
    public GoogleMap map;
    public Context context;
    public double destinationLat = 28.465210, destinationLong = 77.510960;
    public double currentLatitude = 28.559660, currentLongitude = 77.461500;


    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_location);

        context = TrackLocationActivity2.this;
        activity = TrackLocationActivity2.this;

        setHeader();

        FloatingActionButton showDirection = findViewById(R.id.currentLocationImageButton);
        showDirection.setVisibility(View.VISIBLE);
        showDirection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(String.valueOf(currentLatitude).trim())) {
                    showMapFromLocation();
                }
            }
        });

        initGMaps();


    }


    private void showMapFromLocation() {
        try {
            if (CommonMethod.isOnline(context)) {

                String desLocation = "&daddr=" + Double.toString(destinationLat) + "," + Double.toString(destinationLong);
                String currLocation = "saddr=" + Double.toString(currentLatitude) + "," + Double.toString(currentLongitude);
                // "d" means driving car, "w" means walking "r" means by bus
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?" + currLocation + desLocation + "&dirflg=d"));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK & Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                startActivity(intent);
            }
        } catch (Exception e) {
            Log.e(TAG, "Error when showing google map directions, E: " + e);
        }
    }


    private void setHeader() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Track");
    }


    private void initGMaps() {
        SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapFragment);
        supportMapFragment.getMapAsync(this);
    }

    // Callback called when Map is ready
    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.e(TAG, "onMapReady()");
        map = googleMap;
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLatitude, currentLongitude), 20.0f));
        map.setMyLocationEnabled(true);
        map.getUiSettings().setCompassEnabled(true);
        new DrawPath(activity);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}

