package com.smart.school.parent.adaptersP;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.parent.responsesP.MyClassMateResponse;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.LoginPreferences;
import com.squareup.picasso.Picasso;

import java.util.List;


/**
 * Created by Manoj Singh Deopa.
 */

public class MyClassMatesAdapter extends RecyclerView.Adapter<MyClassMatesAdapter.ViewHolder> {

    private List<MyClassMateResponse.Datum> list;
    private Context context;

    public MyClassMatesAdapter(List<MyClassMateResponse.Datum> data) {
        this.list = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.classmates_adapter, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int i) {

        holder.name.setText(list.get(i).studentName);
        holder.class_sec.setText(list.get(i).className);
        String image = LoginPreferences.getActiveInstance(context).getImgBaseUrl() +
                list.get(i).studentPhoto;

        CommonMethod.loadResizeImage(holder.image, image);
        Log.e("ImageUrl--", image);

    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name, class_sec;
        ImageView image;

        public ViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            class_sec = view.findViewById(R.id.class_sec);
            image = view.findViewById(R.id.image);
        }
    }
}