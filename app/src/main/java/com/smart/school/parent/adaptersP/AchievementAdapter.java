package com.smart.school.parent.adaptersP;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.parent.responsesP.AchievementResponse;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.LoginPreferences;

import java.util.List;


/**
 * Created by Manoj Singh Deopa.
 */

public class AchievementAdapter extends RecyclerView.Adapter<AchievementAdapter.ViewHolder> {

    private List<AchievementResponse.Datum> list;
    private Context context;

    public AchievementAdapter(List<AchievementResponse.Datum> data) {
        this.list = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.achievement_adapter, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int i) {
        holder.title.setText(list.get(i).achievementTitle);
        CommonMethod.loadImage(holder.image, LoginPreferences.getActiveInstance(context).getImgBaseUrl() + list.get(i).bg_img);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        ImageView image;

        public ViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            title = view.findViewById(R.id.title);
            view.findViewById(R.id.download_certificate).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CommonMethod.callBrowserIntent(context, LoginPreferences.getActiveInstance(context).getImgBaseUrl() + list.get(getAdapterPosition()).files);
                }
            });

            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CommonMethod.zoomImage(context, LoginPreferences.getActiveInstance(context).getImgBaseUrl() + list.get(getAdapterPosition()).bg_img);
                }
            });


        }
    }
}