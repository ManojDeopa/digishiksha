package com.smart.school.parent.fragmentsP;

/**
 * Created by manoj on 26/1/19.
 */


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smart.school.R;

import java.util.Calendar;


public class TimeTableTabFragment extends Fragment implements ViewPager.OnPageChangeListener, TabLayout.OnTabSelectedListener {
    public static TextView teacherName;
    View view;
    Context context;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private String[] dayList = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
    private int dayposition;

    public TimeTableTabFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.timetable_tab_layout, container, false);
        context = getActivity();
        teacherName = view.findViewById(R.id.teacherName);

        getCurrentDay();

        setUpView();


        return view;
    }

    private void getCurrentDay() {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        switch (day) {
            case Calendar.SUNDAY:
                dayposition = 0;
                break;
            case Calendar.MONDAY:
                dayposition = 1;
                break;
            case Calendar.TUESDAY:
                dayposition = 2;
                break;
            case Calendar.WEDNESDAY:
                dayposition = 3;
                break;
            case Calendar.THURSDAY:
                dayposition = 4;
                break;
            case Calendar.FRIDAY:
                dayposition = 5;
                break;
            case Calendar.SATURDAY:
                dayposition = 6;
                break;
        }
    }


    private void setUpView() {
        tabLayout = view.findViewById(R.id.tabLayout);

        for (int i = 0; i < dayList.length; i++) {
            tabLayout.addTab(tabLayout.newTab().setText(dayList[i]));
        }
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.getTabAt(dayposition).select();
        viewPager = view.findViewById(R.id.pager);
        Pager adapter = new Pager(getActivity().getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);

        viewPager.setCurrentItem(dayposition);
        tabLayout.addOnTabSelectedListener(this);
        viewPager.addOnPageChangeListener(this);
    }


    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {
        tabLayout.getTabAt(i).select();
    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }


    public class Pager extends FragmentStatePagerAdapter {
        int tabCount;

        public Pager(FragmentManager fm, int tabCount) {
            super(fm);
            this.tabCount = tabCount;
        }

        @Override
        public Fragment getItem(int position) {
            return new TimeTableFragP(dayList[position]);
        }

        @Override
        public int getCount() {
            return tabCount;
        }
    }

}

