package com.smart.school.parent.responsesP;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by manoj on 23/3/19.
 */
public class AnnouncementResponse {

    @SerializedName("result")
    @Expose
    public Boolean result;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;


    public class Datum {

        @SerializedName("tbl_id")
        @Expose
        public Integer tblId;
        @SerializedName("title")
        @Expose
        public String title;
        @SerializedName("description")
        @Expose
        public String description;
        @SerializedName("tag_name")
        @Expose
        public String tagName;
        @SerializedName("file")
        @Expose
        public String file;
        @SerializedName("created_at")
        @Expose
        public String createdAt;

    }

}
