package com.smart.school.parent.responsesP;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by manoj on 13/1/19.
 */
public class LoginResponseP {
    @SerializedName("result")
    @Expose
    public Boolean result;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("otp_code")
    @Expose
    public String otpCode;
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;


    public static class Datum {

        @SerializedName("student_id")
        @Expose
        public String studentId;
        @SerializedName("admission_no")
        @Expose
        public String admissionNo;
        @SerializedName("class_id")
        @Expose
        public String classId;
        @SerializedName("section_id")
        @Expose
        public String sectionId;
        @SerializedName("student_name")
        @Expose
        public String studentName;
        @SerializedName("class_name")
        @Expose
        public String className;
        @SerializedName("section_name")
        @Expose
        public String sectionName;
        @SerializedName("student_photo")
        @Expose
        public String studentPhoto;
        public int id;

        public Datum(int id,
                     String studentName, String studentId,
                     String admissionNo, String classId,
                     String sectionId, String className,
                     String sectionName, String studentPhoto) {
            this.id = id;
            this.studentName = studentName;
            this.studentId = studentId;
            this.admissionNo = admissionNo;
            this.classId = classId;
            this.sectionId = sectionId;
            this.className = className;
            this.sectionName = sectionName;
            this.studentPhoto = studentPhoto;

        }
    }
}
