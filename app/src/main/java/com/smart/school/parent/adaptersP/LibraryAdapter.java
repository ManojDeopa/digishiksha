package com.smart.school.parent.adaptersP;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.parent.responsesP.Libraryresponse;

import java.util.List;


/**
 * Created by Manoj Singh Deopa.
 */

public class LibraryAdapter extends RecyclerView.Adapter<LibraryAdapter.ViewHolder> {

    private String pageNo;
    private List<Libraryresponse.Datum> list;
    private Context context;


    public LibraryAdapter(List<Libraryresponse.Datum> data, String pageNo) {
        this.pageNo = pageNo;
        this.list = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.library_adapter, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int i) {
        holder.issue_id.setText("Issue Id  : " + list.get(i).swId);
        holder.isbn_no.setText("Isbn No  : " + list.get(i).isbnNo);
        holder.book.setText("Book : " + list.get(i).book);
        holder.issue_date.setText("Issue Date : " + list.get(i).issueDate);
        holder.renew_date.setText("Renew Date : " + list.get(i).renew_date);
        holder.due_date.setText("Return Date : " + list.get(i).dueDate);
        holder.days.setText("Days : " + list.get(i).days);
        holder.late_charge.setText("Late Charge : " + list.get(i).lateCharge);
        holder.no_of_renew.setText("No of Renew : " + list.get(i).no_of_renew);
        holder.status.setText("Status : " + list.get(i).status);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView issue_id, isbn_no, book, issue_date, renew_date, due_date, days, late_charge, no_of_renew, status;

        public ViewHolder(View view) {
            super(view);
            issue_id = view.findViewById(R.id.issue_id);
            isbn_no = view.findViewById(R.id.isbn_no);
            book = view.findViewById(R.id.book);
            issue_date = view.findViewById(R.id.issue_date);
            renew_date = view.findViewById(R.id.renew_date);
            due_date = view.findViewById(R.id.due_date);
            days = view.findViewById(R.id.days);
            late_charge = view.findViewById(R.id.late_charge);
            no_of_renew = view.findViewById(R.id.no_of_renew);
            status = view.findViewById(R.id.status);

            if (pageNo.equals("2")) {
                days.setVisibility(View.GONE);
                late_charge.setVisibility(View.GONE);
            } else {
                no_of_renew.setVisibility(View.GONE);
            }

        }
    }
}