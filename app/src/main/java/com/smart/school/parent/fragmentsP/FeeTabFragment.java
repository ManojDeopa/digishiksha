package com.smart.school.parent.fragmentsP;

/**
 * Created by manoj on 26/1/19.
 */


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.smart.school.R;
import com.smart.school.parent.responsesP.SchoolFeeResponse;
import com.smart.school.utils.AppData;
import com.smart.school.utils.ConstantURL;
import com.smart.school.utils.LoginPreferences;
import com.smart.school.utils.ProgressD;
import com.smart.school.utils.VolleySingleton;

import java.util.HashMap;
import java.util.Map;


public class FeeTabFragment extends Fragment implements ViewPager.OnPageChangeListener, TabLayout.OnTabSelectedListener {
    View view;
    Context context;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    public FeeTabFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fee_tab_layout, container, false);
        context = getActivity();

        callApi();

        return view;
    }


    private void callApi() {
        final String appBaseUrl = LoginPreferences.getActiveInstance(context).getUserBaseUrl();
        Log.e("URL--", appBaseUrl);
        final ProgressD pd = ProgressD.show(context, context.getResources().getString(R.string.connecting), true);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, appBaseUrl + ConstantURL.APP_API,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.dismiss();
                        Log.e("Response--", response);

                        Gson gson = new Gson();
                        SchoolFeeResponse result = gson.fromJson(String.valueOf(response), SchoolFeeResponse.class);

                        if (result.result) {
                            AppData.getInstance().setFeeResponse(result);
                            setUpView();

                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                        Log.e("error--", error.toString());
                        Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("api_page", ConstantURL.SCHOOL_FEES);
                params.put("session_id", LoginPreferences.getActiveInstance(context).getSessionId());
                params.put("school_id", LoginPreferences.getActiveInstance(context).getSchoolId());
                params.put("branch_id", LoginPreferences.getActiveInstance(context).getBranchId());
                params.put("student_id", LoginPreferences.getActiveInstance(context).getId());
                params.put("class_id", LoginPreferences.getActiveInstance(context).getClassId());
                params.put("section_id", LoginPreferences.getActiveInstance(context).getSectionId());
                params.put("fees_month", "January");
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }

    private void setUpView() {
        tabLayout = view.findViewById(R.id.tabLayout);
        tabLayout.addTab(tabLayout.newTab().setText("School Fee"));
        tabLayout.addTab(tabLayout.newTab().setText("Paid Receipt"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        viewPager = view.findViewById(R.id.pager);
        Pager adapter = new Pager(getActivity().getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        tabLayout.addOnTabSelectedListener(this);
        viewPager.addOnPageChangeListener(this);
    }


    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {
        tabLayout.getTabAt(i).select();
    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }


    public class Pager extends FragmentStatePagerAdapter {
        int tabCount;

        public Pager(FragmentManager fm, int tabCount) {
            super(fm);
            this.tabCount = tabCount;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new SchoolFeeP();
                case 1:
                    return new PaidReceiptFragP();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return tabCount;
        }
    }

}

