package com.smart.school.parent.fragmentsP;

/**
 * Created by manoj on 26/1/19.
 */


import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.utils.AppUtils;


public class AssignmentTabFragment extends Fragment {
    View view;
    Context context;
    private TextView subject, all;

    public AssignmentTabFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.assignment_layout, container, false);
        context = getActivity();

        setUpView();


        return view;
    }


    private void setUpView() {
        all = view.findViewById(R.id.all);
        subject = view.findViewById(R.id.subject);

        updateView(true);

        AppUtils.setFragment(new SubjectFragment("0"), true, getActivity(), R.id.container2);


        all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateView(true);
                AppUtils.setFragment(new SubjectFragment("0"), true, getActivity(), R.id.container2);
            }
        });


        subject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateView(false);
                AppUtils.setFragment(new SubjectTabFragment(), true, getActivity(), R.id.container2);
            }
        });

    }


    void updateView(boolean isAllSelected) {

        if(isAllSelected){
            all.setTextColor(context.getResources().getColor(R.color.white));
            all.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));

            subject.setTextColor(context.getResources().getColor(R.color.black));
            subject.setBackgroundColor(context.getResources().getColor(R.color.back_color));

        }else {

            subject.setTextColor(context.getResources().getColor(R.color.white));
            subject.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));

            all.setTextColor(context.getResources().getColor(R.color.black));
            all.setBackgroundColor(context.getResources().getColor(R.color.back_color));
        }



    }

}

