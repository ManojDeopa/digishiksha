package com.smart.school.parent.adaptersP;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.parent.responsesP.SchoolFeeResponse;

import java.util.List;


/**
 * Created by Manoj Singh Deopa.
 */

public class ReceiptAdapter extends RecyclerView.Adapter<ReceiptAdapter.ViewHolder> {

    private List<SchoolFeeResponse.PaidReceipt> list;
    private Context context;

    public ReceiptAdapter(List<SchoolFeeResponse.PaidReceipt> data) {
        this.list = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.receipt_adapter, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int i) {
        holder.receipt_no.setText("Receipt No  : " + list.get(i).receiptNo);
        holder.payable_fees.setText("Payable Fee : " + list.get(i).payableFee);
        holder.paid_fees.setText("Paid Fee : " + list.get(i).paidFee);
        holder.balance.setText("Balance : " + list.get(i).balance);
        holder.payment_date.setText("Date of Payment : " + list.get(i).paymentDate);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView receipt_no, payable_fees, paid_fees, balance, payment_date;

        public ViewHolder(View view) {
            super(view);
            receipt_no = view.findViewById(R.id.receipt_no);
            payable_fees = view.findViewById(R.id.payable_fees);
            paid_fees = view.findViewById(R.id.paid_fees);
            balance = view.findViewById(R.id.balance);
            payment_date = view.findViewById(R.id.payment_date);
        }
    }
}