package com.smart.school.parent.adaptersP;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.parent.activityP.ImportantLinksP;
import com.smart.school.parent.responsesP.GalleryResponse;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.Constants;
import com.smart.school.utils.LoginPreferences;

import java.util.List;


/**
 * Created by Manoj Singh Deopa.
 */

public class GallaryAdapterP extends RecyclerView.Adapter<GallaryAdapterP.ViewHolder> {

    private String page_no;
    private List<GalleryResponse.Datum> list;
    private Context context;

    public GallaryAdapterP(List<GalleryResponse.Datum> data, String page_no) {
        this.list = data;
        this.page_no = page_no;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.gallary_adapter, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int i) {

        holder.title.setText(list.get(i).mediaName);
        holder.sub_title.setText(list.get(i).alias);

        CommonMethod.loadImage(holder.image,
                LoginPreferences.getActiveInstance(context).getImgBaseUrl()
                        + list.get(i).bgImg);

    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView title, sub_title;
        ImageView image;

        public ViewHolder(View view) {
            super(view);

            title = view.findViewById(R.id.title);
            sub_title = view.findViewById(R.id.sub_title);
            image = view.findViewById(R.id.image);

            switch (page_no) {
                case "2":
                    sub_title.setVisibility(View.GONE);
                    itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            context.startActivity(new Intent(context, ImportantLinksP.class).putExtra(Constants.TYPE, Constants.TYPE_ALL_GALLERY));
                        }
                    });
                    break;
                case "1":
                    sub_title.setVisibility(View.VISIBLE);
                    itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            context.startActivity(new Intent(context, ImportantLinksP.class).putExtra(Constants.TYPE, Constants.TYPE_SUB_GALLERY));
                        }
                    });
                    break;
                default:
                    sub_title.setVisibility(View.VISIBLE);
                    break;
            }
        }
    }
}