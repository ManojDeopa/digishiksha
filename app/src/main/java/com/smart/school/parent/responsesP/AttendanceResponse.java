package com.smart.school.parent.responsesP;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by manoj on 10/3/19.
 */
public class AttendanceResponse {
    @SerializedName("result")
    @Expose
    public Boolean result;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("absent_date")
    @Expose
    public List<AbsentDate> absentDate = null;
    @SerializedName("attendance")
    @Expose
    public List<Attendance> attendance = null;
    @SerializedName("attendance_month")
    @Expose
    public List<Object> attendanceMonth = null;
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;
    @SerializedName("total_data")
    @Expose
    public List<TotalDatum> totalData = null;

    public class AbsentDate {

        @SerializedName("absent_date")
        @Expose
        public String absentDate;

    }

    public class Attendance {

        @SerializedName("total")
        @Expose
        public Integer total;
        @SerializedName("present")
        @Expose
        public Integer present;
        @SerializedName("absent")
        @Expose
        public Integer absent;
        @SerializedName("late")
        @Expose
        public Integer late;

    }


    public class Datum {

        @SerializedName("month")
        @Expose
        public String month;
        @SerializedName("present")
        @Expose
        public String present;
        @SerializedName("percantage")
        @Expose
        public String percantage;

    }

    public class TotalDatum {

        @SerializedName("present")
        @Expose
        public String present;
        @SerializedName("percantage")
        @Expose
        public String percantage;

    }
}
