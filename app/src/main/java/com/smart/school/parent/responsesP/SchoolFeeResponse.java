package com.smart.school.parent.responsesP;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by manoj on 26/1/19.
 */
public class SchoolFeeResponse {
    @SerializedName("result")
    @Expose
    public Boolean result;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("fees_title")
    @Expose
    public List<FeesTitle> feesTitle = null;
    @SerializedName("fees_list")
    @Expose
    public List<FeesList> feesList = null;
    @SerializedName("fees_total")
    @Expose
    public List<FeesTotal> feesTotal = null;
    @SerializedName("paid_receipts")
    @Expose
    public List<PaidReceipt> paidReceipts = null;

    public class FeesList {

        @SerializedName("fees_name")
        @Expose
        public String feesName;
        @SerializedName("fees_month")
        @Expose
        public String feesMonth;
        @SerializedName("fees_amt")
        @Expose
        public String feesAmt;
        @SerializedName("total_fees")
        @Expose
        public String totalFees;

    }

    public class FeesTitle {

        @SerializedName("total_bal")
        @Expose
        public String totalBal;
        @SerializedName("late_fine")
        @Expose
        public String lateFine;
        @SerializedName("discount")
        @Expose
        public String discount;

    }

    public class FeesTotal {

        @SerializedName("Total Balance")
        @Expose
        public String totalBalance;

    }

    public class PaidReceipt {

        @SerializedName("receipt_no")
        @Expose
        public String receiptNo;
        @SerializedName("payment_date")
        @Expose
        public String paymentDate;
        @SerializedName("payable_fee")
        @Expose
        public String payableFee;
        @SerializedName("paid_fee")
        @Expose
        public String paidFee;
        @SerializedName("balance")
        @Expose
        public String balance;

    }
}
