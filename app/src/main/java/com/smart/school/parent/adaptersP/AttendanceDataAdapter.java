package com.smart.school.parent.adaptersP;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.parent.responsesP.AttendanceResponse;

import java.util.List;


/**
 * Created by Manoj Singh Deopa.
 */

public class AttendanceDataAdapter extends RecyclerView.Adapter<AttendanceDataAdapter.ViewHolder> {

    private List<AttendanceResponse.Datum> list;
    private Context context;

    public AttendanceDataAdapter(List<AttendanceResponse.Datum> data) {
        this.list = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.attendance_data_adapter, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int i) {

        holder.month.setText(list.get(i).month);
        holder.percent.setText(list.get(i).present);
        holder.percentage.setText(list.get(i).percantage);

    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView month, percent, percentage;

        public ViewHolder(View view) {
            super(view);
            month = view.findViewById(R.id.month);
            percent = view.findViewById(R.id.percent);
            percentage = view.findViewById(R.id.percentage);

        }
    }
}