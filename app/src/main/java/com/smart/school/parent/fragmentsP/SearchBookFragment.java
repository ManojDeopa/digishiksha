package com.smart.school.parent.fragmentsP;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.smart.school.R;
import com.smart.school.parent.adaptersP.SearchBookAdapter;
import com.smart.school.parent.responsesP.SearchBookResponse;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.ConstantURL;
import com.smart.school.utils.Constants;
import com.smart.school.utils.LoginPreferences;
import com.smart.school.utils.ProgressD;
import com.smart.school.utils.VolleySingleton;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Manoj Singh Deopa on 2/4/18.
 */

@SuppressLint("ValidFragment")

public class SearchBookFragment extends Fragment {

    RecyclerView recycler_view;
    private Context context;
    private View view;


    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.search_book_fragment, container, false);
        context = getActivity();
        callApi();
        return view;

    }


    private void callApi() {
        final String appBaseUrl = LoginPreferences.getActiveInstance(context).getUserBaseUrl();
        Log.e("URL--", appBaseUrl);
        final ProgressD pd = ProgressD.show(context, getString(R.string.connecting), true);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, appBaseUrl + ConstantURL.APP_API,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.dismiss();
                        Log.e("Response--", response);
                        Gson gson = new Gson();
                        SearchBookResponse result = gson.fromJson(String.valueOf(response), SearchBookResponse.class);
                        if (result.result) {
                            if (result.data != null) {
                                setRecyclerViewData(result.data);
                            }
                        } else {
                            CommonMethod.showAlert(result.message, context);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                        Log.e("error--", error.toString());
                        Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                /*Log.e("Page-", pageNo);*/
                Map<String, String> params = new HashMap<>();
                params.put("api_page", ConstantURL.LIBRARY);
                params.put("page_no", "3");
                params.put("session_id", LoginPreferences.getActiveInstance(context).getSessionId());
                params.put("school_id", LoginPreferences.getActiveInstance(context).getSchoolId());
                params.put("branch_id", LoginPreferences.getActiveInstance(context).getBranchId());
                params.put("student_id", LoginPreferences.getActiveInstance(context).getId());
                params.put("class_id", LoginPreferences.getActiveInstance(context).getClassId());
                params.put("section_id", LoginPreferences.getActiveInstance(context).getSectionId());
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }


    private void setRecyclerViewData(List<SearchBookResponse.Datum> data) {

        if (data.size() == 0) {
            LinearLayout linearLayout = view.findViewById(R.id.empty_layout);
            CommonMethod.showEmpty(linearLayout);
            return;
        }

        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        CommonMethod.setRecyclerView(recyclerView, context);
        recyclerView.setAdapter(new SearchBookAdapter(data));
    }


}
