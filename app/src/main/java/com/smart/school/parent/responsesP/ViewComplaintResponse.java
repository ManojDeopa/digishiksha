package com.smart.school.parent.responsesP;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by manoj on 24/3/19.
 */
public class ViewComplaintResponse {
    @SerializedName("result")
    @Expose
    public Boolean result;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;

    public class Datum {

        @SerializedName("complaint_id")
        @Expose
        public String complaintId;
        @SerializedName("to")
        @Expose
        public String to;
        @SerializedName("regarding")
        @Expose
        public String regarding;
        @SerializedName("text")
        @Expose
        public String text;
        @SerializedName("reply_text")
        @Expose
        public String replyText;
        @SerializedName("created_at")
        @Expose
        public String createdAt;
        @SerializedName("updated_at")
        @Expose
        public String updatedAt;
        @SerializedName("status")
        @Expose
        public String status;

    }
}
