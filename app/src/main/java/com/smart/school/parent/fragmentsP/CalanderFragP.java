package com.smart.school.parent.fragmentsP;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.smart.school.R;
import com.smart.school.parent.adaptersP.CalanderAdapter;
import com.smart.school.teacher.responsesT.CalenderResponse;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.ConstantURL;
import com.smart.school.utils.Constants;
import com.smart.school.utils.LoginPreferences;
import com.smart.school.utils.ProgressD;
import com.smart.school.utils.VolleySingleton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Manoj Singh Deopa on 2/4/18.
 */

@SuppressLint("ValidFragment")

public class CalanderFragP extends Fragment {

    RecyclerView recycler_view;
    Spinner spinner_month, spinner_year;
    private Context context;
    private View view;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.calander_frag_p, container, false);

        context = getActivity();

        setSpinnser();

        callApi();

        return view;

    }


    private void callApi() {
        final String appBaseUrl = LoginPreferences.getActiveInstance(context).getUserBaseUrl();
        Log.e("URL--", appBaseUrl);
        final ProgressD pd = ProgressD.show(context, context.getResources().getString(R.string.connecting), true);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, appBaseUrl + ConstantURL.APP_API,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.dismiss();
                        Log.e("Response--", response);
                        Gson gson = new Gson();
                        CalenderResponse result = gson.fromJson(String.valueOf(response), CalenderResponse.class);
                        if (result.result) {
                            if (result.data != null) {
                                setList(result.data);

                            }
                        } else {
                            CommonMethod.showAlert(result.message, context);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                        Log.e("error--", error.toString());
                        Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("api_page", ConstantURL.CALENDER);
                params.put("session_id", LoginPreferences.getActiveInstance(context).getSessionId());
                params.put("school_id", LoginPreferences.getActiveInstance(context).getSchoolId());
                params.put("branch_id", LoginPreferences.getActiveInstance(context).getBranchId());
                params.put("student_id", LoginPreferences.getActiveInstance(context).getId());
                params.put("class_id", LoginPreferences.getActiveInstance(context).getClassId());
                params.put("section_id", LoginPreferences.getActiveInstance(context).getSectionId());
                params.put("month", spinner_month.getSelectedItem().toString());
                params.put("year", spinner_year.getSelectedItem().toString());
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }


    private void setSpinnser() {
        spinner_month = view.findViewById(R.id.spinner_month);
        spinner_year = view.findViewById(R.id.spinner_year);

        Calendar calendar = Calendar.getInstance();
        int currentMonth = calendar.get(Calendar.MONTH);
        String currentyear = String.valueOf(calendar.get(Calendar.YEAR));
        Log.e("currentyear-", String.valueOf(currentyear));

        String monthArr[] = getResources().getStringArray(R.array.month);
        ArrayAdapter<String> monthAdpter = new ArrayAdapter<String>(context,
                android.R.layout.simple_spinner_dropdown_item, Arrays.asList(monthArr));
        monthAdpter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_month.setAdapter(monthAdpter);
        spinner_month.setSelection(currentMonth);

        List<String> year = new ArrayList<String>();
        year.add("2018");
        year.add("2019");
        year.add("2020");
        ArrayAdapter<String> yearAdpter = new ArrayAdapter<String>(context,
                android.R.layout.simple_spinner_dropdown_item, year);
        yearAdpter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_year.setAdapter(yearAdpter);

        int spinnerPosition = yearAdpter.getPosition(currentyear);
        spinner_year.setSelection(spinnerPosition);

    }

    private void setList(List<CalenderResponse.Datum> data) {

        spinner_month.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                callApi();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinner_year.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                callApi();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        if (data.size() == 0) {
            LinearLayout linearLayout = view.findViewById(R.id.empty_layout);
            CommonMethod.showEmpty(linearLayout);
            return;
        }

        recycler_view = view.findViewById(R.id.recycler_view);
        CommonMethod.setRecyclerView(recycler_view, context);
        recycler_view.setAdapter(new CalanderAdapter(data));
    }

}
