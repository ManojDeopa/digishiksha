package com.smart.school.parent.adaptersP;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.parent.responsesP.ViewComplaintResponse;

import java.util.List;


/**
 * Created by Manoj Singh Deopa.
 */

public class ViewComplaintsAdapter extends RecyclerView.Adapter<ViewComplaintsAdapter.ViewHolder> {

    private List<ViewComplaintResponse.Datum> list;
    private Context context;

    public ViewComplaintsAdapter(List<ViewComplaintResponse.Datum> data) {
        this.list = data;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_complaint_adapter, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int i) {
        holder.to.setText("To  : " + list.get(i).to);
        holder.created_at.setText("Created At  : " + list.get(i).createdAt);
        holder.regarding.setText("Regarding : " + list.get(i).regarding);
        holder.complaint.setText("Complaint :-\n" + list.get(i).text);
        holder.reply.setText("Reply :-\n" + list.get(i).replyText);
        holder.closed_at.setText("Status : " + list.get(i).status);

        if (list.get(i).status.equals("open")) {
            holder.closed_at.setTextColor(context.getResources().getColor(R.color.green));
        } else {
            holder.closed_at.setTextColor(context.getResources().getColor(R.color.red));
        }

    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView to, created_at, regarding, complaint, reply, closed_at;

        public ViewHolder(View view) {
            super(view);
            complaint = view.findViewById(R.id.complaint);
            created_at = view.findViewById(R.id.created_at);
            regarding = view.findViewById(R.id.regarding);
            to = view.findViewById(R.id.to);
            reply = view.findViewById(R.id.reply);

            to = view.findViewById(R.id.to);
            closed_at = view.findViewById(R.id.closed_at);


        }
    }
}