package com.smart.school.parent.fragmentsP;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.smart.school.R;
import com.smart.school.parent.activityP.QuickActionDetailsP;
import com.smart.school.teacher.interfacesT.DashboardBottomItemClickListener;
import com.smart.school.utils.Constants;


/**
 * Created by Manoj on 2/4/18.
 */

@SuppressLint("ValidFragment")

public class DashBoardP extends Fragment implements BottomNavigationView.OnNavigationItemSelectedListener {

    private DashboardBottomItemClickListener listener;
    private RecyclerView recyclerView;


    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.parent_dashboard_fragment, container, false);

        loadFragment(new HomeP());

        BottomNavigationView navigation = view.findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(this);
        /*BottomNavigationViewHelper.disableShiftMode(navigation);*/

        return view;
    }


    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;

        switch (item.getItemId()) {
            case R.id.navigation_home:
                fragment = new HomeP();
                break;

            case R.id.navigation_feed:
                startActivity(new Intent(getActivity(), QuickActionDetailsP.class).putExtra(Constants.TYPE, Constants.FEED));
                break;

            case R.id.navigation_activity:
                startActivity(new Intent(getActivity(), QuickActionDetailsP.class).putExtra(Constants.TYPE, Constants.ACTIVITY));
                break;

            case R.id.navigation_notification:
                startActivity(new Intent(getActivity(), QuickActionDetailsP.class).putExtra(Constants.TYPE, Constants.NOTIFICATION));
                break;
        }

        return loadFragment(fragment);
    }


}

