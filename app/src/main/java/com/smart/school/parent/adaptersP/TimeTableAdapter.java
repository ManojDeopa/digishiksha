package com.smart.school.parent.adaptersP;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.parent.responsesP.TimeTableTabResponse;

import java.util.List;


/**
 * Created by Manoj Singh Deopa.
 */

public class TimeTableAdapter extends RecyclerView.Adapter<TimeTableAdapter.ViewHolder> {

    private List<TimeTableTabResponse.TimeTable> list;
    private Context context;

    public TimeTableAdapter(List<TimeTableTabResponse.TimeTable> timeTable) {
        this.list = timeTable;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.time_table_adapter, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int i) {
        holder.time.setText(list.get(i).time);
        holder.subject.setText(list.get(i).subject);
        holder.teacher.setText(list.get(i).teacher);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView time, subject, teacher;

        public ViewHolder(View view) {
            super(view);
            time = view.findViewById(R.id.time);
            subject = view.findViewById(R.id.subject);
            teacher = view.findViewById(R.id.teacher);
        }
    }
}