package com.smart.school.parent.fragmentsP;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smart.school.R;
import com.smart.school.parent.adaptersP.FeedsAdapter;
import com.smart.school.parent.adaptersP.ViewComplaintsAdapter;
import com.smart.school.utils.CommonMethod;


/**
 * Created by Manoj Singh Deopa on 2/4/18.
 */

@SuppressLint("ValidFragment")

public class FeedsFragment extends Fragment {

    RecyclerView recycler_view;
    private Context context;
    private View view;


    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.common_recycler_view, container, false);
        context = getActivity();
        setRecyclerViewData();
        return view;

    }

    /*private void callApi() {
        final String appBaseUrl = LoginPreferences.getActiveInstance(context).getUserBaseUrl();
        Log.e("URL--", appBaseUrl);
        final ProgressD pd = ProgressD.show(context, getString(R.string.connecting), true);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, appBaseUrl + ConstantURL.APP_API,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.dismiss();
                        Log.e("Response--", response);
                        Gson gson = new Gson();
                        ViewComplaintResponse result = gson.fromJson(String.valueOf(response), ViewComplaintResponse.class);
                        if (result.result) {
                            if (result.data.size() != 0) {
                                setRecyclerViewData(result.data);
                            } else {
                                LinearLayout linearLayout = view.findViewById(R.id.empty_layout);
                                CommonMethod.showEmpty(linearLayout);
                            }
                        } else {
                            CommonMethod.showAlert(result.message, context);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                        Log.e("error--", error.toString());
                        Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                *//*Log.e("Page-", pageNo);*//*
                Map<String, String> params = new HashMap<>();
                params.put("api_page", ConstantURL.VIEW_COMPLAINTS);
                params.put("session_id", LoginPreferences.getActiveInstance(context).getSessionId());
                params.put("school_id", LoginPreferences.getActiveInstance(context).getSchoolId());
                params.put("branch_id", LoginPreferences.getActiveInstance(context).getBranchId());
                params.put("student_id", LoginPreferences.getActiveInstance(context).getId());
                params.put("class_id", LoginPreferences.getActiveInstance(context).getClassId());
                params.put("section_id", LoginPreferences.getActiveInstance(context).getSectionId());
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }*/


    private void setRecyclerViewData() {
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        CommonMethod.setRecyclerView(recyclerView, context);
        recyclerView.setAdapter(new FeedsAdapter());
    }


}
