package com.smart.school.parent.adaptersP;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.parent.activityP.QuickActionDetailsP;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.Constants;
import com.squareup.picasso.Picasso;


/**
 * Created by Manoj Singh Deopa .
 */

public class QuickActionAdapterP extends RecyclerView.Adapter<QuickActionAdapterP.ViewHolder> {

    private FragmentActivity context;
    private String[] list = {
            Constants.TYPE_ATTENDANCE,
            Constants.TYPE_TIME_TABLE,
            Constants.TYPE_NOTICE,
            Constants.TYPE_FEES,
            Constants.TYPE_EXAM_TIMETABLE,
            Constants.TYPE_HOMEWORK,
            Constants.TYPE_EXAM_RESULT,
            Constants.TYPE_LEAVE_REQUEST,
            Constants.TYPE_CLASSTEST_RESULT,
            Constants.TYPE_LIBRARY,
            Constants.TYPE_BUS_ROUTE,
            Constants.TYPE_MY_FEEDBACK,
            Constants.TYPE_ASSIGNMENT,
            Constants.TYPE_WEBSITE
    };

    private int[] list_image = {
            R.drawable.attendance_p,
            R.drawable.timetable_p,
            R.drawable.notice_p,
            R.drawable.fees_p,
            R.drawable.exam_time_table_p,
            R.drawable.homework_p,
            R.drawable.exam_result_p,
            R.drawable.leave_request_p,
            R.drawable.class_test_p,
            R.drawable.library_p,
            R.drawable.bus_route_p,
            R.drawable.my_feedback_p,
            R.drawable.exam_result_p,
            R.drawable.announcement_p,
    };

    public QuickActionAdapterP(FragmentActivity activity) {
        this.context = activity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dashboard_item_layout, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.tv_titles.setText(list[position]);
        Picasso.get().load(list_image[position]).into(holder.iv_image);

    }

    @Override
    public int getItemCount() {
        return list.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_titles;
        private ImageView iv_image;
        private LinearLayout parent;

        public ViewHolder(View view) {
            super(view);
            iv_image = view.findViewById(R.id.iv_image);
            tv_titles = view.findViewById(R.id.tv_title);
            parent = view.findViewById(R.id.parent);
            parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String type = list[getAdapterPosition()].trim();
                    if (type.equals(Constants.TYPE_WEBSITE)) {
                        CommonMethod.callBrowserIntent(context, Constants.DIGI_SHIKSHA_URL_WEB);
                        return;
                    }
                    context.startActivity(new Intent(context, QuickActionDetailsP.class).putExtra(Constants.TYPE, type));
                }
            });
        }
    }
}