package com.smart.school.parent.responsesP;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by manoj on 26/1/19.
 */
public class MyTeacherResponse {
    @SerializedName("result")
    @Expose
    public Boolean result;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;

    public class Datum {

        @SerializedName("teacher_id")
        @Expose
        public String teacherId;
        @SerializedName("teacher_name")
        @Expose
        public String teacherName;
        @SerializedName("teacher_photo")
        @Expose
        public String teacherPhoto;

        @SerializedName("designation_name")
        @Expose
        public String designation_name;

        @SerializedName("subject_name")
        @Expose
        public String subject_name;

    }

}
