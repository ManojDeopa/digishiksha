package com.smart.school.parent.activityP;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

/**
 * Created by Manoj.
 */


public class ChildListDatabase extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "ChildListDatabase";
    public static final String TABLE_NAME = "ChildList";
    private static final int DATABASE_VERSION = 1;
    private static final String COLUMN_ID = "id";


    private static final String STUDENT_NAME = "SESSION_NAME";
    private static final String STUDENT_ID = "SESSION_ID";
    private static final String ADMISSION_NO = "ADMISSION_NO";
    private static final String CLASS_ID = "CLASS_ID";
    private static final String SECTION_ID = "SECTION_ID";
    private static final String CLASS_NAME = "CLASS_NAME";
    private static final String SECTION_NAME = "SECTION_NAME";
    private static final String STUDENT_PHOTO = "STUDENT_PHOTO";
    private Context context;


    public ChildListDatabase(Context contexts) {
        super(contexts, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = contexts;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL(" CREATE TABLE " + TABLE_NAME + " (" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                STUDENT_NAME + " TEXT, " +
                STUDENT_ID + " TEXT, " +
                ADMISSION_NO + " TEXT, " +
                CLASS_ID + " TEXT, " +
                SECTION_ID + " TEXT, " +
                CLASS_NAME + " TEXT, " +
                SECTION_NAME + " TEXT, " +
                STUDENT_PHOTO + " TEXT);"
        );

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        String sql = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";
        sqLiteDatabase.execSQL(sql);
        onCreate(sqLiteDatabase);
    }

    public void storeData(int size,
                          String studentName,
                          String studentId, String admissionNo,
                          String classId, String sectionId,
                          String className, String sectionName,
                          String studentPhoto) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(STUDENT_NAME, studentName);
        contentValues.put(STUDENT_ID, studentId);
        contentValues.put(ADMISSION_NO, admissionNo);
        contentValues.put(CLASS_ID, classId);
        contentValues.put(SECTION_ID, sectionId);
        contentValues.put(CLASS_NAME, className);
        contentValues.put(SECTION_NAME, sectionName);
        contentValues.put(STUDENT_PHOTO, studentPhoto);

        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_NAME, null, contentValues);
        db.close();

    }

    public Cursor getAllData() {
        SQLiteDatabase db = getReadableDatabase();
        return db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
    }

    public boolean isChildExist(String student_id) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(DATABASE_NAME, new String[]{STUDENT_NAME},
                STUDENT_NAME + " LIKE ?", new String[]{"%" + student_id + "%"},
                null, null, null);

        if (cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {

                } while (cursor.moveToNext());
            }

        } else {
            Toast.makeText(context, "No data was found in the system!", Toast.LENGTH_LONG).show();
            return false;
        }
        cursor.close();

        return true;
    }

    public void delete(int id) {
        SQLiteDatabase mmDatabase = getWritableDatabase();
        String sql = "DELETE FROM ChildList WHERE id = ?";
        mmDatabase.execSQL(sql, new Integer[]{id});
    }

}
