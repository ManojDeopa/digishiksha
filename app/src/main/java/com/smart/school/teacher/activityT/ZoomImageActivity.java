package com.smart.school.teacher.activityT;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.smart.school.R;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.SwipeBackActivity;
import com.smart.school.utils.SwipeBackLayout;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class ZoomImageActivity extends SwipeBackActivity {

    private LinearLayout progress_detail;
    private ImageView imageView, ib_close, ib_download;
    private ProgressBar pb;
    private int downloadedSize = 0;
    private int totalSize = 0;
    private TextView cur_val;
    private String dwnload_file_path = "http://neerajbisht.com/ims/upload/demo.xlsx";
    private Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoom_image);

        overridePendingTransition(R.anim.slide_up_in, R.anim.slide_up_out);

        context = ZoomImageActivity.this;
        setDragEdge(SwipeBackLayout.DragEdge.BOTTOM);
        progress_detail = findViewById(R.id.progress_detail);
        ib_download = findViewById(R.id.ib_download);
        ib_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (CommonMethod.isOnline(context)) {
                    isStoragePermissionGranted();
                } else {
                    CommonMethod.showAlert(getString(R.string.noInternet), context);
                }
            }
        });
        ib_close = findViewById(R.id.ib_close);
        imageView = findViewById(R.id.image);

        String image = getIntent().getStringExtra("IMAGE");
        Picasso.get()
                .load(image)
                .placeholder(R.drawable.place_holder)
                .into(imageView);

        ib_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }


    public void isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                getDownload();
                Log.v("UploadExcelActivity : ", "Permission is granted");
            } else {
                Log.v("UploadExcelActivity : ", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }

        } else {
            getDownload();
            //permission is automatically granted on sdk<23 upon installation
            Log.v("UploadExcelActivity : ", "Permission is granted");
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.v("UploadExcelActivity : ", "Permission: " + permissions[0] + "was " + grantResults[0]);
            //resume tasks needing this permission
            getDownload();
        } else {

            Toast.makeText(context, "Permission is revoked by user ! try Again .", Toast.LENGTH_SHORT).show();

            // ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }
    }

    private void getDownload() {
        showProgress(dwnload_file_path);
        new Thread(new Runnable() {
            public void run() {
                downloadFile();
            }
        }).start();
    }


    void downloadFile() {

        try {

            URL url = new URL(dwnload_file_path);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.setRequestMethod("GET");
            urlConnection.setDoOutput(true);

            //connect
            urlConnection.connect();

            //set the path where we want to save the file
            File SDCardRoot = Environment.getExternalStorageDirectory();
            //create a new file, to save the downloaded file
            File file = new File(SDCardRoot, "demo.xlsx");

            FileOutputStream fileOutput = new FileOutputStream(file);

            //Stream used for reading the data from the internet
            InputStream inputStream = urlConnection.getInputStream();

            //this is the total size of the file which we are downloading
            totalSize = urlConnection.getContentLength();

            runOnUiThread(new Runnable() {
                public void run() {
                    pb.setMax(totalSize);
                }
            });

            //create a buffer...
            byte[] buffer = new byte[1024];
            int bufferLength = 0;

            while ((bufferLength = inputStream.read(buffer)) > 0) {
                fileOutput.write(buffer, 0, bufferLength);
                downloadedSize += bufferLength;
                // update the progressbar //
                runOnUiThread(new Runnable() {
                    public void run() {
                        pb.setProgress(downloadedSize);
                        float per = ((float) downloadedSize / totalSize) * 100;
                        Toast.makeText(context, "Downloaded", Toast.LENGTH_SHORT).show();
                        cur_val.setText("Downloaded " + downloadedSize + "KB / " + totalSize + "KB (" + (int) per + "%)");
                    }
                });
            }
            //close the output stream when complete //
            fileOutput.close();
            runOnUiThread(new Runnable() {
                public void run() {
                    // pb.dismiss(); // if you want close it..
                }
            });

        } catch (final MalformedURLException e) {
            showError("Error : MalformedURLException " + e);
            e.printStackTrace();
        } catch (final IOException e) {
            showError("Error : IOException " + e);
            e.printStackTrace();
        } catch (final Exception e) {
            showError("Error : Please check your internet connection " + e);
        }
    }

    void showError(final String err) {
        runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(context, err, Toast.LENGTH_LONG).show();
            }
        });
    }


    void showProgress(String file_path) {
        progress_detail.setVisibility(View.GONE);
        TextView text = findViewById(R.id.tv1);
        text.setText("Downloaded File Path : " + " " + "Device storage/demo.xlsx");
        cur_val = findViewById(R.id.cur_pg_tv);
        cur_val.setText("Starting download...");
        pb = findViewById(R.id.progress_bar);
        pb.setProgress(0);
        // pb.setProgressDrawable(getResources().getDrawable(R.drawable.green_progress));

        Toast.makeText(context, "Downloading..wait", Toast.LENGTH_SHORT).show();

    }


}
