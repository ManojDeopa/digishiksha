package com.smart.school.teacher.activityT;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;

import com.smart.school.R;
import com.smart.school.utils.CommonMethod;

/**
 * Created by manoj on 16/6/18.
 */
public class MyApplication extends Application implements Application.ActivityLifecycleCallbacks {

    private int activityReferences = 0;
    private boolean isActivityChangingConfigurations = false;

    @Override
    public void onCreate() {
        super.onCreate();
        registerActivityLifecycleCallbacks(this);
        FontsOverride.setDefaultFont(this, "DEFAULT", "fonts/OpenSans-Regular.ttf");
        FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/OpenSans-Regular.ttf");
        FontsOverride.setDefaultFont(this, "SERIF", "fonts/OpenSans-Regular.ttf");
        FontsOverride.setDefaultFont(this, "SANS_SERIF", "fonts/OpenSans-Regular.ttf");
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {
        Log.e("App Status -", "onActivityCreated");

        if (++activityReferences == 1 && !isActivityChangingConfigurations) {
            /*if (!CommonMethod.isOnline(activity)) {
                CommonMethod.showAlert(getString(R.string.noInternet), activity);
            }*/
        }


    }

    @Override
    public void onActivityStarted(Activity activity) {
        Log.e("App Status -", "onActivityStarted");
    }

    @Override
    public void onActivityResumed(Activity activity) {
        Log.e("App Status -", "onActivityResumed");
        if (!CommonMethod.isOnline(activity)) {
            CommonMethod.showNetworkAlert(activity);
        }
    }

    @Override
    public void onActivityPaused(Activity activity) {
        Log.e("App Status -", "onActivityPaused");
    }

    @Override
    public void onActivityStopped(Activity activity) {
        Log.e("App Status -", "onActivityStopped");
        isActivityChangingConfigurations = activity.isChangingConfigurations();
        if (--activityReferences == 0 && !isActivityChangingConfigurations) {
            Log.e("MyApplication--", "App Enters Background");
        }

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        Log.e("App Status -", "onActivitySaveInstanceState");
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        Log.e("App Status -", "onActivityDestroyed");

    }
}
