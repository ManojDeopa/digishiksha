package com.smart.school.teacher.activityT;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.smart.school.R;
import com.smart.school.VerifySchoolActivity;
import com.smart.school.geteKeeper.DashboardGk;
import com.smart.school.masterAdmin.activitiesMA.DashboardMasterAdmin;
import com.smart.school.teacher.responsesT.LoginResponse;
import com.smart.school.teacher.responsesT.VarifySchoolResponse;
import com.smart.school.utils.ClassListDatabase;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.ConstantURL;
import com.smart.school.utils.Constants;
import com.smart.school.utils.LoginPreferences;
import com.smart.school.utils.ProgressD;
import com.smart.school.utils.SessionListDatabase;
import com.smart.school.utils.VolleySingleton;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = LoginActivity.class.getSimpleName();
    private EditText et_email, et_password, et_session;
    private Button btn_login;
    private LoginActivity context;
    private ImageView iv_school_logo;
    private TextView et_school_code;
    private String role;
    private Spinner spinner, spinner_role;
    private String session;
    private List<VarifySchoolResponse.SchoolSession> sessionList = new ArrayList<>();
    private String activeSession;
    private String sessionId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = LoginActivity.this;

        setContentView(R.layout.activity_login);

        getSupportActionBar().hide();


        _initializeViews();

        _onButtonClick();

    }

    private void _initializeViews() {
        TextView app_version = findViewById(R.id.app_version);
        CommonMethod.setVersionName(app_version);
        btn_login = findViewById(R.id.btn_login);
        et_email = findViewById(R.id.et_user_name);
        et_password = findViewById(R.id.et_password);
        iv_school_logo = findViewById(R.id.iv_school_logo);
        et_school_code = findViewById(R.id.et_school_code);
        et_session = findViewById(R.id.et_session);

        String image = LoginPreferences.getActiveInstance(context).getSchoolLogo();
        if (!TextUtils.isEmpty(image)) {
            Picasso.get().load(image).placeholder(R.drawable.place_holder).into(iv_school_logo);
        }
        if (!TextUtils.isEmpty(LoginPreferences.getActiveInstance(context).getSchoolName())) {
            et_school_code.setText(LoginPreferences.getActiveInstance(context).getSchoolName());
        }

        fetchLocalData();

        spinner_role = findViewById(R.id.spinner_role);
        List<String> list_role = new ArrayList<String>();
        list_role.add("Master Admin");
        list_role.add("Principal");
        list_role.add("Teacher");
        list_role.add("Chairman");
        list_role.add("Inventory Manager");
        list_role.add("Receptionist");
        list_role.add("Gate Keeper");
        ArrayAdapter<String> roleAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, list_role);
        roleAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_role.setAdapter(roleAdapter);

        spinner_role.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String str = parent.getItemAtPosition(position).toString();

                switch (str) {
                    case "Master Admin":
                        str = "masteradmin";
                        break;
                    case "Inventory Manager":
                        str = "inventorymanager";
                        break;
                    case "Gate Keeper":
                        str = "gatekeeper";
                        break;
                }
                str = str.replaceFirst(str.substring(0, 1), str.substring(0, 1).toLowerCase());
                role = "/" + str;
                Log.e("Role--", role);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayList<String> sessionName = new ArrayList<>();

        if (sessionList != null) {

            activeSession = sessionList.get(0).sessionName;

            LoginPreferences.getActiveInstance(context).setSessionId(sessionList.get(0).t1Id);

            for (int i = 0; i < sessionList.size(); i++) {

                sessionName.add(sessionList.get(i).sessionName);

            }
        }

        et_session.setText(activeSession);
        spinner = findViewById(R.id.spinner);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, sessionName);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                session = parent.getItemAtPosition(position).toString();
                et_session.setText(session);
                sessionId = sessionList.get(position).t1Id;
                Log.e("Session ID", sessionId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void _onButtonClick() {
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _getLogin();
            }
        });
    }


    private void _getLogin() {
        if (TextUtils.isEmpty(et_email.getText().toString().trim())) {
            et_email.setError(getString(R.string.field_required));
            et_email.requestFocus();
        } else if (TextUtils.isEmpty(et_password.getText().toString().trim())) {
            et_password.setError(getString(R.string.field_required));
            et_password.requestFocus();
        } else {
            if (CommonMethod.isOnline(context)) {
                callApi();
            } else {
                CommonMethod.showNetworkAlert(context);
            }
        }
    }


    private void fetchLocalData() {
        SessionListDatabase mDatabase = new SessionListDatabase(context);
        sessionList = new ArrayList<>();
        Cursor cursor = mDatabase.getAllData();
        if (cursor.moveToFirst()) {
            do {
                sessionList.add(new VarifySchoolResponse.SchoolSession(
                        cursor.getString(1),
                        cursor.getString(2)));
            } while (cursor.moveToNext());
        }

    }


    private void callApi() {
        final String appBaseUrl = LoginPreferences.getActiveInstance(context).getUserBaseUrl() + role;
        final ProgressD pd = ProgressD.show(context, context.getResources().getString(R.string.connecting), true);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, appBaseUrl + ConstantURL.APP_API,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.dismiss();

                        Log.e("Response--", response);

                        Gson gson = new Gson();
                        LoginResponse result = gson.fromJson(String.valueOf(response), LoginResponse.class);

                        if (result.result) {

                            if (result.data.size() != 0) {

                                saveSession(appBaseUrl,result);

                            }
                        } else {
                            CommonMethod.showAlert(result.message, context);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                        Log.e("error--", error.toString());
                        CommonMethod.showAlert(Constants.SERVER_ERROR, context);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("api_page", ConstantURL.SCHOOL_LOGIN);
                params.put("session_id", sessionId);
                params.put("school_id", LoginPreferences.getActiveInstance(context).getSchoolId());
                params.put("branch_id", LoginPreferences.getActiveInstance(context).getBranchId());
                params.put("emp_username", et_email.getText().toString().trim());
                params.put("emp_password", et_password.getText().toString().trim());
                return params;
            }
        };

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);

    }

    private void saveSession(String appBaseUrl, LoginResponse result) {
        LoginPreferences.getActiveInstance(context).setBASE_URL(appBaseUrl + ConstantURL.APP_API);
        LoginPreferences.getActiveInstance(context).setIsLoggedIn(true);
        LoginPreferences.getActiveInstance(context).setId(result.data.get(0).empId);
        LoginPreferences.getActiveInstance(context).setFullname(result.data.get(0).fullName);
        LoginPreferences.getActiveInstance(context).setMobile(result.data.get(0).mobileNo);
        LoginPreferences.getActiveInstance(context).setUser_email(result.data.get(0).emailId);
        LoginPreferences.getActiveInstance(context).setUserImage(result.data.get(0).empPhoto);

        switch (role) {
            case "/teacher":
                if (result.classVal.size() != 0) {
                    storeData(result.classVal);
                }
                LoginPreferences.getActiveInstance(context).setType(Constants.TYPE_TEACHER);
                finishAffinity();
                CommonMethod.startIntent(this, DashboardActivity.class);

                break;
            case "/gatekeeper":
                LoginPreferences.getActiveInstance(context).setType(Constants.TYPE_GATEKEEPER);
                finishAffinity();
                CommonMethod.startIntent(this, DashboardGk.class);

                break;


            case "/masteradmin":
                if (result.classVal.size() != 0) {
                    storeData(result.classVal);
                }
                LoginPreferences.getActiveInstance(context).setType(Constants.TYPE_MASTER_ADMIN);
                finishAffinity();
                CommonMethod.startIntent(this, DashboardMasterAdmin.class);
                break;

        }
    }

    private void storeData(List<LoginResponse.ClassVal> classVal) {
        ClassListDatabase mDatabase = new ClassListDatabase(context);
        mDatabase.getWritableDatabase().execSQL("delete from " + ClassListDatabase.TABLE_NAME);
        for (int i = 0; i < classVal.size(); i++) {
            mDatabase.storeData(
                    classVal.size(),
                    classVal.get(i).className,
                    classVal.get(i).id);
        }

    }


    @Override
    public void onBackPressed() {
        finish();
    }

    public void anotherSchool(View view) {
        LoginPreferences.getActiveInstance(context).setType("");
        finish();
        CommonMethod.startIntent(this, VerifySchoolActivity.class);
    }
}
