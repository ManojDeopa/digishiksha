package com.smart.school.teacher.responsesT;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by manoj on 23/3/19.
 */
public class CalenderResponse {

    @SerializedName("result")
    @Expose
    public Boolean result;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;

    public class Datum {

        @SerializedName("calender_id")
        @Expose
        public Integer calenderId;
        @SerializedName("from_date")
        @Expose
        public String fromDate;
        @SerializedName("to_date")
        @Expose
        public String toDate;
        @SerializedName("hl_title")
        @Expose
        public String hlTitle;
        @SerializedName("hl_description")
        @Expose
        public String hlDescription;
        @SerializedName("hl_status")
        @Expose
        public String hlStatus;

    }


}
