package com.smart.school.teacher.fragmentsT;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.smart.school.R;
import com.smart.school.teacher.activityT.QuickActionDetails;
import com.smart.school.teacher.adaptersT.ClassListAdapter;
import com.smart.school.teacher.adaptersT.SectionListAdapter;
import com.smart.school.teacher.responsesT.CommonResponse;
import com.smart.school.teacher.responsesT.LoginResponse;
import com.smart.school.teacher.responsesT.SectionResponse;
import com.smart.school.utils.AppData;
import com.smart.school.utils.ClassListDatabase;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.Constants;
import com.smart.school.utils.LoginPreferences;
import com.smart.school.utils.ProgressD;
import com.smart.school.utils.VolleySingleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Manoj Singh Deopa on 2/4/18.
 */

@SuppressLint("ValidFragment")

public class StudentFeedbackT extends Fragment {

    public static TextView section_name;
    TextView feedback, class_name;
    private Context context;
    private View view;
    private List<LoginResponse.ClassVal> classlist;
    private List<SectionResponse.Datum> section_list;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.student_feedback, container, false);

        initViews();

        fetchClassData();

        onclicks();


        return view;

    }

    private void validate() {
        if (TextUtils.isEmpty(class_name.getText().toString().trim())) {
            Toast.makeText(context, "Please Select Class", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(section_name.getText().toString().trim())) {
            Toast.makeText(context, "Please Select Section", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(feedback.getText().toString().trim())) {
            Toast.makeText(context, "Please Select Feedback", Toast.LENGTH_SHORT).show();
        } else {
            callApi();
        }
    }

    private void initViews() {
        context = getActivity();
        feedback = view.findViewById(R.id.feedback);
        class_name = view.findViewById(R.id.class_name);
        section_name = view.findViewById(R.id.section_name);
    }

    private void onclicks() {

        feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        class_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showClassDialog("Select Class");
            }
        });


        section_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setData();
            }
        });

        view.findViewById(R.id.btn_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate();
            }
        });


    }

    private void setData() {
        section_list = AppData.getInstance().getSectionList();
        if (section_list != null) {
            showClassDialog("Select Section");
        } else {
            Toast.makeText(context, "Please Select Class First", Toast.LENGTH_SHORT).show();
        }
    }


    private void fetchClassData() {

        ClassListDatabase mDatabase = new ClassListDatabase(context);
        classlist = new ArrayList<>();
        Cursor cursor = mDatabase.getAllData();
        if (cursor.moveToFirst()) {
            do {

                classlist.add(new LoginResponse.ClassVal(
                        cursor.getString(1),
                        cursor.getString(2)));
            } while (cursor.moveToNext());
        }

    }


    public void showClassDialog(String title) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.list_dialog_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;


        RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view);
        CommonMethod.setRecyclerView(recyclerView, context);


        TextView btn_submit = dialog.findViewById(R.id.btn_submit);
        TextView btn_cancel = dialog.findViewById(R.id.btn_cancel);

        TextView tvtitle = dialog.findViewById(R.id.title);
        tvtitle.setText(title);

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        switch (title) {

            case "Select Class":
                AppData.getInstance().setfrom("Student Feedback");
                recyclerView.setAdapter(new ClassListAdapter(dialog, classlist, class_name));

                break;

            case "Select Section":
                recyclerView.setAdapter(new SectionListAdapter(dialog, section_list, section_name));
                break;

            default:

                break;
        }


        dialog.show();
        /* FOR SETTING CUSTOM HIGHT AND WIDTH */
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(dialog.getWindow().getAttributes());
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes(layoutParams);
    }


    private void callApi() {
        String appBaseUrl = LoginPreferences.getActiveInstance(context).getBASE_URL();
        Log.e("URL---", appBaseUrl);
        final ProgressD pd = ProgressD.show(context, context.getResources().getString(R.string.connecting), true);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, appBaseUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.dismiss();
                        Log.e("Response--", response);
                        Gson gson = new Gson();
                        CommonResponse result = gson.fromJson(String.valueOf(response), CommonResponse.class);

                        if (result.result) {

                            startActivity(new Intent(context, QuickActionDetails.class).putExtra(Constants.TYPE, Constants.VIEW_HW_DETAIL));
                            Toast.makeText(context, result.message, Toast.LENGTH_SHORT).show();

                           /* if (result.data.size() != 0) {
                                 setList();
                            }*/

                        } else {
                            CommonMethod.showAlert(result.message, context);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        startActivity(new Intent(context, QuickActionDetails.class).putExtra(Constants.TYPE, Constants.VIEW_HW_DETAIL));
                        pd.dismiss();
                        Log.e("error--", error.toString());
                        Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Log.e("Class ID--", AppData.getInstance().getClassId());

                Map<String, String> params = new HashMap<>();
                params.put("api_page", "homework_view");
                params.put("class_id", AppData.getInstance().getClassId());
                params.put("feedback", "");
                params.put("section_id", AppData.getInstance().getSectionId());
                params.put("school_id", LoginPreferences.getActiveInstance(context).getSchoolId());
                params.put("branch_id", LoginPreferences.getActiveInstance(context).getBranchId());
                params.put("session_id", LoginPreferences.getActiveInstance(context).getSessionId());
                params.put("emp_id", LoginPreferences.getActiveInstance(context).getId());
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }


}
