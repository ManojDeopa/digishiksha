package com.smart.school.teacher.responsesT;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by manoj on 1/12/18.
 */
public class VarifySchoolResponse {

    @SerializedName("result")
    @Expose
    public Boolean result;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;
    @SerializedName("school_session")
    @Expose
    public List<SchoolSession> schoolSession = null;

    public static class SchoolSession {
        @SerializedName("by_defult")
        @Expose
        public String byDefult;
        @SerializedName("t1_id")
        @Expose
        public String t1Id;
        @SerializedName("session_name")
        @Expose
        public String sessionName;

        public SchoolSession(String string, String string1) {

            this.sessionName = string;
            this.t1Id = string1;

        }
    }

    public class Datum {

        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("encrypt_id")
        @Expose
        public String encryptId;
        @SerializedName("school_id")
        @Expose
        public String schoolId;
        @SerializedName("branch_id")
        @Expose
        public String branchId;
        @SerializedName("school_name")
        @Expose
        public String schoolName;
        @SerializedName("school_logo")
        @Expose
        public String schoolLogo;
        @SerializedName("school_code")
        @Expose
        public String schoolCode;
        @SerializedName("default_session")
        @Expose
        public String defaultSession;
        @SerializedName("app_api_url")
        @Expose
        public String appApiUrl;
        @SerializedName("app_status")
        @Expose
        public String appStatus;

    }

}
