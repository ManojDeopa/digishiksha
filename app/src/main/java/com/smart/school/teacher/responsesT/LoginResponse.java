package com.smart.school.teacher.responsesT;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by manoj on 8/12/18.
 */
public class LoginResponse {

    @SerializedName("result")
    @Expose
    public Boolean result;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;

    @SerializedName("class_val")
    @Expose
    public List<ClassVal> classVal = null;

    public static class ClassVal {

        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("class_name")
        @Expose
        public String className;

        public boolean isChecked;

        public ClassVal(String string, String string1) {
            this.className = string;
            this.id = string1;
        }
    }

    public class Datum {

        @SerializedName("emp_id")
        @Expose
        public String empId;
        @SerializedName("employee_no")
        @Expose
        public String employeeNo;
        @SerializedName("full_name")
        @Expose
        public String fullName;
        @SerializedName("mobile_no")
        @Expose
        public String mobileNo;
        @SerializedName("email_id")
        @Expose
        public String emailId;
        @SerializedName("emp_photo")
        @Expose
        public String empPhoto;

    }


}
