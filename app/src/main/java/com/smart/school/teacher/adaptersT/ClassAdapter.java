package com.smart.school.teacher.adaptersT;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.smart.school.R;
import com.smart.school.teacher.activityT.UploadImage_MarkAttendance;
import com.smart.school.teacher.responsesT.LoginResponse;
import com.smart.school.teacher.responsesT.StudentListResponse;
import com.smart.school.utils.AppData;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.Constants;
import com.smart.school.utils.LoginPreferences;
import com.smart.school.utils.ProgressD;
import com.smart.school.utils.VolleySingleton;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Manoj Singh Deopa.
 */

public class ClassAdapter extends RecyclerView.Adapter<ClassAdapter.ViewHolder> {

    private List<LoginResponse.ClassVal> list;
    private Context context;

    public ClassAdapter(List<LoginResponse.ClassVal> classlist) {
        this.list = classlist;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.class_list_adapter, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        holder.text.setText(list.get(position).className);

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    private void callApi() {

        final ProgressD pd = ProgressD.show(context, context.getString(R.string.connecting), true);

        String appBaseUrl = LoginPreferences.getActiveInstance(context).getBASE_URL();
        Log.e("URL---", appBaseUrl);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, appBaseUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.dismiss();

                        Log.e("Response--", response);

                        Gson gson = new Gson();
                        StudentListResponse result = gson.fromJson(String.valueOf(response), StudentListResponse.class);

                        if (result.result) {
                            if (result.data.size() != 0) {

                                AppData.getInstance().setStudentList(result.data);
                                context.startActivity(new Intent(context, UploadImage_MarkAttendance.class).putExtra("TYPE", Constants.TYPE_STUDENT_LIST));
                            }

                        } else {
                            CommonMethod.showAlert(result.message, context);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                        Log.e("error--", error.toString());
                        Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Log.e("Class ID--", AppData.getInstance().getClassId());
                Log.e("school ID--", LoginPreferences.getActiveInstance(context).getSchoolId());
                Log.e("branch ID--", LoginPreferences.getActiveInstance(context).getBranchId());


                Map<String, String> params = new HashMap<>();
                params.put("api_page", "student_list");
                params.put("class_id", AppData.getInstance().getClassId());
                params.put("school_id", LoginPreferences.getActiveInstance(context).getSchoolId());
                params.put("branch_id", LoginPreferences.getActiveInstance(context).getBranchId());
                params.put("session_id", LoginPreferences.getActiveInstance(context).getSessionId());


                return params;
            }
        };

        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);

    }

    private void move(int i) {
        AppData.getInstance().setClassId(list.get(i).id);
        callApi();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView text;
        CheckBox checkbox;

        public ViewHolder(View view) {
            super(view);

            text = view.findViewById(R.id.text);
            checkbox = view.findViewById(R.id.checkbox);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    move(getAdapterPosition());
                }
            });

            checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    move(getAdapterPosition());

                }
            });
        }
    }


}