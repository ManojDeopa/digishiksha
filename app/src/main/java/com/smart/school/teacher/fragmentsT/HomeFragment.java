package com.smart.school.teacher.fragmentsT;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.smart.school.R;
import com.smart.school.teacher.adaptersT.AboutSchoolAdapterT;
import com.smart.school.teacher.adaptersT.HomeTopAdapter;
import com.smart.school.teacher.adaptersT.QuickActionAdapterT;
import com.smart.school.teacher.responsesT.CommonResponse;
import com.smart.school.utils.AppData;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.LinePagerIndicatorDecoration;
import com.smart.school.utils.LoginPreferences;
import com.smart.school.utils.ProgressD;
import com.smart.school.utils.VolleySingleton;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by Manoj Singh Deopa on 2/4/18.
 */

@SuppressLint("ValidFragment")

public class HomeFragment extends Fragment {

    private RecyclerView home_top_rv, quick_action_rv, rv_about_school;
    private Context context;


    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.home_fragment, container, false);
        context = getActivity();
        home_top_rv = view.findViewById(R.id.rv_home_top);
        CommonMethod.setHorizontalRecyclerView(home_top_rv, context);
        //home_top_rv.addItemDecoration(new LinePagerIndicatorDecoration());


        home_top_rv.setAdapter(new HomeTopAdapter());


        quick_action_rv = view.findViewById(R.id.rv_quick_action);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        quick_action_rv.setLayoutManager(layoutManager);
        quick_action_rv.setItemAnimator(new DefaultItemAnimator());
        quick_action_rv.setAdapter(new QuickActionAdapterT());


        rv_about_school = view.findViewById(R.id.rv_about_s);
        RecyclerView.LayoutManager layoutManager1 = new GridLayoutManager(context, 3);
        rv_about_school.setLayoutManager(layoutManager1);
        rv_about_school.setItemAnimator(new DefaultItemAnimator());
        rv_about_school.setAdapter(new AboutSchoolAdapterT());

        return view;
    }


    private void callApi() {
        String appBaseUrl = LoginPreferences.getActiveInstance(context).getBASE_URL();
        Log.e("URL---", appBaseUrl);
        final ProgressD pd = ProgressD.show(context, context.getResources().getString(R.string.connecting), true);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, appBaseUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.dismiss();
                        Log.e("Response--", response);
                        Gson gson = new Gson();
                        CommonResponse result = gson.fromJson(String.valueOf(response), CommonResponse.class);

                        if (result.result) {

                            Toast.makeText(context, result.message, Toast.LENGTH_SHORT).show();

                           /* if (result.data.size() != 0) {


                            }*/

                        } else {
                            CommonMethod.showAlert(result.message, context);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                        Log.e("error--", error.toString());
                        Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Log.e("Class ID--", AppData.getInstance().getClassId());

                Map<String, String> params = new HashMap<>();
                params.put("api_page", "class_subject_list");
                params.put("class_id", AppData.getInstance().getClassId());
                params.put("section_id", AppData.getInstance().getSectionId());
                params.put("school_id", LoginPreferences.getActiveInstance(context).getSchoolId());
                params.put("branch_id", LoginPreferences.getActiveInstance(context).getBranchId());
                params.put("session_id", LoginPreferences.getActiveInstance(context).getSessionId());
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }


}
