package com.smart.school.teacher.fragmentsT;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.smart.school.R;
import com.smart.school.teacher.adaptersT.ClassAdapter;
import com.smart.school.teacher.responsesT.LoginResponse;
import com.smart.school.utils.ClassListDatabase;
import com.smart.school.utils.CommonMethod;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Manoj Singh Deopa on 2/4/18.
 */

@SuppressLint("ValidFragment")

public class ClassList extends Fragment {

    RecyclerView recycler_view;

    private Context context;
    private View view;
    private List<LoginResponse.ClassVal> classlist;


    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.common_recycler_view, container, false);

        context = getActivity();

        classlist = CommonMethod.getClassListForSchool(context);

        setList();

        return view;
    }

    private void setList() {

        if (classlist.size() == 0) {
            LinearLayout linearLayout = view.findViewById(R.id.empty_layout);
            CommonMethod.showEmpty(linearLayout);
            return;
        }
        recycler_view = view.findViewById(R.id.recycler_view);
        CommonMethod.setRecyclerView(recycler_view, context);
        recycler_view.setAdapter(new ClassAdapter(classlist));

    }

}
