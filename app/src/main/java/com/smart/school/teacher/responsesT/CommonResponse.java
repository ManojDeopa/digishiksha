package com.smart.school.teacher.responsesT;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by manoj on 10/2/19.
 */
public class CommonResponse {

    @SerializedName("result")
    @Expose
    public Boolean result;
    @SerializedName("message")
    @Expose
    public String message;
}
