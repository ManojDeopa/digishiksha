package com.smart.school.teacher.responsesT;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by manoj on 9/12/18.
 */
public class SectionResponse {
    @SerializedName("result")
    @Expose
    public Boolean result;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;


    public class Datum {

        @SerializedName("t2_id")
        @Expose
        public String t2Id;
        @SerializedName("t2_section_name")
        @Expose
        public String t2SectionName;

    }

}
