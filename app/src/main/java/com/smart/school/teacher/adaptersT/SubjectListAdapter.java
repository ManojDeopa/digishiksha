package com.smart.school.teacher.adaptersT;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.teacher.responsesT.SubjectListResponse;
import com.smart.school.utils.AppData;

import java.util.List;


/**
 * Created by Manoj Singh Deopa.
 */

public class SubjectListAdapter extends RecyclerView.Adapter<SubjectListAdapter.ViewHolder> {

    private List<SubjectListResponse.Datum> list;
    private TextView class_name;
    private Dialog dialog;
    private Context context;


    public SubjectListAdapter(Dialog dialog, List<SubjectListResponse.Datum> data, TextView subject_name) {
        this.dialog = dialog;
        this.list = data;
        this.class_name = subject_name;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dialog_list_item, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.text.setText(list.get(position).subjectName);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {


        LinearLayout parent_layout;
        TextView text;
        RadioButton radio_button;


        public ViewHolder(View view) {
            super(view);

            text = view.findViewById(R.id.text);
            radio_button = view.findViewById(R.id.radio_button);

            parent_layout = view.findViewById(R.id.parent_layout);

            parent_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    class_name.setText(list.get(getAdapterPosition()).subjectName);
                    AppData.getInstance().setSubjectId(list.get(getAdapterPosition()).subjectId);
                    dialog.dismiss();

                }
            });

            radio_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    class_name.setText(list.get(getAdapterPosition()).subjectName);
                    AppData.getInstance().setSubjectId(list.get(getAdapterPosition()).subjectId);
                    dialog.dismiss();


                }
            });

        }
    }


}