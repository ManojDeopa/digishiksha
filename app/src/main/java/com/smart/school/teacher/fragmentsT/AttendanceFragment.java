package com.smart.school.teacher.fragmentsT;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;

import com.smart.school.R;
import com.smart.school.teacher.activityT.AddProjectActivity;
import com.smart.school.teacher.adaptersT.AnnouncementAdapter;
import com.smart.school.utils.CommonMethod;


/**
 * Created by Manoj Singh Deopa on 2/4/18.
 */

@SuppressLint("ValidFragment")

public class AttendanceFragment extends Fragment implements View.OnClickListener {

    private RecyclerView recyclerView;
    private Context context;
    private AnnouncementAdapter adapter;
    private Boolean isFabOpen = false;
    private FloatingActionButton fab;
    private Animation fab_open, fab_close, rotate_forward, rotate_backward;
    private LinearLayout add_btn_layout;
    private Button fab1, fab2;


    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.announcement_fragment_layout, container, false);

        initilizeView(view);

        setUpFab(view);

        return view;

    }

    private void initilizeView(View view) {
        context = getActivity();
        recyclerView = view.findViewById(R.id.recycler_view);
        CommonMethod.setRecyclerView(recyclerView, context);
        adapter = new AnnouncementAdapter();
        recyclerView.setAdapter(adapter);
    }

    private void setUpFab(View view) {
        add_btn_layout = view.findViewById(R.id.add_btn_layout);
        fab = view.findViewById(R.id.fab);
        fab1 = view.findViewById(R.id.fab1);
        fab2 = view.findViewById(R.id.fab2);
        fab_open = AnimationUtils.loadAnimation(context, R.anim.fab_open);
        fab_close = AnimationUtils.loadAnimation(context, R.anim.fab_close);
        rotate_forward = AnimationUtils.loadAnimation(context, R.anim.rotate_forword);
        rotate_backward = AnimationUtils.loadAnimation(context, R.anim.rotate_backward);
        fab.setOnClickListener(this);
        fab1.setOnClickListener(this);
        fab2.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.fab:
                animateFAB();
                break;
            case R.id.fab1:

                startActivity(new Intent(context, AddProjectActivity.class).putExtra("From",
                        getString(R.string.new_announcement)));

                break;
            case R.id.fab2:
                startActivity(new Intent(context, AddProjectActivity.class).putExtra("From",
                        getString(R.string.new_project)));

                break;
        }
    }

    public void animateFAB() {

        if (isFabOpen) {
            fab.startAnimation(rotate_backward);
            add_btn_layout.startAnimation(fab_close);
            fab1.setClickable(false);
            fab2.setClickable(false);
            isFabOpen = false;

        } else {
            fab.startAnimation(rotate_forward);
            add_btn_layout.startAnimation(fab_open);
            fab1.setClickable(true);
            fab2.setClickable(true);
            isFabOpen = true;
        }
    }


}
