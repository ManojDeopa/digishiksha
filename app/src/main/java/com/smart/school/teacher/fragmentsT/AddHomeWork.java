package com.smart.school.teacher.fragmentsT;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.smart.school.BuildConfig;
import com.smart.school.R;
import com.smart.school.teacher.adaptersT.UploadImageAdapterT;
import com.smart.school.teacher.responsesT.CommonResponse;
import com.smart.school.teacher.responsesT.SubjectListResponse;
import com.smart.school.utils.AppData;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.ConstantURL;
import com.smart.school.utils.GetSubjects;
import com.smart.school.utils.LoginPreferences;
import com.smart.school.utils.ProgressD;
import com.smart.school.utils.VolleySingleton;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.app.Activity.RESULT_OK;


/**
 * Created by Manoj Singh Deopa on 2/4/18.
 */

@SuppressLint("ValidFragment")

public class AddHomeWork extends Fragment implements View.OnClickListener {


    public static ArrayList<String> uploadImageList = new ArrayList<>();
    TextView subject_name;
    TextView class_name;
    Button btn_submit;
    LinearLayout upload;
    EditText description;
    CheckBox cb_parentApp, cb_phoneSms;
    String notify_type_app = "1";
    String notify_type_phone = "0";

    private Context context;
    private RecyclerView rv_upload_image;
    private File cameraFile;

    public AddHomeWork() {
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_home_work, container, false);
        context = getActivity();
        class_name = view.findViewById(R.id.class_name);
        subject_name = view.findViewById(R.id.subject_name);
        btn_submit = view.findViewById(R.id.btn_submit);


        upload = view.findViewById(R.id.upload);
        rv_upload_image = view.findViewById(R.id.recycler_view);
        description = view.findViewById(R.id.description);
        cb_parentApp = view.findViewById(R.id.cb_parentApp);
        cb_phoneSms = view.findViewById(R.id.cb_phoneSms);

        btn_submit.setOnClickListener(this);
        class_name.setOnClickListener(this);
        subject_name.setOnClickListener(this);
        upload.setOnClickListener(this);

        cb_parentApp.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    notify_type_app = "1";
                } else {
                    notify_type_app = "0";
                }

            }
        });

        cb_phoneSms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    notify_type_phone = "1";
                } else {
                    notify_type_phone = "0";
                }

            }
        });


        return view;
    }


    @Override
    public void onClick(View v) {
        if (v == class_name) {
            CommonMethod.showClassDialog(context, class_name);
        }

        if (v == subject_name) {

            if (!TextUtils.isEmpty(AppData.getInstance().getClassId())) {
                new GetSubjects(context, subject_name);
            } else {
                CommonMethod.showAlert("Please Select Class First", context);
            }
        }

        if (v == upload) {
            selectImage();
        }

        if (v == btn_submit) {
            if (TextUtils.isEmpty(class_name.getText().toString().trim())) {
                Toast.makeText(context, "Please Select Class", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(subject_name.getText().toString().trim())) {
                Toast.makeText(context, "Please Select Subject", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(description.getText().toString().trim())) {
                Toast.makeText(context, "Please Enter Homework Description", Toast.LENGTH_SHORT).show();
            } else if (uploadImageList.size() == 0) {
                Toast.makeText(context, "Please Upload Homework Photo", Toast.LENGTH_SHORT).show();
            } else {
                uploadImageList = UploadImageAdapterT.list;
                if (uploadImageList.size() == 0) {
                    Toast.makeText(context, "Please Upload Homework Photo", Toast.LENGTH_SHORT).show();
                } else {
                    callApi();
                }
            }
        }
    }


    public void selectImage() {
        final CharSequence[] items = {"Take Photo", "Select From Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    onLaunchCamera();

                } else if (items[item].equals("Select From Gallery")) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), 4);
                }
            }
        });
        builder.show();
    }


    public void onLaunchCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraFile = getPhotoFileUri("photo.jpg");

        // wrap File object into a content provider
        // required for API >= 24
        // See https://guides.codepath.com/android/Sharing-Content-with-Intents#sharing-files-with-api-24-or-higher

        Uri fileProvider = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", cameraFile);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileProvider);

        // If you call startActivityForResult() using an intent that no app can handle, your app will crash.
        // So as long as the result is not null, it's safe to use the intent.
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            // Start the image capture intent to take photo
            startActivityForResult(intent, 3);
        }
    }


    public File getPhotoFileUri(String fileName) {
        // Get safe storage directory for photos
        // Use `getExternalFilesDir` on Context to access package-specific directories.
        // This way, we don't need to request external read/write runtime permissions.
        File mediaStorageDir = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "DigiShiksha");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) {
            Log.e("DigiShiksha--", "failed to create directory");
        }

        return new File(mediaStorageDir.getPath() + File.separator + fileName);
    }

    @Override
    @SuppressLint("NewApi")
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.e("requestCode-", String.valueOf(requestCode));
        Log.e("resultCode-", String.valueOf(resultCode));
        Log.e("data-", String.valueOf(data));

        if (resultCode != RESULT_OK) {
            return;
        }
        if (requestCode == 3) {

            Uri uri = Uri.fromFile(cameraFile);

            Log.e("cameraFile URI-", String.valueOf(cameraFile));
            Log.e("uri URI-", String.valueOf(uri));

            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (bitmap != null) {
                setImageData(bitmap);
            }

        }
        if (requestCode == 4 && data != null && data.getData() != null) {
            Uri uri = data.getData();
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (bitmap != null) {
                setImageData(bitmap);
            }
        }

    }


    private void setImageData(Bitmap bitmap) {
        /*Bitmap resized = Bitmap.createScaledBitmap(bitmap, 900, 900, true);*/
        AppData.getInstance().setfrom("AddHomework");
        uploadImageList.add(CommonMethod.convertBitmaptoString(bitmap));

        CommonMethod.setGridRecyclerview(context,rv_upload_image,4);
        rv_upload_image.setAdapter(new UploadImageAdapterT(uploadImageList));

    }


    private void callApi() {
        final ProgressD pd = ProgressD.show(context, getString(R.string.connecting), true);
        String appBaseUrl = LoginPreferences.getActiveInstance(context).getBASE_URL();
        Log.e("URL---", appBaseUrl);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, appBaseUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.dismiss();
                        Log.e("Response--", response);
                        Gson gson = new Gson();
                        CommonResponse result = gson.fromJson(String.valueOf(response), CommonResponse.class);

                        if (result.result) {
                            AppData.getInstance().setfrom(null);
                            UploadImageAdapterT.list.clear();
                            CommonMethod.showFinishAlert(result.message, getActivity());
                        } else {
                            CommonMethod.showAlert(result.message, context);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                        Log.e("error--", error.toString());
                        Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                params.put("api_page", ConstantURL.HOMEWORK_CREATE);
                params.put("class_id", AppData.getInstance().getClassId());
                params.put("emp_id", LoginPreferences.getActiveInstance(context).getId());
                params.put("school_id", LoginPreferences.getActiveInstance(context).getSchoolId());
                params.put("branch_id", LoginPreferences.getActiveInstance(context).getBranchId());
                params.put("session_id", LoginPreferences.getActiveInstance(context).getSessionId());
                params.put("files", UploadImageAdapterT.list.toString().replace("[", "").replace("]", ""));
                params.put("subject_id", AppData.getInstance().getSubjectId());
                params.put("description", description.getText().toString().trim());
                params.put("notify_type_app", notify_type_app);
                params.put("notify_type_phone", notify_type_phone);

                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }

}
