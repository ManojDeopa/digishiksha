package com.smart.school.teacher.adaptersT;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.smart.school.R;
import com.smart.school.utils.AppData;
import com.smart.school.utils.CommonMethod;

import java.util.ArrayList;


/**
 * Created by Manoj Singh Deopa .
 */

public class UploadImageAdapterT extends RecyclerView.Adapter<UploadImageAdapterT.ViewHolder> {
    public static ArrayList<String> list=new ArrayList<>();
    private Context context;

    public UploadImageAdapterT(ArrayList<String> uploadImageList) {
        list = uploadImageList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.upload_image_adapter, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        CommonMethod.setBase64Image(holder.image, list.get(position));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView image, delete;

        public ViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            delete = view.findViewById(R.id.delete);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    list.remove(getAdapterPosition());
                    notifyItemRemoved(getAdapterPosition());
                    notifyItemRangeChanged(getAdapterPosition(), list.size());
                }
            });
        }
    }
}