package com.smart.school.teacher.fragmentsT;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.smart.school.R;
import com.smart.school.teacher.activityT.QuickActionDetails;
import com.smart.school.teacher.adaptersT.ClassListAdapter;
import com.smart.school.teacher.responsesT.LoginResponse;
import com.smart.school.teacher.responsesT.SectionResponse;
import com.smart.school.teacher.responsesT.ViewHwResponse;
import com.smart.school.utils.AppData;
import com.smart.school.utils.ClassListDatabase;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.ConstantURL;
import com.smart.school.utils.Constants;
import com.smart.school.utils.LoginPreferences;
import com.smart.school.utils.ProgressD;
import com.smart.school.utils.VolleySingleton;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Manoj Singh Deopa on 2/4/18.
 */

@SuppressLint("ValidFragment")

public class ViewHomeworkT extends Fragment {

    TextView date, class_name;
    private Context context;
    private View view;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.view_homework, container, false);

        initViews();

        setCurrentDate();

        onclicks();

        return view;

    }

    private void validate() {
        if (TextUtils.isEmpty(class_name.getText().toString().trim())) {
            Toast.makeText(context, "Please Select Class", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(date.getText().toString().trim())) {
            Toast.makeText(context, "Please Select Date", Toast.LENGTH_SHORT).show();
        } else {
            callApi();
        }
    }

    private void initViews() {
        context = getActivity();
        date = view.findViewById(R.id.date);
        class_name = view.findViewById(R.id.class_name);
    }

    private void onclicks() {

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethod.showDatePicker(context, date);
            }
        });

        class_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethod.showClassDialog(context,class_name);
            }
        });


        view.findViewById(R.id.btn_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate();
            }
        });


    }


    private void setCurrentDate() {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(c);
        date.setText(formattedDate);
    }

    private void callApi() {
        String appBaseUrl = LoginPreferences.getActiveInstance(context).getBASE_URL();
        Log.e("URL---", appBaseUrl);
        final ProgressD pd = ProgressD.show(context, context.getResources().getString(R.string.connecting), true);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, appBaseUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.dismiss();
                        Log.e("Response--", response);
                        Gson gson = new Gson();
                        ViewHwResponse result = gson.fromJson(String.valueOf(response), ViewHwResponse.class);

                        if (result.result) {
                            if (result.data.size() != 0) {
                                AppData.getInstance().setHwDetail(result.data);
                                startActivity(new Intent(context, QuickActionDetails.class).putExtra(Constants.TYPE, Constants.VIEW_HW_DETAIL));
                            } else {
                                Toast.makeText(context, "Homework Not Available", Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            CommonMethod.showAlert(result.message, context);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                        Log.e("error--", error.toString());
                        Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("api_page", ConstantURL.HOMEWORK_VIEW);
                params.put("class_id", AppData.getInstance().getClassId());
                params.put("date", date.getText().toString().trim());
                params.put("school_id", LoginPreferences.getActiveInstance(context).getSchoolId());
                params.put("branch_id", LoginPreferences.getActiveInstance(context).getBranchId());
                params.put("session_id", LoginPreferences.getActiveInstance(context).getSessionId());
                params.put("emp_id", LoginPreferences.getActiveInstance(context).getId());
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }


}
