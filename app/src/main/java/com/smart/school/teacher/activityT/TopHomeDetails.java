package com.smart.school.teacher.activityT;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

import com.smart.school.R;
import com.smart.school.teacher.fragmentsT.HomeworkTabFragment;
import com.smart.school.teacher.fragmentsT.StudentList;
import com.smart.school.utils.AppData;
import com.smart.school.utils.AppUtils;
import com.smart.school.utils.Constants;

public class TopHomeDetails extends AppCompatActivity {

    private static final String TAG = "TopHomeDetails";

    private Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.quick_action_details);

        context = TopHomeDetails.this;

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String type = getIntent().getStringExtra(Constants.TYPE);

        getSupportActionBar().setTitle(type);

        setFragment(type);


    }

    private void setFragment(String type) {

        Fragment fragment = new Fragment();

        switch (type) {
            case Constants.TYPE_STUDENT_LIST:
                fragment = new StudentList(Constants.TYPE_STUDENT_LIST);
                break;

            case Constants.TYPE_MARK_ATTENDENCE:
                fragment = new StudentList(Constants.TYPE_MARK_ATTENDENCE);
                break;

            case Constants.TYPE_WRITE_HOMEWORK:
                fragment = new HomeworkTabFragment();
                break;

            case Constants.UPLOAD_STUDENT_PHOTO:
                AppData.getInstance().setfrom(Constants.UPLOAD_STUDENT_PHOTO);
                fragment = new StudentList(Constants.TYPE_STUDENT_LIST);
                break;

            default:
                Toast.makeText(context, "No Data Found..", Toast.LENGTH_SHORT).show();
                finish();
                break;

        }

        AppUtils.setFragment(fragment, true, TopHomeDetails.this, R.id.container);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        finish();
    }


}
