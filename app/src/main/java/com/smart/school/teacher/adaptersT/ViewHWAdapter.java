package com.smart.school.teacher.adaptersT;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.teacher.responsesT.ViewHwResponse;
import com.smart.school.utils.AppData;
import com.smart.school.utils.LoginPreferences;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Manoj Singh Deopa.
 */

public class ViewHWAdapter extends RecyclerView.Adapter<ViewHWAdapter.ViewHolder> {

    private List<ViewHwResponse.Datum> list;
    private Context context;

    public ViewHWAdapter(List<ViewHwResponse.Datum> hwDetail) {
        this.list = hwDetail;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_hw_detail_adapter, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int i) {
        holder.subject.setText(list.get(i).subjectName);
        holder.created_at.setText("Created At : " + list.get(i).created);
        holder.description.setText(list.get(i).description);

        if (list.get(i).files.size() == 0) {
            holder.no_attachment.setVisibility(View.VISIBLE);
        } else {
            holder.no_attachment.setVisibility(View.GONE);
            ArrayList<String> imageList = new ArrayList<>();
            String BaseUrl = LoginPreferences.getActiveInstance(context).getImgBaseUrl();
            for (int i1 = 0; i1 < list.get(i).files.size(); i1++) {
                imageList.add(BaseUrl + list.get(i).files.get(i1).attachFile);
            }
            setAttachment(holder, imageList);
        }
    }

    private void setAttachment(ViewHolder holder, ArrayList<String> imageList) {
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 4);
        holder.rv_attachments.setLayoutManager(layoutManager);
        holder.rv_attachments.setItemAnimator(new DefaultItemAnimator());
        holder.rv_attachments.setAdapter(new HWImageAdapter(imageList));
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView subject, created_at, description, no_attachment;
        RecyclerView rv_attachments;

        public ViewHolder(View view) {
            super(view);
            subject = view.findViewById(R.id.subject);
            created_at = view.findViewById(R.id.created_at);
            description = view.findViewById(R.id.description);
            rv_attachments = view.findViewById(R.id.recycler_view);
            no_attachment = view.findViewById(R.id.no_attachment);
        }
    }
}