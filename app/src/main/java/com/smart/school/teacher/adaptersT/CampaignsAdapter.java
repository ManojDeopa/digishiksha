package com.smart.school.teacher.adaptersT;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.teacher.fragmentsT.NotificationFragment;
import com.smart.school.teacher.interfacesT.MenuItemListener;


/**
 * Created by Manoj Singh Deopa.
 */

public class CampaignsAdapter extends RecyclerView.Adapter<CampaignsAdapter.ViewHolder> {

    private final MenuItemListener listener;
    private Context context;

    public CampaignsAdapter(NotificationFragment listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.campaigns_item_layout, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
    }


    @Override
    public int getItemCount() {
        return 10;
    }

    private void ShowOptionMenu(TextView tv_option_menu, final int adapterPosition) {
        PopupMenu popup = new PopupMenu(context, tv_option_menu);
        popup.inflate(R.menu.issue_item_menu);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu1:
                        listener.onUpdateClick(adapterPosition);
                        break;
                }
                return false;
            }
        });
        popup.show();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_option_menu;

        public ViewHolder(View view) {
            super(view);
            tv_option_menu = view.findViewById(R.id.tv_option_menu);
            tv_option_menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ShowOptionMenu(tv_option_menu, getAdapterPosition());
                }
            });
        }
    }


}