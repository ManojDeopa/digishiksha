package com.smart.school.teacher.adaptersT;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.teacher.activityT.UploadImage_MarkAttendance;
import com.smart.school.teacher.responsesT.StudentListResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


/**
 * Created by Manoj Singh Deopa.
 */

public class MarkStudentAdapter extends RecyclerView.Adapter<MarkStudentAdapter.ViewHolder> {

    List<String> colors;
    private List<StudentListResponse.Datum> list;
    private Context context;


    public MarkStudentAdapter(List<StudentListResponse.Datum> list) {
        this.list = list;
        colors = new ArrayList<String>();
        colors.add("#5E97F6");
        colors.add("#9CCC65");
        colors.add("#FF8A65");
        colors.add("#9E9E9E");
        colors.add("#9FA8DA");
        colors.add("#90A4AE");
        colors.add("#AED581");
        colors.add("#F6BF26");
        colors.add("#FFA726");
        colors.add("#4DD0E1");
        colors.add("#BA68C8");
        colors.add("#A1887F");
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.mark_student_item, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        holder.name.setText(list.get(position).t2StudentName);

        if (list.get(position).t2_gender.equals("female")) {
            holder.fname.setText(
                    "D/O - " + list.get(position).t3FatherName + " ( " + list.get(position).t3FatherMobileNo + " ) ");

        } else {
            holder.fname.setText(
                    "S/O - " + list.get(position).t3FatherName + " ( " + list.get(position).t3FatherMobileNo + " ) ");

        }


        Random r = new Random();
        int i1 = r.nextInt(11 - 0) + 0;

        GradientDrawable draw = new GradientDrawable();
        draw.setShape(GradientDrawable.OVAL);
        draw.setColor(Color.parseColor(colors.get(i1)));


        String test = list.get(position).t2StudentName;
        String firstText = test.substring(0, 1);

        holder.txtIcon.setBackground(draw);
        holder.txtIcon.setText(firstText);

        if (list.get(position).t6_attendance == null) {
            holder.parent.setBackgroundColor(context.getResources().getColor(R.color.present_color));
            holder.attendance_switch.setChecked(true);
            holder.attendance_switch.setText("Present");
            UploadImage_MarkAttendance.list.get(position).setAbsent(false);
            holder.attendance_switch.setTextColor(context.getResources().getColor(R.color.dark_green));
        } else {
            holder.attendance_switch.setChecked(false);
            holder.attendance_switch.setText("Absent");
            holder.attendance_switch.setTextColor(Color.RED);
            UploadImage_MarkAttendance.list.get(position).setAbsent(true);
            holder.parent.setBackgroundColor(context.getResources().getColor(R.color.absent_color));
        }


        /*holder.class_name.setText(list.get(position).t4CourseName + " - " + list.get(position).t5SectionName);*/
        holder.admission_no.setText("Adm. No : " + list.get(position).t1AdmissionNo +
                " | " + "DOB :" + list.get(position).t2_dob);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name, fname, txtIcon;
        TextView admission_no;
        SwitchCompat attendance_switch;
        LinearLayout parent;

        public ViewHolder(View view) {
            super(view);

            name = view.findViewById(R.id.name);
            fname = view.findViewById(R.id.fname);
            txtIcon = view.findViewById(R.id.txtIcon);
            admission_no = view.findViewById(R.id.admission_no);
            attendance_switch = view.findViewById(R.id.attendance_switch);
            parent = view.findViewById(R.id.parent);


            parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (attendance_switch.isChecked()) {
                        parent.setBackgroundColor(context.getResources().getColor(R.color.absent_color));
                        attendance_switch.setChecked(false);
                        attendance_switch.setText("Absent");
                        attendance_switch.setTextColor(Color.RED);
                        UploadImage_MarkAttendance.list.get(getAdapterPosition()).setAbsent(true);
                        list.get(getAdapterPosition()).setT6_attendance("1");

                    } else {
                        UploadImage_MarkAttendance.list.get(getAdapterPosition()).setAbsent(false);
                        parent.setBackgroundColor(context.getResources().getColor(R.color.present_color));
                        attendance_switch.setChecked(true);
                        attendance_switch.setText("Present");
                        list.get(getAdapterPosition()).setT6_attendance(null);
                        attendance_switch.setTextColor(context.getResources().getColor(R.color.dark_green));
                    }

                }
            });


            attendance_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    if (isChecked) {
                        UploadImage_MarkAttendance.list.get(getAdapterPosition()).setAbsent(false);
                        parent.setBackgroundColor(context.getResources().getColor(R.color.present_color));
                        attendance_switch.setChecked(true);
                        attendance_switch.setText("Present");
                        list.get(getAdapterPosition()).setT6_attendance(null);
                        attendance_switch.setTextColor(context.getResources().getColor(R.color.dark_green));

                    } else {
                        list.get(getAdapterPosition()).setT6_attendance("1");
                        parent.setBackgroundColor(context.getResources().getColor(R.color.absent_color));
                        attendance_switch.setChecked(false);
                        attendance_switch.setText("Absent");
                        attendance_switch.setTextColor(Color.RED);
                        UploadImage_MarkAttendance.list.get(getAdapterPosition()).setAbsent(true);
                    }
                }
            });
        }
    }


}