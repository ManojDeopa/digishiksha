package com.smart.school.teacher.adaptersT;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.teacher.responsesT.LoginResponse;
import com.smart.school.utils.AppData;

import java.util.List;


/**
 * Created by Manoj Singh Deopa.
 */

public class ClassListAdapter extends RecyclerView.Adapter<ClassListAdapter.ViewHolder> {

    private List<LoginResponse.ClassVal> list;
    private TextView class_name;
    private Dialog dialog;
    private Context context;

    public ClassListAdapter(Dialog dialog, List<LoginResponse.ClassVal> classlist, TextView class_name) {
        this.dialog = dialog;
        this.list = classlist;
        this.class_name = class_name;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dialog_list_item, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        holder.text.setText(list.get(position).className);

    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {


        TextView text;
        RadioButton radio_button;


        public ViewHolder(View view) {
            super(view);

            text = view.findViewById(R.id.text);
            radio_button = view.findViewById(R.id.radio_button);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    class_name.setText(list.get(getAdapterPosition()).className);
                    AppData.getInstance().setClassId(list.get(getAdapterPosition()).id);
                    dialog.dismiss();
                }
            });

            radio_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    class_name.setText(list.get(getAdapterPosition()).className);
                    AppData.getInstance().setClassId(list.get(getAdapterPosition()).id);
                    dialog.dismiss();

                }
            });

        }
    }


}