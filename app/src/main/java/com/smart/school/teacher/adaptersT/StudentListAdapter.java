package com.smart.school.teacher.adaptersT;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.smart.school.BuildConfig;
import com.smart.school.R;
import com.smart.school.teacher.activityT.UploadImage_MarkAttendance;
import com.smart.school.teacher.activityT.QuickActionDetails;
import com.smart.school.teacher.responsesT.CommonResponse;
import com.smart.school.teacher.responsesT.StudentListResponse;
import com.smart.school.utils.AppData;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.ConstantURL;
import com.smart.school.utils.Constants;
import com.smart.school.utils.LoginPreferences;
import com.smart.school.utils.ProgressD;
import com.smart.school.utils.VolleySingleton;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;


/**
 * Created by Manoj Singh Deopa.
 */

public class StudentListAdapter extends RecyclerView.Adapter<StudentListAdapter.ViewHolder> {

    public static File photoFile;
    public static UploadImage_MarkAttendance context;
    List<String> colors;
    private List<StudentListResponse.Datum> list;
    private int selectedPos;


    public StudentListAdapter(List<StudentListResponse.Datum> list) {
        this.list = list;
        colors = new ArrayList<String>();
        colors.add("#5E97F6");
        colors.add("#9CCC65");
        colors.add("#FF8A65");
        colors.add("#9E9E9E");
        colors.add("#9FA8DA");
        colors.add("#90A4AE");
        colors.add("#AED581");// Get safe storage directory for photos
        // Use `getExternalFilesDir` on Context to access package-specific directories.
        // This way, we don't need to request external read/write runtime permissions.
        colors.add("#F6BF26");
        colors.add("#FFA726");
        colors.add("#4DD0E1");
        colors.add("#BA68C8");
        colors.add("#A1887F");
    }

    public static void selectImage() {
        final CharSequence[] items = {"Take Photo", "Select From Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(UploadImage_MarkAttendance.activity);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    onLaunchCamera();

                } else if (items[item].equals("Select From Gallery")) {
                    Crop.pickImage(UploadImage_MarkAttendance.activity);
                }
            }
        });
        builder.show();
    }

    public static void onLaunchCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        photoFile = getPhotoFileUri("photo.jpg");
        // wrap File object into a content provider
        // required for API >= 24
        // See https://guides.codepath.com/android/Sharing-Content-with-Intents#sharing-files-with-api-24-or-higher

        Uri fileProvider = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", photoFile);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileProvider);

        // If you call startActivityForResult() using an intent that no app can handle, your app will crash.
        // So as long as the result is not null, it's safe to use the intent.
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            // Start the image capture intent to take photo
            context.startActivityForResult(intent, 3);
        }
    }

    public static File getPhotoFileUri(String fileName) {
        // Get safe storage directory for photos
        // Use `getExternalFilesDir` on Context to access package-specific directories.
        // This way, we don't need to request external read/write runtime permissions.
        File mediaStorageDir = new File(UploadImage_MarkAttendance.activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "DigiShiksha");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) {
            Log.e("DigiShiksha--", "failed to create directory");
        }

        File file = new File(mediaStorageDir.getPath() + File.separator + fileName);
        return file;
    }

    public static void callAlert(final String number) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(number);

        alertDialog.setPositiveButton("Call", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                CommonMethod.callPhoneIntent(context, number);
            }
        });

        // Showing Alert Message
        alertDialog.show();

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.student_list_item, parent, false);
        context = UploadImage_MarkAttendance.activity;
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        holder.name.setText(list.get(position).t2StudentName);

        if (list.get(position).t2_gender.equals("female")) {
            holder.fname.setText(
                    "D/O - " + list.get(position).t3FatherName + " ( " + list.get(position).t3FatherMobileNo + " ) ");

        } else {
            holder.fname.setText(
                    "S/O - " + list.get(position).t3FatherName + " ( " + list.get(position).t3FatherMobileNo + " ) ");

        }


        Random r = new Random();
        int i1 = r.nextInt(11 - 0) + 0;

        GradientDrawable draw = new GradientDrawable();
        draw.setShape(GradientDrawable.OVAL);
        draw.setColor(Color.parseColor(colors.get(i1)));


        String test = list.get(position).t2StudentName;
        String firstText = test.substring(0, 1);

        String image = list.get(position).t1_student_photo;

        if (!TextUtils.isEmpty(image)) {
            holder.image.setVisibility(View.VISIBLE);
            holder.txtIcon.setVisibility(View.GONE);

            CommonMethod.loadResizeImage(holder.image, LoginPreferences.getActiveInstance(context).getImgBaseUrl() + image);

        } else {
            holder.image.setVisibility(View.GONE);
            holder.txtIcon.setVisibility(View.VISIBLE);
            holder.txtIcon.setBackground(draw);
            holder.txtIcon.setText(firstText);
        }


        holder.class_name.setText(list.get(position).t4CourseName + " - " + list.get(position).t5SectionName);
        holder.admission_no.setText("Adm. No : " + list.get(position).t1AdmissionNo +
                " | " + "DOB :" + list.get(position).t2_dob);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private void showAlert(final StudentListResponse.Datum datum) {
        ViewGroup viewGroup = context.findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(context).inflate(R.layout.edit_delete_layout, viewGroup, false);

        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(context);
        builder.setView(dialogView);

        final android.support.v7.app.AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.setCancelable(true);


        dialogView.findViewById(R.id.call).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                String number = datum.t3FatherMobileNo;
                if (!TextUtils.isEmpty(number))
                    CommonMethod.callPhoneIntent(context, number);
            }
        });

        dialogView.findViewById(R.id.edit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                AppData.getInstance().setStudentData(datum);
                context.startActivity(new Intent(context, QuickActionDetails.class).putExtra(Constants.TYPE, Constants.TYPE_EDIT_STUDENT));

            }
        });

        dialogView.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                showDeleteAlert(datum.t2StudentName, datum.t1Id);

            }
        });


    }


    private void showDeleteAlert(String t2StudentName, final String t1Id) {
        String text = "Are you sure to Delete ";
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(text + t2StudentName + " ? ");

        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                deleteStudent(t1Id);
                dialog.dismiss();
            }
        });


        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }


    private void deleteStudent(final String student_id) {
        String appBaseUrl = LoginPreferences.getActiveInstance(context).getBASE_URL();
        Log.e("URL---", appBaseUrl);
        final ProgressD pd = ProgressD.show(context, context.getResources().getString(R.string.connecting), true);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, appBaseUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.dismiss();

                        Log.e("Response--", response);

                        Gson gson = new Gson();
                        CommonResponse result = gson.fromJson(String.valueOf(response), CommonResponse.class);

                        if (result.result) {
                            removeAt();
                            Toast.makeText(context, result.message, Toast.LENGTH_SHORT).show();
                        } else {
                            CommonMethod.showAlert(result.message, context);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                        Log.e("error--", error.toString());
                        Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                Log.e("Class ID--", AppData.getInstance().getClassId());
                Log.e("EMP ID--", LoginPreferences.getActiveInstance(context).getId());
                Log.e("school ID--", LoginPreferences.getActiveInstance(context).getSchoolId());
                Log.e("branch ID--", LoginPreferences.getActiveInstance(context).getBranchId());

                Map<String, String> params = new HashMap<>();
                params.put("api_page", ConstantURL.DELETE_STUDENT);
                params.put("emp_id", LoginPreferences.getActiveInstance(context).getId());
                params.put("school_id", LoginPreferences.getActiveInstance(context).getSchoolId());
                params.put("branch_id", LoginPreferences.getActiveInstance(context).getBranchId());
                params.put("session_id", LoginPreferences.getActiveInstance(context).getSessionId());


                params.put("student_id", student_id);
                /*params.put("class_id", AppData.getInstance().getClassId());*/


                return params;
            }
        };

        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);

    }

    private void removeAt() {
        list.remove(selectedPos);
        notifyItemRemoved(selectedPos);
        notifyItemRangeChanged(selectedPos, list.size());
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name, fname, txtIcon;
        TextView admission_no, class_name;
        LinearLayout parent;
        ImageView image;

        public ViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            fname = view.findViewById(R.id.fname);
            txtIcon = view.findViewById(R.id.txtIcon);
            admission_no = view.findViewById(R.id.admission_no);
            class_name = view.findViewById(R.id.class_name);
            parent = view.findViewById(R.id.parent);
            image = view.findViewById(R.id.image);


            parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (AppData.getInstance().getFrom()) {
                        case Constants.UPLOAD_STUDENT_PHOTO:
                            AppData.getInstance().setPosition(getAdapterPosition());
                            AppData.getInstance().setStudentId(list.get(getAdapterPosition()).t1Id);
                            selectImage();
                            break;
                        case Constants.TYPE_STUDENT_LIST:
                            if (AppData.getInstance().getType().equals(Constants.TYPE_MASTER_ADMIN)) {
                                selectedPos = getAdapterPosition();
                                showAlert(list.get(getAdapterPosition()));
                            }
                            break;

                        default:

                            break;
                    }
                }
            });
        }
    }


}