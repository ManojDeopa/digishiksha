package com.smart.school.teacher.activityT;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.smart.school.R;

import java.util.HashMap;

public class StudentDetail extends AppCompatActivity {

    private static final String TAG = "StudentDetail";
    private EditText et_name, et_designation, et_pincode, et_email, et_phone, et_address;
    private FloatingActionButton btn_edit, btn_doneAll;
    private Context context;
    private HashMap<String, String> userDetail;
    private LinearLayout content_layout_linear;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_detail);
        context = StudentDetail.this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        _findIDs();

        _onClick();

    }


    private void _onClick() {

        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!et_name.isEnabled()) {
                    _setEditable(true);
                } else {
                    _setEditable(false);
                }

            }
        });


    }

    private void _setEditable(boolean b) {
        et_name.setEnabled(b);
        et_designation.setEnabled(b);
        et_pincode.setEnabled(b);
        et_email.setEnabled(b);
        et_phone.setEnabled(b);
        et_address.setEnabled(b);

        if (!b) {
            btn_doneAll.setVisibility(View.GONE);
        } else {
            btn_doneAll.setVisibility(View.VISIBLE);
        }

    }

    private void _findIDs() {

        et_name = findViewById(R.id.et_name);
        et_designation = findViewById(R.id.et_designation);
        et_pincode = findViewById(R.id.et_pincode);
        et_email = findViewById(R.id.et_email);
        et_phone = findViewById(R.id.et_phone);
        et_address = findViewById(R.id.et_address);

        et_name.setText("User Name");
        et_designation.setText("Accountant");
        et_pincode.setText("201301");
        et_email.setText("email@email.com");
        et_phone.setText("9988776655");
        et_address.setText("Greater Noida , U.P.");

        btn_edit = findViewById(R.id.btn_edit);
        btn_doneAll = findViewById(R.id.btn_doneAll);

        _setEditable(false);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        finish();
    }

}
