package com.smart.school.teacher.adaptersT;

/**
 * Created by manoj on 3/2/19.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.teacher.responsesT.MenuModel;
import com.smart.school.utils.Constants;
import com.smart.school.utils.LoginPreferences;

import java.util.HashMap;
import java.util.List;

public class ExpandableListAdapterT extends BaseExpandableListAdapter {

    int[] icons = {
            R.drawable.ic_home,
            R.drawable.ic_quick_action,
            R.drawable.ic_important_links,
            R.drawable.ic_about_school,
            R.drawable.ic_share,
            R.drawable.ic_important_links,
            R.drawable.ic_info,
            R.drawable.ic_app_tutorial,
            R.drawable.ic_logout_blue,
    };
    int[] iconsMA = {
            R.drawable.ic_home,
            R.drawable.ic_quick_action,
            R.drawable.ic_communication,
            R.drawable.ic_account,
            R.drawable.ic_share,
            R.drawable.ic_important_links,
            R.drawable.ic_info,
            R.drawable.ic_app_tutorial,
            R.drawable.ic_logout_blue,
    };
    private Context context;
    private List<MenuModel> listDataHeader;
    private HashMap<MenuModel, List<MenuModel>> listDataChild;

    public ExpandableListAdapterT(Context context, List<MenuModel> listDataHeader,
                                  HashMap<MenuModel, List<MenuModel>> listChildData) {
        this.context = context;
        this.listDataHeader = listDataHeader;
        this.listDataChild = listChildData;
    }

    @Override
    public MenuModel getChild(int groupPosition, int childPosititon) {
        return this.listDataChild.get(this.listDataHeader.get(groupPosition)).get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = getChild(groupPosition, childPosition).menuName;

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group_child, null);
        }

        TextView txtListChild = convertView.findViewById(R.id.lblListItem);
        txtListChild.setText(childText);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        if (this.listDataChild.get(this.listDataHeader.get(groupPosition)) == null)
            return 0;
        else
            return this.listDataChild.get(this.listDataHeader.get(groupPosition))
                    .size();
    }

    @Override
    public MenuModel getGroup(int groupPosition) {
        return this.listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.listDataHeader.size();

    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String headerTitle = getGroup(groupPosition).menuName;
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group_header, null);
        }

        ImageView indicator = convertView.findViewById(R.id.indicator);

        if (getChildrenCount(groupPosition) == 0) {
            indicator.setVisibility(View.INVISIBLE);
        } else {
            indicator.setVisibility(View.VISIBLE);
            indicator.setImageResource(isExpanded ? R.drawable.ic_expand_less : R.drawable.ic_expand_more);
        }

        TextView lblListHeader = convertView.findViewById(R.id.lblListHeader);
        ImageView image = convertView.findViewById(R.id.image);

        if (LoginPreferences.getActiveInstance(context).getType().equals(Constants.TYPE_MASTER_ADMIN)) {
            image.setImageResource(iconsMA[groupPosition]);
        } else {
            image.setImageResource(icons[groupPosition]);
        }

        lblListHeader.setText(headerTitle);
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
