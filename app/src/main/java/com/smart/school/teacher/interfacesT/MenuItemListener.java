package com.smart.school.teacher.interfacesT;

/**
 * Created by manoj on 1/6/18.
 */
public interface MenuItemListener {

    void onUpdateClick(int pos);

}
