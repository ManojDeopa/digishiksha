package com.smart.school.teacher.activityT;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.smart.school.R;
import com.smart.school.teacher.adaptersT.ConfirmStudentAdapter;
import com.smart.school.teacher.adaptersT.MarkStudentAdapter;
import com.smart.school.teacher.adaptersT.StudentListAdapter;
import com.smart.school.teacher.fragmentsT.StudentList;
import com.smart.school.teacher.responsesT.CommonResponse;
import com.smart.school.teacher.responsesT.StudentListResponse;
import com.smart.school.teacher.responsesT.UploadImageResponse;
import com.smart.school.utils.AppData;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.ConstantURL;
import com.smart.school.utils.Constants;
import com.smart.school.utils.LoginPreferences;
import com.smart.school.utils.ProgressD;
import com.smart.school.utils.VolleySingleton;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Manoj Singh Deopa on 2/4/18.
 */


public class UploadImage_MarkAttendance extends AppCompatActivity implements View.OnClickListener {
    public static List<StudentListResponse.Datum> list;
    public static ArrayList<String> absentStudentsName = new ArrayList<>();
    public static ArrayList<String> absentStudentsID = new ArrayList<>();
    public static RecyclerView recyclerView;
    public static UploadImage_MarkAttendance activity;
    public static int CAMERA_REQUEST = 3;
    private Context context;
    private RecyclerView.Adapter adapter;
    private Toolbar toolbar;
    private TextView tv_title;
    private FloatingActionButton btn_doneAll;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.student_list_view);

        context = UploadImage_MarkAttendance.this;
        activity = UploadImage_MarkAttendance.this;
        toolbar = findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        btn_doneAll = findViewById(R.id.btn_doneAll);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        recyclerView = findViewById(R.id.recycler_view);
        CommonMethod.setRecyclerView(recyclerView, context);

        String type = getIntent().getStringExtra("TYPE");
        list = AppData.getInstance().getStudentList();


        if (type.equals(Constants.TYPE_MARK_ATTENDENCE)) {
            btn_doneAll.setOnClickListener(this);
            tv_title.setText("Mark Student");
            adapter = new MarkStudentAdapter(list);
        } else {
            btn_doneAll.setVisibility(View.GONE);
            tv_title.setText(Constants.TYPE_STUDENT_LIST);
            adapter = new StudentListAdapter(list);

        }
        recyclerView.setAdapter(adapter);
    }

    private void getAbsentStudentData() {
        absentStudentsID.clear();
        absentStudentsName.clear();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).isAbsent) {

                absentStudentsName.add("Student Name : " + list.get(i).t2StudentName + "\n"
                        + "F.Name : " + list.get(i).t3FatherName + "\n"
                        + "Adm.No. :" + list.get(i).t1AdmissionNo + "\n"
                        + "DOB :" + list.get(i).t2_dob);

                absentStudentsID.add(list.get(i).t1Id);


            } else {
                Log.e("Size--", String.valueOf(absentStudentsID.size()));
                Log.e("i--", String.valueOf(i));
            }


        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {

        if (v == btn_doneAll) {

            getAbsentStudentData();

            if (!absentStudentsName.isEmpty()) {
                confirmAttendance(absentStudentsName);
            } else {
                callAttendenceApi();
            }
        }

    }


    public void confirmAttendance(ArrayList<String> absentStudentsName) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        builder.setTitle("Confirm Attendance");
        View dialogLayout = inflater.inflate(R.layout.confirm_attendance, null);
        RecyclerView recyclerView = dialogLayout.findViewById(R.id.recycler_view);
        CommonMethod.setRecyclerView(recyclerView, context);

        recyclerView.setAdapter(new ConfirmStudentAdapter(absentStudentsName));

        builder.setView(dialogLayout);

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                callAttendenceApi();
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.setCancelable(false);
        builder.show();
    }


    private void callAttendenceApi() {
        final ProgressD pd = ProgressD.show(context, getString(R.string.connecting), true);
        String appBaseUrl = LoginPreferences.getActiveInstance(context).getBASE_URL();
        Log.e("URL---", appBaseUrl);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, appBaseUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.dismiss();
                        Log.e("Response--", response);

                        Gson gson = new Gson();
                        CommonResponse result = gson.fromJson(String.valueOf(response), CommonResponse.class);

                        if (result != null) {
                            if (result.result) {
                                CommonMethod.showAlert(result.message, context);

                            } else {
                                CommonMethod.showAlert(result.message, context);
                            }
                        } else {
                            CommonMethod.showAlert("Server Error", context);
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                        Log.e("error--", error.toString());
                        Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Log.e("class_id --", AppData.getInstance().getClassId());
                Log.e("session_id --", LoginPreferences.getActiveInstance(context).getSessionId());
                Log.e("school_id --", LoginPreferences.getActiveInstance(context).getSchoolId());
                Log.e("branch_id --", LoginPreferences.getActiveInstance(context).getBranchId());
                Log.e("student_id --", absentStudentsID.toString().replace("[", "").replace("]", ""));
                Log.e("att_date --", StudentList.date.getText().toString().trim());
                Log.e("api_page --", "mark_attendance");


                Map<String, String> params = new HashMap<>();
                params.put("api_page", ConstantURL.MARK_ATTANDENCE);
                params.put("school_id", LoginPreferences.getActiveInstance(context).getSchoolId());
                params.put("branch_id", LoginPreferences.getActiveInstance(context).getBranchId());
                params.put("session_id", LoginPreferences.getActiveInstance(context).getSessionId());
                params.put("class_id", AppData.getInstance().getClassId());
                params.put("att_date", StudentList.date.getText().toString().trim());
                params.put("student_id", absentStudentsID.toString().replace("[", "").replace("]", ""));
                /* params.put("student_id", "3,3");*/


                return params;
            }
        };

        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);

    }


    @Override
    @SuppressLint("NewApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.e("requestCode-", String.valueOf(requestCode));
        Log.e("resultCode-", String.valueOf(resultCode));
        Log.e("data-", String.valueOf(data));

        if (resultCode != RESULT_OK) {
            return;
        }
        if (requestCode == Crop.REQUEST_PICK) {
            beginCrop(data.getData());
        } else if (requestCode == 3) {
            beginCrop(Uri.fromFile(StudentListAdapter.photoFile));
        } else if (requestCode == Crop.REQUEST_CROP) {
            handleCrop(resultCode, data);
        }
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            final InputStream imageStream;
            try {
                imageStream = getContentResolver().openInputStream(Crop.getOutput(result));
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);

                String encodedImage = CommonMethod.convertBitmaptoString(selectedImage);
                callUploadImageApi(encodedImage);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    private void callUploadImageApi(final String encodedImage) {
        final ProgressD pd = ProgressD.show(context, getString(R.string.connecting), false);
        String appBaseUrl = LoginPreferences.getActiveInstance(context).getBASE_URL();
        Log.e("URL---", appBaseUrl);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, appBaseUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.dismiss();
                        Log.e("Response--", response);
                        Gson gson = new Gson();
                        UploadImageResponse result = gson.fromJson(String.valueOf(response), UploadImageResponse.class);

                        if (result.result) {
                            CommonMethod.showAlert(result.message, context);
                            adapter.notifyDataSetChanged();
                            recyclerView.scrollToPosition(AppData.getInstance().getPosition());
                            list.get(AppData.getInstance().getPosition()).setT1_student_photo(result.data);

                        } else {
                            CommonMethod.showAlert(result.message, context);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                        Log.e("error--", error.toString());
                        Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {


                Log.e("session_id --", LoginPreferences.getActiveInstance(context).getSessionId());
                Log.e("school_id --", LoginPreferences.getActiveInstance(context).getSchoolId());
                Log.e("branch_id --", LoginPreferences.getActiveInstance(context).getBranchId());
                Log.e("student_id --", AppData.getInstance().getStudentId());
                Log.e("base_photo --", encodedImage);


                Map<String, String> params = new HashMap<>();
                params.put("api_page", ConstantURL.STUDENT_PHOTO_UPLOAD);
                params.put("school_id", LoginPreferences.getActiveInstance(context).getSchoolId());
                params.put("branch_id", LoginPreferences.getActiveInstance(context).getBranchId());
                params.put("session_id", LoginPreferences.getActiveInstance(context).getSessionId());
                params.put("student_id", AppData.getInstance().getStudentId());
                params.put("base_photo", encodedImage);
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }


}
