package com.smart.school.teacher.activityT;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.smart.school.R;
import com.smart.school.masterAdmin.fragmentsMA.AddStudent;
import com.smart.school.masterAdmin.fragmentsMA.SendNotification;
import com.smart.school.masterAdmin.fragmentsMA.SendNotificationTab;
import com.smart.school.teacher.fragmentsT.ClassList;
import com.smart.school.teacher.fragmentsT.HomeworkTabFragment;
import com.smart.school.teacher.fragmentsT.StudentFeedbackT;
import com.smart.school.teacher.fragmentsT.StudentList;
import com.smart.school.teacher.fragmentsT.ViewHwDetail;
import com.smart.school.utils.AppData;
import com.smart.school.utils.AppUtils;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.Constants;

public class QuickActionDetails extends AppCompatActivity {

    private static final String TAG = "QuickActionDetails";
    public static QuickActionDetails activity;
    public String type;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quick_action_details);
        context = QuickActionDetails.this;
        activity = QuickActionDetails.this;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        type = getIntent().getStringExtra(Constants.TYPE);
        getSupportActionBar().setTitle(type);
        setFragment(type);


    }

    private void setFragment(String type) {

        android.support.v4.app.Fragment fragment = new Fragment();

        switch (type) {

            case Constants.TYPE_ADD_STUDENT:
                fragment = new AddStudent();
                break;

            case Constants.TYPE_EDIT_STUDENT:
                fragment = new AddStudent();
                break;

            case Constants.TYPE_STUDENT_LIST:
                AppData.getInstance().setfrom(type);
                fragment = new ClassList();

                break;

            case Constants.TYPE_MARK_ATTENDENCE:
                fragment = new StudentList(Constants.TYPE_MARK_ATTENDENCE);
                break;

            case Constants.TYPE_WRITE_HOMEWORK:
                fragment = new HomeworkTabFragment();
                break;

            case Constants.TYPE_UPLOAD_HOMEWORK:
                fragment = new HomeworkTabFragment();
                break;

            case Constants.UPLOAD_STUDENT_PHOTO:
                AppData.getInstance().setfrom(type);
                fragment = new ClassList();
                break;

            case Constants.EXAM_MARKS:
                /*fragment = new ExamAssignmentFragment();*/
                CommonMethod.showFinishAlert("No Data Found", this);
                break;

            case Constants.VIEW_HW_DETAIL:
                fragment = new ViewHwDetail();
                break;

            case Constants.STUDENT_FEEDBACK:
                fragment = new StudentFeedbackT();
                break;


            default:
                CommonMethod.showFinishAlert(Constants.SERVER_ERROR, this);
                break;
        }

        AppUtils.setFragment(fragment, true, QuickActionDetails.this, R.id.container);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        finish();
    }


}
