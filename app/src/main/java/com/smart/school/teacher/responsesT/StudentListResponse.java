package com.smart.school.teacher.responsesT;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by manoj on 9/12/18.
 */
public class StudentListResponse {

    @SerializedName("result")
    @Expose
    public Boolean result;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;


    public class Datum {
        @SerializedName("t1_student_photo")
        @Expose
        public String t1_student_photo;
        @SerializedName("t1_id")
        @Expose
        public String t1Id;
        @SerializedName("t5_section_name")
        @Expose
        public String t5SectionName;
        @SerializedName("t4_course_name")
        @Expose
        public String t4CourseName;
        @SerializedName("t2_student_name")
        @Expose
        public String t2StudentName;
        @SerializedName("t3_father_name")
        @Expose
        public String t3FatherName;
        @SerializedName("t3_mother_name")
        @Expose
        public String t3_mother_name;
        @SerializedName("t2_address")
        @Expose
        public String t2_address;
        @SerializedName("t3_father_mobile_no")
        @Expose
        public String t3FatherMobileNo;
        @SerializedName("t1_admission_no")
        @Expose
        public String t1AdmissionNo;
        @SerializedName("t2_gender")
        @Expose
        public String t2_gender;
        @SerializedName("t2_dob")
        @Expose
        public String t2_dob;
        @SerializedName("t6_attendance")
        @Expose
        public String t6_attendance = null;
        public boolean isAbsent = false;

        public String getT1_student_photo() {
            return t1_student_photo;
        }

        public void setT1_student_photo(String t1_student_photo) {
            this.t1_student_photo = t1_student_photo;
        }

        public String getT1Id() {
            return t1Id;
        }

        public void setT1Id(String t1Id) {
            this.t1Id = t1Id;
        }

        public String getT6_attendance() {
            return t6_attendance;
        }

        public void setT6_attendance(String t6_attendance) {
            this.t6_attendance = t6_attendance;
        }

        public boolean isAbsent() {
            return isAbsent;
        }

        public void setAbsent(boolean absent) {
            isAbsent = absent;
        }


    }

}
