package com.smart.school.teacher.interfacesT;

/**
 * Created by manoj on 2/4/18.
 */

public interface DashboardBottomItemClickListener {
    void bottomItemClicked(int position);
}
