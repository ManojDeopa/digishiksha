package com.smart.school.teacher.fragmentsT;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;

import com.smart.school.R;
import com.smart.school.teacher.adaptersT.CampaignsAdapter;
import com.smart.school.teacher.interfacesT.MenuItemListener;
import com.smart.school.utils.CommonMethod;


/**
 * Created by Manoj Singh Deopa on 2/4/18.
 */

@SuppressLint("ValidFragment")

public class NotificationFragment extends Fragment implements MenuItemListener {

    private RecyclerView recyclerView;
    private Context context;
    private CampaignsAdapter campaignAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.home_fragment, container, false);
        context = getActivity();
        recyclerView = view.findViewById(R.id.recycler_view);
        CommonMethod.setRecyclerView(recyclerView, context);
        campaignAdapter = new CampaignsAdapter(this);
        recyclerView.setAdapter(campaignAdapter);
        return view;
    }


    private void showOptionsDialog(int position) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.update_status_dialog_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        RadioButton rb_others = dialog.findViewById(R.id.rb_others);

        final EditText et_reason = dialog.findViewById(R.id.et_reason);
        et_reason.setVisibility(View.GONE);

        rb_others.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    et_reason.setVisibility(View.VISIBLE);
                } else {
                    et_reason.setVisibility(View.GONE);
                }

            }
        });

        Button btn_done = dialog.findViewById(R.id.btn_done);
        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
        /* FOR SETTING CUSTOM HIGHT AND WIDTH */
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(dialog.getWindow().getAttributes());
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes(layoutParams);
    }


    @Override
    public void onUpdateClick(int pos) {
        showOptionsDialog(pos);
    }
}
