package com.smart.school.teacher.responsesT;

/**
 * Created by manoj on 29/6/18.
 */
public class DashboardModel {

    public String serviceName;
    public int image_offline;

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public int getImage_offline() {
        return image_offline;
    }

    public void setImage_offline(int image_offline) {
        this.image_offline = image_offline;
    }
}
