package com.smart.school.teacher.fragmentsT;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.smart.school.R;
import com.smart.school.teacher.activityT.UploadImage_MarkAttendance;
import com.smart.school.teacher.activityT.QuickActionDetails;
import com.smart.school.teacher.responsesT.StudentListResponse;
import com.smart.school.utils.AppData;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.Constants;
import com.smart.school.utils.LoginPreferences;
import com.smart.school.utils.ProgressD;
import com.smart.school.utils.VolleySingleton;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by Manoj Singh Deopa on 2/4/18.
 */

@SuppressLint("ValidFragment")

public class StudentList extends Fragment implements View.OnClickListener {


    public static TextView date;
    TextView class_name;
    Button btn_submit;
    LinearLayout date_layout, order_by_layout;
    TextView order_by;
    private String type;
    private Context context;


    public StudentList(String typeStudentList) {
        this.type = typeStudentList;
    }



    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.student_list, container, false);
        context = getActivity();


        class_name = view.findViewById(R.id.class_name);
        date = view.findViewById(R.id.date);
        btn_submit = view.findViewById(R.id.btn_submit);
        date_layout = view.findViewById(R.id.date_layout);
        order_by_layout = view.findViewById(R.id.order_by_layout);
        order_by = view.findViewById(R.id.order_by);

        if (type.equals(Constants.TYPE_MARK_ATTENDENCE)) {
            date_layout.setVisibility(View.VISIBLE);
            order_by_layout.setVisibility(View.VISIBLE);
            AppData.getInstance().setOrderByNo("student_full_name");

            Date c = Calendar.getInstance().getTime();

            SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
            String formattedDate = df.format(c);
            date.setText(formattedDate);


        } else {
            date_layout.setVisibility(View.GONE);
            order_by_layout.setVisibility(View.GONE);
        }


        btn_submit.setOnClickListener(this);
        class_name.setOnClickListener(this);
        date.setOnClickListener(this);
        order_by.setOnClickListener(this);


        return view;
    }


    @Override
    public void onClick(View v) {
        if (v == class_name) {
            CommonMethod.showClassDialog(context, class_name);
        }

        if (v == date) {
            CommonMethod.showDatePicker(context, date);
        }

        if (v == order_by) {
            showOrderByDialog();
        }

        if (v == btn_submit) {
            if (TextUtils.isEmpty(class_name.getText().toString().trim())) {
                Toast.makeText(context, "Please Select Class", Toast.LENGTH_SHORT).show();
            } else {
                callApi(type);
            }
        }
    }

    private void showOrderByDialog() {
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.order_by);
        dialog.setTitle("Title...");


        LinearLayout admission_no_wise, roll_no__wise, name_wise;


        admission_no_wise = dialog.findViewById(R.id.admission_no_wise);
        roll_no__wise = dialog.findViewById(R.id.roll_no__wise);
        name_wise = dialog.findViewById(R.id.name_wise);


        admission_no_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                order_by.setText(context.getResources().getString(R.string.admission_no_wise));
                AppData.getInstance().setOrderByNo("admission_no");
                dialog.dismiss();
            }
        });


        roll_no__wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                order_by.setText(context.getResources().getString(R.string.roll_no_wise));
                AppData.getInstance().setOrderByNo("roll_no");
                dialog.dismiss();
            }
        });

        name_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                order_by.setText(context.getResources().getString(R.string.student_name_wise));
                AppData.getInstance().setOrderByNo("student_full_name");
                dialog.dismiss();
            }
        });

        dialog.show();
    }



    private void callApi(final String type) {


        final ProgressD pd = ProgressD.show(context, getString(R.string.connecting), true);
        String appBaseUrl = LoginPreferences.getActiveInstance(context).getBASE_URL();
        Log.e("URL---", appBaseUrl);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, appBaseUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.dismiss();

                        Log.e("Response--", response);

                        Gson gson = new Gson();
                        StudentListResponse result = gson.fromJson(String.valueOf(response), StudentListResponse.class);

                        if (result.result) {
                            if (result.data.size() != 0) {
                                AppData.getInstance().setStudentList(result.data);
                                startActivity(new Intent(context, UploadImage_MarkAttendance.class).putExtra("TYPE", type));
                            }

                        } else {
                            CommonMethod.showAlert(result.message, context);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                        Log.e("error--", error.toString());
                        Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Log.e("Class ID--", AppData.getInstance().getClassId());
                Log.e("session ID--", LoginPreferences.getActiveInstance(context).getSessionId());

                Log.e("school ID--", LoginPreferences.getActiveInstance(context).getSchoolId());
                Log.e("branch ID--", LoginPreferences.getActiveInstance(context).getBranchId());


                Map<String, String> params = new HashMap<>();
                params.put("api_page", "student_list");
                params.put("class_id", AppData.getInstance().getClassId());
                params.put("school_id", LoginPreferences.getActiveInstance(context).getSchoolId());
                params.put("branch_id", LoginPreferences.getActiveInstance(context).getBranchId());
                params.put("session_id", LoginPreferences.getActiveInstance(context).getSessionId());

                if (type.equals(Constants.TYPE_MARK_ATTENDENCE)) {
                    params.put("order_by", AppData.getInstance().getOrderByNo());
                    params.put("att_date", date.getText().toString().trim());
                }


                return params;
            }
        };

        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);

    }

}
