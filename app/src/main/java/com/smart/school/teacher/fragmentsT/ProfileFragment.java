package com.smart.school.teacher.fragmentsT;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smart.school.R;
import com.smart.school.teacher.adaptersT.JobOpeningAdapter;
import com.smart.school.utils.CommonMethod;


/**
 * Created by Manoj Singh Deopa on 2/4/18.
 */

@SuppressLint("ValidFragment")

public class ProfileFragment extends Fragment {

    private RecyclerView recyclerView;
    private Context context;
    private JobOpeningAdapter adapter;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.home_fragment, container, false);
        context = getActivity();
        recyclerView = view.findViewById(R.id.recycler_view);
        CommonMethod.setRecyclerView(recyclerView, context);
        adapter = new JobOpeningAdapter();
        recyclerView.setAdapter(adapter);
        return view;
    }

}
