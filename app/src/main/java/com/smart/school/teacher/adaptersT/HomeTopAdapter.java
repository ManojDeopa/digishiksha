package com.smart.school.teacher.adaptersT;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.teacher.activityT.QuickActionDetails;
import com.smart.school.teacher.activityT.TopHomeDetails;
import com.smart.school.utils.Constants;


/**
 * Created by Manoj Singh Deopa.
 */

public class HomeTopAdapter extends RecyclerView.Adapter<HomeTopAdapter.ViewHolder> {

    private Context context;
    private String[] list = {
            Constants.MY_ATTENDANCE,
            Constants.MY_TIME_TABLE,
            Constants.PAY_SLIPS,
            Constants.LIBRARY,
            Constants.TRANSPORT,
            Constants.EXAM_DUTY,
            Constants.CREATE_LEAVE,
            Constants.MY_PERFORMANCE,
            Constants.COMPLAINT,
            Constants.MY_PROFILE,
            Constants.CHANGE_PASSWORD
    };

    private int[] list_image = {
            R.drawable.student_list,
            R.drawable.mark_attendance,
            R.drawable.homework_icon,
            R.drawable.home_work_image,
            R.drawable.student_photo,
            R.drawable.student_behaviour1,
            R.drawable.complaint_to_parents,
            R.drawable.exam_management,
            R.drawable.notice_for_parents,
            R.drawable.student_photo,
            R.drawable.student_behaviour1};

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_top_adapter, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.text.setText(list[position]);
        // Picasso.get().load(list_image[position]).into(holder.image);
    }


    @Override
    public int getItemCount() {
        return list.length;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView text;

        public ViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            text = view.findViewById(R.id.text);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String type = list[getAdapterPosition()].trim();
                    context.startActivity(new Intent(context, TopHomeDetails.class).putExtra(Constants.TYPE, type));
                }
            });

        }
    }
}