package com.smart.school.teacher.responsesT;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by manoj on 10/2/19.
 */
public class SubjectListResponse {

    @SerializedName("result")
    @Expose
    public Boolean result;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;


    public static class Datum {

        @SerializedName("subject_id")
        @Expose
        public String subjectId;
        @SerializedName("subject_name")
        @Expose
        public String subjectName;

    }

}
