package com.smart.school.teacher.responsesT;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by manoj on 23/2/19.
 */
public class ViewHwResponse {

    @SerializedName("result")
    @Expose
    public Boolean result;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;


    public class Datum {

        @SerializedName("subject_id")
        @Expose
        public String subjectId;
        @SerializedName("subject_name")
        @Expose
        public String subjectName;
        @SerializedName("description")
        @Expose
        public String description;
        @SerializedName("created")
        @Expose
        public String created;
        @SerializedName("files")
        @Expose
        public List<File> files = null;

    }

    public class File {

        @SerializedName("attach_file")
        @Expose
        public String attachFile;

    }


}
