package com.smart.school.teacher.fragmentsT;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.smart.school.R;
import com.smart.school.teacher.interfacesT.DashboardBottomItemClickListener;


/**
 * Created by Manoj on 2/4/18.
 */

@SuppressLint("ValidFragment")

public class DashBoardFragment extends Fragment implements BottomNavigationView.OnNavigationItemSelectedListener {

    private DashboardBottomItemClickListener dashBoardDashboardBottomItemClickListener;
    private RecyclerView recyclerView;


    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dashboard_fragment_layout, container, false);

        loadFragment(new HomeFragment());

        BottomNavigationView navigation = view.findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(this);

        return view;
    }


    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;

        switch (item.getItemId()) {
            case R.id.navigation_home:
                fragment = new HomeFragment();
                break;

        }

        return loadFragment(fragment);
    }


}

