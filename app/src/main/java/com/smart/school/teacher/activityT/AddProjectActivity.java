package com.smart.school.teacher.activityT;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.smart.school.R;
import com.smart.school.utils.CommonMethod;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class AddProjectActivity extends AppCompatActivity {

    private static final int PLACE_PICKER_REQUEST = 1;
    private static final int SELECT_VIDEO = 1000;
    private static final int REQUEST_VIDEO_CAPTURE = 110;
    private ImageView uploadImg;
    private Uri mCropImageUri;

    private ImageView location, img_video;
    private EditText et_location;
    private Activity context;
    private TextView tv_date;
    private Uri videoUri;
    private TextView btn_post;
    private Bitmap thumbnail_Video = null;
    private boolean isImageSelected = false;
    private String taggedFriends;
    private File VideoFileUri = null;
    private String imageFile = "";
    private String taggedFriendsID;
    private ProgressBar progress;
    private Uri selectedImageUri;
    private TextView tv_start_date, tv_end_date;
    private EditText et_event_name, et_event_description;
    private TextView tv_addTag;
    private String latitude = "";
    private String longitude = "";
    private boolean isButtonclicked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_project);

        context = AddProjectActivity.this;

        setToolbar();

        initializeGoogle();

        initializeViews();

        onClicks();

    }

    private void onClicks() {

        img_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showBottomDialog(context);

            }
        });

        uploadImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage("IMAGE");
            }
        });

        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _callPlacePicker();
            }
        });

        btn_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });
    }

    private void initializeViews() {
        progress = findViewById(R.id.progress);
        tv_start_date = findViewById(R.id.tv_start_date);
        tv_end_date = findViewById(R.id.tv_end_date);

        et_event_name = findViewById(R.id.et_event_name);
        et_event_description = findViewById(R.id.et_event_description);
        et_location = findViewById(R.id.et_area);
        img_video = findViewById(R.id.img_video);
        uploadImg = findViewById(R.id.uploadImg);
        location = findViewById(R.id.location);
        btn_post = findViewById(R.id.btn_post);
    }

    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getIntent().getStringExtra("From"));
    }


    public void showBottomDialog(final Context context) {

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(context);
        bottomSheetDialog.setContentView(R.layout.upload_video_layout);
        bottomSheetDialog.show();

        TextView tvCapture = bottomSheetDialog.findViewById(R.id.tvCapture);
        TextView tvSelect = bottomSheetDialog.findViewById(R.id.tvSelect);


        tvCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
                selectImage("VIDEO");
            }
        });

        tvSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
                _SelectVideofile();
            }
        });

    }

    private void _SelectVideofile() {
        Intent intent = new Intent();
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select a Video "), SELECT_VIDEO);
    }


    private void captureVideofile() {
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);

        }
    }

    private void _callPlacePicker() {


    }


    private void initializeGoogle() {

    }


    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onStop() {

        super.onStop();
    }


    @SuppressLint("NewApi")
    public void selectImage(String video) {

    }



    private String encodeImage(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        return Base64.encodeToString(b, Base64.DEFAULT);
    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void selectDate(View view) {
        if (view == tv_end_date) {
            CommonMethod.showDatePicker(context, tv_end_date);
        } else {
            CommonMethod.showDatePicker(context, tv_start_date);
        }
    }

}
