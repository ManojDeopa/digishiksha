package com.smart.school;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.smart.school.parent.activityP.ChildAccounts;
import com.smart.school.teacher.activityT.LoginActivity;
import com.smart.school.teacher.responsesT.VarifySchoolResponse;
import com.smart.school.utils.AppData;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.ConstantURL;
import com.smart.school.utils.LoginPreferences;
import com.smart.school.utils.ProgressD;
import com.smart.school.utils.SessionListDatabase;
import com.smart.school.utils.VolleySingleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VerifySchoolActivity extends AppCompatActivity {
    Spinner spinner;
    EditText et_school_code;
    TextView tvClick;
    Context context;
    private String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_varify_school);
        getSupportActionBar().hide();

        configureView();


    }

    private void configureView() {
        context = VerifySchoolActivity.this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        TextView app_version = findViewById(R.id.app_version);
        CommonMethod.setVersionName(app_version);
        et_school_code = findViewById(R.id.et_school_code);
        tvClick = findViewById(R.id.tvClick);
        tvClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callinBrowser();
            }
        });


        spinner = findViewById(R.id.spinner);
        List<String> list = new ArrayList<String>();
        list.add("Parent App");
        list.add("School App");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                type = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void verify(View view) {
        if (TextUtils.isEmpty(et_school_code.getText().toString().trim())) {
            et_school_code.requestFocus();
            et_school_code.setError("This Field is required");
        } else {

            if (CommonMethod.isOnline(context)) {
                callApi();
            } else {
                CommonMethod.showNetworkAlert(context);
            }

        }


    }

    private void callApi() {
        final ProgressD pd = ProgressD.show(context, context.getResources().getString(R.string.connecting), true);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConstantURL.VARIFY_SCHOOL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.dismiss();
                        Log.e("onResponse--", response);
                        Gson gson = new Gson();
                        VarifySchoolResponse result = gson.fromJson(String.valueOf(response), VarifySchoolResponse.class);

                        if (result.result) {
                            if (result.data.size() != 0) {

                                if (result.schoolSession.size() != 0) {
                                    AppData.getInstance().setSession(result.schoolSession);
                                    LoginPreferences.getActiveInstance(context).setSessionId(result.schoolSession.get(0).t1Id);
                                    storeData(result.schoolSession);

                                }

                                VarifySchoolResponse.Datum data = result.data.get(0);
                                LoginPreferences.getActiveInstance(context).setSchoolId(data.schoolId);
                                LoginPreferences.getActiveInstance(context).setBranchId(data.branchId);
                                LoginPreferences.getActiveInstance(context).setSchoolName(data.schoolName);
                                LoginPreferences.getActiveInstance(context).setSchoolLogo(data.schoolLogo);
                                LoginPreferences.getActiveInstance(context).setUserBaseUrl(data.appApiUrl);
                                LoginPreferences.getActiveInstance(context).setImgBaseUrl(data.appApiUrl.replace("mobileapp", ""));


                                if (type.equals("Parent App")) {
                                    LoginPreferences.getActiveInstance(context).setBASE_URL(data.appApiUrl + "/parent/app_api.php");
                                    LoginPreferences.getActiveInstance(context).setUserBaseUrl(data.appApiUrl + "/parent/app_api.php");
                                    LoginPreferences.getActiveInstance(context).setImgBaseUrl(data.appApiUrl.replace("mobileapp", ""));

                                    LoginPreferences.getActiveInstance(context).setSessionId(result.schoolSession.get(0).t1Id);
                                    startActivity(new Intent(context, ChildAccounts.class));
                                    finishAffinity();
                                } else {
                                    AppData.getInstance().setResponse(result.data.get(0));
                                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                                    overridePendingTransition(0, 0);
                                    finish();
                                }

                            }

                        } else {
                            CommonMethod.showAlert(result.message, context);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("school_code", et_school_code.getText().toString().trim());
                return params;
            }
        };

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);

    }

    private void storeData(List<VarifySchoolResponse.SchoolSession> schoolSession) {

        SessionListDatabase mDatabase = new SessionListDatabase(context);
        mDatabase.getWritableDatabase().execSQL("delete from " + SessionListDatabase.TABLE_NAME);
        for (int i = 0; i < schoolSession.size(); i++) {
            mDatabase.storeData(
                    schoolSession.size(),
                    schoolSession.get(i).sessionName,
                    schoolSession.get(i).t1Id);
        }


    }


    public void callinBrowser() {
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://digishiksha.in/mobileapp"));
        startActivity(i);
    }


}
