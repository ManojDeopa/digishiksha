package com.smart.school.geteKeeper;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.geteKeeper.fragments.DashBoardFrag;
import com.smart.school.teacher.activityT.LoginActivity;
import com.smart.school.utils.AppUtils;
import com.smart.school.utils.LoginPreferences;
import com.squareup.picasso.Picasso;

public class DashboardGk extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DashboardGk context;
    private Toolbar toolbar;
    private TextView tv_title;
    private DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_gk);

        initilizeViews();

    }


    @SuppressLint("RestrictedApi")
    private void initilizeViews() {
        context = DashboardGk.this;
        toolbar = findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        setSupportActionBar(toolbar);
        findViewById(R.id.logout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showLogoutDialog();
            }
        });

        FloatingActionButton add = findViewById(R.id.add);
        add.setVisibility(View.VISIBLE);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        toggle.getDrawerArrowDrawable().setColor(Color.WHITE);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        TextView user_name = navigationView.getHeaderView(0).findViewById(R.id.user_name);
        ImageView user_image = navigationView.getHeaderView(0).findViewById(R.id.user_image);

        user_name.setText(LoginPreferences.getActiveInstance(context).getFullname());
        String userImage = LoginPreferences.getActiveInstance(context).getImgBaseUrl() + LoginPreferences.getActiveInstance(context).getUserImage();
        Picasso.get().load(userImage).placeholder(R.drawable.shool_logo).into(user_image);

        Log.e("Image---", userImage);

        _callHomeFragment();
    }

    private void showLogoutDialog() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(getString(R.string.sure_to_logout));

        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                LoginPreferences.getActiveInstance(context).logOut();
                Intent intent = new Intent(context, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(intent);
                finishAffinity();
            }
        });

        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alertDialog.show();
    }


    private void _callHomeFragment() {
        tv_title.setText(LoginPreferences.getActiveInstance(context).getFullname());
        DashBoardFrag dashBoardFragment = new DashBoardFrag();
        AppUtils.setFragment(dashBoardFragment, true, DashboardGk.this, R.id.container);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_home) {
            _callHomeFragment();
        } else if (id == R.id.nav_logout) {
            showLogoutDialog();
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
