package com.smart.school.utils;

import android.content.Context;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.smart.school.R;
import com.smart.school.teacher.responsesT.SubjectListResponse;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by manoj on 8/6/19.
 */
public class GetSubjects {

    private Context context;
    private TextView subject_name;

    public GetSubjects(Context context, TextView subject_name) {
        this.context = context;
        this.subject_name = subject_name;
        callSubjectApi();
    }


    private void callSubjectApi() {
        String appBaseUrl = LoginPreferences.getActiveInstance(context).getBASE_URL();
        Log.e("URL---", appBaseUrl);
        final ProgressD pd = ProgressD.show(context, context.getResources().getString(R.string.connecting), true);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, appBaseUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.dismiss();
                        Log.e("Response--", response);
                        Gson gson = new Gson();
                        SubjectListResponse result = gson.fromJson(String.valueOf(response), SubjectListResponse.class);

                        if (result.result) {
                            if (result.data.size() != 0) {
                                CommonMethod.showSubjectDialog(context, result.data, subject_name);
                            }else {
                                CommonMethod.showAlert(result.message,context);
                            }

                        } else {
                            CommonMethod.showAlert(result.message, context);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                        Log.e("error--", error.toString());
                        Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Log.e("Class ID--", AppData.getInstance().getClassId());

                Map<String, String> params = new HashMap<>();
                params.put("api_page", ConstantURL.CLASS_SUBJECT_LIST);
                params.put("class_id", AppData.getInstance().getClassId());
                params.put("school_id", LoginPreferences.getActiveInstance(context).getSchoolId());
                params.put("branch_id", LoginPreferences.getActiveInstance(context).getBranchId());
                params.put("session_id", LoginPreferences.getActiveInstance(context).getSessionId());
                return params;
            }
        };

        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);

    }

}
