package com.smart.school.utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.smart.school.teacher.activityT.DashboardActivity;
import com.smart.school.teacher.activityT.LoginActivity;

import java.util.HashMap;


/**
 * Created by manoj on 15/2/18.
 */

public class SessionManager {
    public static final String KEY_NAME = "KEY_NAME";
    public static final String KEY_SOCIAL_IMG_PATH = "KEY_SOCIAL_IMG_PATH";
    public static final String KEY_MOBILE = "KEY_MOBILE";
    public static final String KEY_IMAGE = "KEY_IMAGE";
    public static final String KEY_EMAIL = "KEY_EMAIL";
    public static final String KEY_PIN_CODE = "KEY_PIN_CODE";
    public static final String KEY_COUNTRY = "KEY_COUNTRY";
    public static final String KEY_LAT = "KEY_LAT";
    public static final String KEY_USER_ID = "KEY_USER_ID";
    public static final String KEY_LONG = "KEY_LONG";
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    private static final String PREF_NAME = "PREF_NAME";
    private static final String IS_LOGIN = "IS_LOGIN";
    private int PRIVATE_MODE = 0;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context _context;


    // Constructor
    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    /**
     * Create User session
     */
    public void createUserSession(String name,
                                  String email,
                                  String pincode,
                                  String country,
                                  String lat,
                                  String longs,
                                  String userid,
                                  String mobile,
                                  String imagePath,
                                  String social_image_path) {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_NAME, name);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_PIN_CODE, pincode);
        editor.putString(KEY_COUNTRY, country);
        editor.putString(KEY_LAT, lat);
        editor.putString(KEY_LONG, longs);
        editor.putString(KEY_USER_ID, userid);
        editor.putString(KEY_MOBILE, mobile);
        editor.putString(KEY_IMAGE, imagePath);
        editor.putString(KEY_SOCIAL_IMG_PATH, social_image_path);
        editor.commit();
    }

    /**
     * Get stored UserSaved data
     */

    public HashMap<String, String> getUserData() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_NAME, pref.getString(KEY_NAME, "Name"));
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, "Email"));
        user.put(KEY_PIN_CODE, pref.getString(KEY_PIN_CODE, "Pincode"));
        user.put(KEY_COUNTRY, pref.getString(KEY_COUNTRY, "Country"));
        user.put(KEY_LAT, pref.getString(KEY_LAT, "Lat"));
        user.put(KEY_LONG, pref.getString(KEY_LONG, "LNG"));
        user.put(KEY_USER_ID, pref.getString(KEY_USER_ID, "UserID"));
        user.put(KEY_MOBILE, pref.getString(KEY_MOBILE, "MobileNo"));
        user.put(KEY_IMAGE, pref.getString(KEY_IMAGE, ""));
        user.put(KEY_SOCIAL_IMG_PATH, pref.getString(KEY_SOCIAL_IMG_PATH, ""));
        return user;
    }


    /**
     * Create login Social session
     */
    public void createSocialSession(String name,
                                    String email,
                                    String image) {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_NAME, name);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_IMAGE, image);
        editor.commit();
    }

    /**
     * Get stored SocialSaved data
     */

    public HashMap<String, String> getSocialSavedDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_NAME, pref.getString(KEY_NAME, ""));
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, ""));
        user.put(KEY_IMAGE, pref.getString(KEY_IMAGE, ""));
        return user;
    }


    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     */
    public void checkLogin() {
        // Check login status
        if (!this.isLoggedIn()) {
            Intent i = new Intent(_context, LoginActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            _context.startActivity(i);
        } else {
            Intent i = new Intent(_context, DashboardActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            _context.startActivity(i);
        }
    }


    /**
     * Clear session details
     **/

    public void logoutUser() {
        CommonMethod.saveDeviceToken(_context, "");
        CommonMethod.saveFBEMAIL(_context, "");
        CommonMethod.saveUserCoverImage(_context, "");
        CommonMethod.saveUserImage(_context, "");
        CommonMethod.saveUserId(_context, "");
        editor.clear();
        editor.commit();
        Intent i = new Intent(_context, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        _context.startActivity(i);
        this.setFirstTimeLaunch(false);

    }

    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }
}