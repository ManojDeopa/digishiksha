package com.smart.school.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Manoj.
 */


public class SessionListDatabase extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "SessionListDatabase";
    public static final String TABLE_NAME = "SessionList";
    private static final int DATABASE_VERSION = 1;
    private static final String COLUMN_ID = "id";
    private static final String SESSION_NAME = "SESSION_NAME";
    private static final String SESSION_ID = "SESSION_ID";



    public SessionListDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(" CREATE TABLE " + TABLE_NAME + " (" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                SESSION_NAME + " TEXT, " +
                SESSION_ID + " TEXT);"
        );

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        String sql = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";
        sqLiteDatabase.execSQL(sql);
        onCreate(sqLiteDatabase);
    }


    public void storeData(int size,
                          String sessionName,
                          String sessionId) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(SESSION_NAME, sessionName);
        contentValues.put(SESSION_ID, sessionId);

        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_NAME, null, contentValues);
        db.close();

    }

    public Cursor getAllData() {
        SQLiteDatabase db = getReadableDatabase();
        return db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
    }

}
