package com.smart.school.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.smart.school.BuildConfig;
import com.smart.school.R;
import com.smart.school.teacher.activityT.ZoomImageActivity;
import com.smart.school.teacher.adaptersT.ClassListAdapter;
import com.smart.school.teacher.adaptersT.SubjectListAdapter;
import com.smart.school.teacher.responsesT.LoginResponse;
import com.smart.school.teacher.responsesT.SubjectListResponse;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by manoj on 15/2/18.
 */

public class CommonMethod {


    public final static long ONE_SECOND = 1000;
    private static Animation animation;

    // Login & signup Validation

    public static boolean isEmailValid(String email) {
        boolean isValid = false;
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }


    public static void setGridRecyclerview(Context context, RecyclerView recyclerView, int count) {
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, count);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

    }

    public static void print(String text) {
        Log.e("--#####--", text);
    }


    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public final static boolean isValidPass(CharSequence target) {

        if (TextUtils.isEmpty(target)) {
            return false;
        } else return target.length() >= 4;
    }

    public final static boolean isValidTelephone(CharSequence target) {

        if (TextUtils.isEmpty(target)) {
            return false;
        } else return target.length() >= 10;

    }

    public static boolean validateName(String name) {
        if (TextUtils.isEmpty(name)) {
            return false;
        } else return name.matches("[A-Z][a-zA-Z]*");
    }


    //***********************************************************************************************


    //Utilities ^^^^^^^^^^
    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static String getTimeStamp(String date_time, String format) {

        DateFormat formatter = new SimpleDateFormat(format);
        formatter.setTimeZone(TimeZone.getDefault());
        Date datee;
        try {
            datee = formatter.parse(date_time);
            Log.e("", "Today is  : " + datee.getTime());
            String timestamp = "" + datee.getTime();
            if (timestamp.length() > 10) {
                timestamp = "" + Long.parseLong(timestamp) / 1000L;
            }
            return timestamp;
        } catch (ParseException pe) {
            pe.printStackTrace();
            return "";
        }
    }

    public static void closeKeyboard(final Context context, final View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /*public static void showAlert(String message, Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(message)
                .setCancelable(false).
                setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        try {
            builder.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/


    public static void setRecyclerView(RecyclerView recyclerView, Context context) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    public static void setHorizontalRecyclerView(RecyclerView recyclerView, Context context) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        //recyclerView.setItemAnimator(new DefaultItemAnimator());
    }


    public static void showEmpty(final LinearLayout empty_layout) {
        empty_layout.setVisibility(View.VISIBLE);
        final TextView tvEmptyMsg = empty_layout.findViewById(R.id.tv_errorText);
        final ImageView iv_errorImage = empty_layout.findViewById(R.id.iv_errorImage);
        tvEmptyMsg.setText("No Data Found");
        iv_errorImage.setImageResource(R.drawable.no_data);
        Animation animation = AnimationUtils.loadAnimation(empty_layout.getContext(), R.anim.bounce);
        iv_errorImage.startAnimation(animation);
        tvEmptyMsg.startAnimation(animation);

        iv_errorImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Animation animation = AnimationUtils.loadAnimation(empty_layout.getContext(), R.anim.bounce);
                iv_errorImage.startAnimation(animation);
                tvEmptyMsg.startAnimation(animation);

            }
        });
    }


//    public static void showBottomDialog(final Context context) {
//
//        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(context);
//        bottomSheetDialog.setContentView(R.layout.detail_dialog_layout);
//        bottomSheetDialog.show();
//        ImageView iv_cross = (ImageView) bottomSheetDialog.findViewById(R.id.iv_cross);
//        iv_cross.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                bottomSheetDialog.dismiss();
//
//            }
//        });
//    }


    public static void showDialog(String title, String msg, final Context context) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

        alertDialog.setTitle(title);
        alertDialog.setMessage(msg);

        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ((Activity) context).finish();
            }
        });

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();

    }


    public static void showAlert(String msg, final Context context) {

       /* if (msg.equals(Constants.NO_DATA_FOUND)) {
            View content_layout = ((Activity) context).getWindow().getDecorView().findViewById(android.R.id.content);
            content_layout.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
            content_layout.setAlpha((float) .3);

            content_layout.setBackgroundResource(R.drawable.login_back);

        } else {
            // Showing Alert Message
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
            alertDialog.setMessage(msg);

            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog.show();
        }*/


        View content_layout = ((Activity) context).getWindow().getDecorView().findViewById(android.R.id.content);
        final Snackbar snackbar = Snackbar.make(content_layout, msg, Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        sbView.setBackgroundColor(ContextCompat.getColor(context, R.color.black));
        snackbar.setAction("Ok", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                snackbar.dismiss();
            }
        });
        snackbar.setActionTextColor(Color.WHITE);
        snackbar.show();


       /* AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setMessage(msg);

        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.show();*/


    }


    public static void showFinishAlert(String msg, final Activity context) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setMessage(msg);
        alertDialog.setCancelable(false);

        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                context.finish();
            }
        });

        // Showing Alert Message
        alertDialog.show();

    }

    public static void showNetworkAlert(final Context context) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setMessage(context.getString(R.string.noInternet));
        alertDialog.setCancelable(false);

        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if (CommonMethod.isOnline(context)) {
                    dialog.dismiss();
                } else {
                    Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                    context.startActivity(intent);
                    dialog.dismiss();
                }


            }
        });

        // Showing Alert Message
        alertDialog.show();

    }


    public static void loading(Activity context) {
        try {
            final View content_layout = context.findViewById(android.R.id.content);
            content_layout.setBackgroundColor(context.getResources().getColor(android.R.color.darker_gray));
            content_layout.setAlpha((float) .3);
//            animation = AnimationUtils.loadAnimation(context, R.anim.fade_in);
//            content_layout.startAnimation(animation);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void loadImage(ImageView imageView, String imagePath) {
        try {
            Picasso.get().load(imagePath)
                    .placeholder(R.drawable.place_holder)
                    .into(imageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void loadResizeImage(ImageView imageView, String imagePath) {
        try {
            Picasso.get().load(imagePath)
                    .placeholder(R.drawable.place_holder)
                    .resize(350, 350)
                    .into(imageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showContent(Activity context) {

        try {
            final View content_layout = context.findViewById(android.R.id.content);
            content_layout.setBackgroundColor(context.getResources().getColor(android.R.color.white));
            content_layout.setAlpha(1);
//            animation = AnimationUtils.loadAnimation(context, R.anim.fade_in);
//            content_layout.clearAnimation();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void _shareIntent(Context context) {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        share.putExtra(Intent.EXTRA_SUBJECT, "DIGI SHIKSHA");
        share.putExtra(Intent.EXTRA_TEXT, "https://www.digishiksha.in/");
        context.startActivity(Intent.createChooser(share, "Share using.."));

        //        Intent launchIntent = context.getPackageManager().getLaunchIntentForPackage("example.socialmedia.integration");
//        if (launchIntent != null) {
//            context.startActivity(launchIntent);
//            //null pointer check in case package name was not found
//        }

    }

    public static float dpToPx(Context context, float valueInDp) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, metrics);
    }

    public static void ZoomImage(Context context, String image) {
        context.startActivity(new Intent(context, ZoomImageActivity.class).putExtra("IMAGE", image));
    }

    /******
     *    saveUserId
     * ***/

    public static void saveUserId(Context context, String value) {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("USERIDX", value);
        editor.apply();

    }

    public static String getUserId(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        return sharedPreferences.getString("USERIDX", "");
    }

    /******
     *    saveUserCoverImage
     * ***/

    public static void saveUserCoverImage(Context context, String value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("USERCOVERIMAGEX", value);
        editor.apply();
    }

    public static String getUserUserCoverImage(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        return sharedPreferences.getString("USERCOVERIMAGEX", "");
    }

    /******
     *    saveDeviceToken
     * ***/

    public static void saveDeviceToken(Context context, String value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("DEVICETOKEN", value);
        editor.apply();
    }

    public static String getDeviceToken(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        return sharedPreferences.getString("DEVICETOKEN", "");
    }

    /******
     *    saveUserImage
     * ***/

    public static void saveUserImage(Context context, String value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("USERIMAGEX", value);
        editor.apply();
    }

    public static String getUserUserImage(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        return sharedPreferences.getString("USERIMAGEX", "");
    }

    /******
     * Save Student id in saveFBEMAIL
     * ***/

    public static void saveFBEMAIL(Context context, String value) {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("FBEMAIL", value);
        editor.apply();

    }

    public static String getFBEMAIL(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);

        return sharedPreferences.getString("FBEMAIL", "");
    }

    /**
     * Called for checking Internet connection
     */
    public static boolean isOnline(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo;
        try {
            netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null && netInfo.isConnectedOrConnecting()) {
                return true;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return false;
    }

    /**
     * Called for check the runtime permission -
     */


    public static void checkPermission(Activity context, String permission) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (isPermissionChecked(context, permission)) {
                Log.e("Status :--", "Permission checked..");
            } else {
                Log.e("Status :--", "Requesting Permission..");
                requestPermission(context, permission);
            }
        }

    }

    protected static boolean isPermissionChecked(Activity context, String permission) {
        int result = ContextCompat.checkSelfPermission(context, permission);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    protected static void requestPermission(Activity context, String permission) {

        if (ActivityCompat.shouldShowRequestPermissionRationale(context, permission)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                context.requestPermissions(new String[]{permission}, 100);
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                context.requestPermissions(new String[]{permission}, 100);
            }
        }
    }

    /**
     * Called for get String from ArrayList
     */


    public static String getStringFromList(ArrayList<String> mArrayList) {
        String tempString = mArrayList.toString().replaceAll("\\[|\\]", "").replaceAll(" ", "").replace(",,", ",");
        String res = "";
        if (!TextUtils.isEmpty(tempString)) {
            if (tempString.charAt(tempString.length() - 1) == ',') {
                tempString = tempString.substring(0, tempString.length() - 1);
            }
            return tempString;
        }
        return "";
    }

    public static void setFabButton(final FloatingActionButton fab, RecyclerView view) {
        view.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE && recyclerView.canScrollVertically(1)) {
                    fab.show();
                }
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0 || dy < 0 && fab.isShown()) {
                    fab.hide();
                }
            }
        });
    }

    public static void TimePickerDialog(Context context, final TextView tvTimer) {
        /*Code for show  Timer picker dialog */
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                tvTimer.setText(selectedHour + ":" + selectedMinute);
            }
        }, hour, minute, false);//Yes for 24 hour time no for 12 hour
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();

    }

    public static void showDatePicker(Context context, final TextView textView) {

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context, R.style.DatePickerDialogThemes, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                textView.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
            }
        }, mYear, mMonth, mDay);
        datePickerDialog.show();
        /*datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000)*/
        ;
    }

    public static void showTimePicker(Context context, final TextView tvTimer) {
        /*Code for show  Timer picker dialog */
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(context, R.style.DatePickerDialogThemes, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                tvTimer.setText(selectedHour + ":" + selectedMinute);
            }
        }, hour, minute, false);//Yes for 24 hour time no for 12 hour
        mTimePicker.show();

    }


    public static void spinnerText(AdapterView<?> parent) {
        ((TextView) parent.getChildAt(0)).setTextColor(Color.DKGRAY);
        ((TextView) parent.getChildAt(0)).setTextSize(14);
    }

    public static String convertByteToString(byte[] bytes) {
        return resizeBase64Image(Base64.encodeToString(bytes, Base64.DEFAULT));
    }

    public static String resizeBase64Image(String base64image) {
        byte[] encodeByte = Base64.decode(base64image.getBytes(), Base64.DEFAULT);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPurgeable = true;
        Bitmap image = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length, options);


        if (image.getHeight() < 400 && image.getWidth() < 300) {
            return base64image;
        }

        if (image.getHeight() >= 800 || image.getWidth() >= 700) {
            Log.e("Height--", "bigger than 800");
            image = Bitmap.createScaledBitmap(image, 700, 800, false);
        } else {
            image = Bitmap.createScaledBitmap(image, image.getWidth(), image.getHeight(), false);
            Log.e("Height--", "Smaller than 800");
        }

        Log.e("Height--", String.valueOf(image.getHeight()));
        Log.e("Width--", String.valueOf(image.getWidth()));

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 100, baos);

        byte[] b = baos.toByteArray();
        System.gc();
        return Base64.encodeToString(b, Base64.NO_WRAP);
    }

    public static void setBase64Image(ImageView imageView, String base64String) {
        if (!base64String.isEmpty()) {
            byte[] bytes = Base64.decode(base64String, Base64.DEFAULT);
            Bitmap myBitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
            Bitmap resized = Bitmap.createScaledBitmap(myBitmap, 200, 200, true);
            imageView.setImageBitmap(resized);
        }
    }


    public static String convertBitmaptoString(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();

        return CommonMethod.resizeBase64Image(Base64.encodeToString(b, Base64.DEFAULT));
    }


    public static void zoomImage(final Context context, String url) {
        final Dialog dialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.zoom_image_dialog);
        final ImageView imageView = dialog.findViewById(R.id.imageView);
        Picasso.get().load(url).placeholder(R.drawable.place_holder).into(imageView);
        dialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public static void callBrowserIntent(Context context, String url) {

        try {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            context.startActivity(browserIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @SuppressLint("MissingPermission")
    public static void callPhoneIntent(Context context, String phone) {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
        context.startActivity(intent);
    }

    public static List<LoginResponse.ClassVal> getClassListForSchool(Context context) {
        ClassListDatabase mDatabase = new ClassListDatabase(context);
        ArrayList<LoginResponse.ClassVal> classlist = new ArrayList<>();
        Cursor cursor = mDatabase.getAllData();
        if (cursor.moveToFirst()) {
            do {
                classlist.add(new LoginResponse.ClassVal(
                        cursor.getString(1),
                        cursor.getString(2)));
            } while (cursor.moveToNext());
        }
        return classlist;
    }


    public static void showClassDialog(Context context, TextView class_section) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.list_dialog_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;


        RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view);
        CommonMethod.setRecyclerView(recyclerView, context);


        TextView btn_submit = dialog.findViewById(R.id.btn_submit);
        TextView btn_cancel = dialog.findViewById(R.id.btn_cancel);

        TextView title = dialog.findViewById(R.id.title);
        title.setText("Select Class - Section");

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        recyclerView.setAdapter(new ClassListAdapter(dialog, getClassListForSchool(context), class_section));


        dialog.show();

        /* FOR SETTING CUSTOM HIGHT AND WIDTH */
        /*WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(dialog.getWindow().getAttributes());
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes(layoutParams);*/

    }


    public static void showSubjectDialog(Context context, List<SubjectListResponse.Datum> subjectList, TextView subject_name) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.list_dialog_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view);
        CommonMethod.setRecyclerView(recyclerView, context);

        recyclerView.setAdapter(new SubjectListAdapter(dialog, subjectList, subject_name));

        TextView btn_submit = dialog.findViewById(R.id.btn_submit);
        TextView btn_cancel = dialog.findViewById(R.id.btn_cancel);

        TextView title = dialog.findViewById(R.id.title);
        title.setText("Select Subject");

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        dialog.show();
        /* FOR SETTING CUSTOM HIGHT AND WIDTH */
        /*WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(dialog.getWindow().getAttributes());
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes(layoutParams);*/

    }


    @SuppressLint("SetTextI18n")
    public static void setVersionName(TextView app_version) {
        app_version.setText("© Deltegic Solutions (P) Ltd.  |  APP Ver " + BuildConfig.VERSION_NAME);
    }

    public static void startIntent(Activity context, Class<?> cls) {
        Intent intent = new Intent(context, cls);
        context.overridePendingTransition(0, 0);
        context.startActivity(intent);
    }

    public static Map<String, String> MasterAdminParams(Context context) {
        Map<String, String> params = new HashMap<>();
        params.put("emp_id", LoginPreferences.getActiveInstance(context).getId());
        params.put("school_id", LoginPreferences.getActiveInstance(context).getSchoolId());
        params.put("branch_id", LoginPreferences.getActiveInstance(context).getBranchId());
        params.put("session_id", LoginPreferences.getActiveInstance(context).getSessionId());
        return params;
    }
}