package com.smart.school.utils;

import com.smart.school.parent.responsesP.AttendanceResponse;
import com.smart.school.parent.responsesP.DashboardTopResponse;
import com.smart.school.parent.responsesP.LoginResponseP;
import com.smart.school.parent.responsesP.SchoolFeeResponse;
import com.smart.school.teacher.responsesT.SectionResponse;
import com.smart.school.teacher.responsesT.StudentListResponse;
import com.smart.school.teacher.responsesT.VarifySchoolResponse;
import com.smart.school.teacher.responsesT.ViewHwResponse;

import java.util.List;
import java.util.Map;

/**
 * Created by manoj on 1/12/18.
 */
public class AppData {
    public static final AppData ourInstance = new AppData();
    private VarifySchoolResponse.Datum response;
    private List<VarifySchoolResponse.SchoolSession> session = null;
    private String classId;
    private String sectionId;
    private List<SectionResponse.Datum> sectionList = null;
    private String orderByNo;
    private List<StudentListResponse.Datum> studentList;
    private List<LoginResponseP.Datum> childDataP;
    private SchoolFeeResponse feeResponse;
    private List<DashboardTopResponse.Datum> dashboardTopResponse = null;
    private String from = "From";
    private String studentId;
    private int position;
    private String subjectId;
    private List<ViewHwResponse.Datum> hwDetail;
    private AttendanceResponse attendanceData;
    private StudentListResponse.Datum studentData;
    private String type = Constants.EMPTY;
    private Map<String, String> feeParams;
    private String version;

    public AppData() {
    }

    public static AppData getInstance() {
        return ourInstance;
    }

    public VarifySchoolResponse.Datum getResponse() {
        return response;
    }

    public void setResponse(VarifySchoolResponse.Datum response) {
        this.response = response;
    }

    public List<VarifySchoolResponse.SchoolSession> getSession() {
        return session;
    }

    public void setSession(List<VarifySchoolResponse.SchoolSession> session) {
        this.session = session;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    public List<SectionResponse.Datum> getSectionList() {
        return sectionList;
    }

    public void setSectionList(List<SectionResponse.Datum> sectionList) {
        this.sectionList = sectionList;
    }

    public String getOrderByNo() {
        return orderByNo;
    }

    public void setOrderByNo(String orderByNo) {
        this.orderByNo = orderByNo;
    }

    public List<StudentListResponse.Datum> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<StudentListResponse.Datum> studentList) {
        this.studentList = studentList;
    }

    public List<LoginResponseP.Datum> getChildDataP() {
        return childDataP;
    }

    public void setChildDataP(List<LoginResponseP.Datum> studentDataP) {
        this.childDataP = studentDataP;
    }

    public SchoolFeeResponse getFeeResponse() {
        return feeResponse;
    }

    public void setFeeResponse(SchoolFeeResponse feeResponse) {
        this.feeResponse = feeResponse;
    }

    public List<DashboardTopResponse.Datum> getDashboardTopResponse() {
        return dashboardTopResponse;
    }

    public void setDashboardTopResponse(List<DashboardTopResponse.Datum> dashboardTopResponse) {
        this.dashboardTopResponse = dashboardTopResponse;
    }

    public void setfrom(String from) {
        this.from = from;
    }

    public String getFrom() {
        return from;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public List<ViewHwResponse.Datum> getHwDetail() {
        return hwDetail;
    }

    public void setHwDetail(List<ViewHwResponse.Datum> hwDetail) {
        this.hwDetail = hwDetail;
    }

    public AttendanceResponse getAttendanceData() {
        return attendanceData;
    }

    public void setAttendanceData(AttendanceResponse attendanceData) {
        this.attendanceData = attendanceData;
    }

    public StudentListResponse.Datum getStudentData() {
        return studentData;
    }

    public void setStudentData(StudentListResponse.Datum studentData) {
        this.studentData = studentData;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Map<String, String> getFeeParams() {
        return feeParams;
    }

    public void setFeeParams(Map<String, String> feeParams) {
        this.feeParams = feeParams;
    }


}
