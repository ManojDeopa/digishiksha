package com.smart.school.utils;

/**
 * Created by manoj on 31/5/18.
 */
public class Constants {


    /*Common Tags*/

    public static final String SERVER_ERROR = "Sorry for Trouble ! We are Working on this...";
    public static final String TYPE_TEACHER = "Teacher";
    public static final String TYPE_PARENT = "Parent";
    public static final String TYPE_GATEKEEPER = "Gate Keeper";
    public static final String TYPE_MASTER_ADMIN = "Master Admin";

    public static final String NO_DATA_FOUND = "No Data Found";


    /* Teacher Modules Values */


    public static final String LOG_OUT = "Logout";
    public static final String SHARE = "Share";
    public static final String PRIVACY_POLICY = "Privacy Policy";
    public static final String ABOUT = "About Digi Shiksha";
    public static final String APP_TUTORIAL = "App Tutorial";
    public static final String PRIVACY_POLICY_URL = "https://www.digishiksha.in/privacypolicies/";
    public static final String DIGI_SHIKSHA_URL = "https://play.google.com/store/apps/details?id=com.smart.school";
    public static final String DIGI_SHIKSHA_URL_WEB = "https://www.digishiksha.in/";


    public static final String HOME = "Home";
    public static final String TYPE = "TYPE";
    public static final String TYPE_ADD_STUDENT = "Add Student";
    public static final String TYPE_EDIT_STUDENT = "Edit Student";
    public static final String TYPE_STUDENT_LIST = "Student List";
    public static final String TYPE_MARK_ATTENDENCE = "Mark Attendance";
    public static final String TYPE_WRITE_HOMEWORK = "Write Homework";
    public static final String TYPE_UPLOAD_HOMEWORK = "Upload Homework";
    public static final String TYPE_LEAVE_APPROVALS = "Leave Approvals";
    public static final String TYPE_FEES_COLLECTION = "Fees Collection";
    public static final String STUDENT_FEE_DEFAULTER = "Student Fees Defaulter";


    public static final String ASSIGNMENT = "Assignment";
    public static final String UPLOAD_STUDENT_PHOTO = "Upload Student Photo";
    public static final String STUDENT_FEEDBACK = "Student Feedback";
    public static final String COMPLAINT_TO_PARENT = "Complaint to Parents";
    public static final String EXAM_MARKS = "Exam Marks";
    public static final String NOTICE_FOR_PARENTS = "Notice for Parents";


    public static final String MY_ATTENDANCE = "My Attendance";
    public static final String MY_TIME_TABLE = "Time Table";
    public static final String PAY_SLIPS = "Pay Slips";
    public static final String LIBRARY = "Library";
    public static final String TRANSPORT = "Transport";
    public static final String EXAM_DUTY = "Exam Duty";
    public static final String CREATE_LEAVE = "Create Leave";
    public static final String MY_PERFORMANCE = "My Performance";
    public static final String COMPLAINT = "Complaint";
    public static final String MY_PROFILE = "My Profile";
    public static final String CHANGE_PASSWORD = "Change Password";

    public static final String TYPE_TEACHER_GUIDANCE = "Teacher Guidance";
    public static final String TYPE_PAY_SLIP_TC = "PaySlip Terms & Conditions";
    public static final String TYPE_SEND_NOTIFICATION = "Send Notification";
    public static final String TYPE_SEND_NOTIFICATION2 = " Send Notification ";


    public static final String VIEW_HW_DETAIL = "Homework Detail";






    /* Parent Modules Values */

    public static final String TYPE_ATTENDANCE = "Attendance";
    public static final String TYPE_TIME_TABLE = "Time Table";
    public static final String TYPE_NOTICE = "Notice";
    public static final String TYPE_FEES = "Fees";
    public static final String TYPE_EXAM_TIMETABLE = "Exam Timetable";
    public static final String TYPE_HOMEWORK = "Homework";
    public static final String TYPE_EXAM_RESULT = "Exam Result";
    public static final String TYPE_LEAVE_REQUEST = "Leave Request";
    public static final String TYPE_CLASSTEST_RESULT = "ClassTest Result";
    public static final String TYPE_LIBRARY = "Library";
    public static final String TYPE_BUS_ROUTE = "Bus Route";
    public static final String TYPE_MY_FEEDBACK = "My Feedback";
    public static final String TYPE_ASSIGNMENT = "Assignment";
    public static final String TYPE_WEBSITE = "Website";


    public static final String TYPE_CALLENDER = "Calender";
    public static final String TYPE_ANNOUNCEMENT = "Announcement";
    public static final String TYPE_GALLERY = "Gallery";
    public static final String TYPE_STYDY_MATERIAL = "Study Material";
    public static final String TYPE_STUDY_MATERIAL_GRID = "Album";
    public static final String TYPE_MY_TEACHER = "My Teacher";
    public static final String TYPE_MY_CLASSMATES = "My Classmates";
    public static final String TYPE_MY_ACHIEVEMENTS = "My Achievements";
    public static final String TYPE_SERVICE_REQUEST = "Service Request";
    public static final String TYPE_COMPLAINTS = "Complaints";
    public static final String TYPE_SUB_GALLERY = "Albums";
    public static final String TYPE_ALL_GALLERY = "All";

    public static final String TYPE_ABOUT_SCHOOL = "About School";
    public static final String TYPE_SCHOOL_RULE_REGULATIONS = "School Rule Regulation";
    public static final String TYPE_STUDENT_GUIDANCE = "Student Guidance";
    public static final String TYPE_PARENT_GUIDANCE = "Parent Guidance";
    public static final String TYPE_SCHOOL_STAFF = "School Staff";
    public static final String TYPE_CONTACT_US = "Contact Us";


    public static final String EMPTY = "EMPTY";
    public static final String ACCOUNT_BALANCE = "A/C Balance";
    public static final String FEE_DISCOUNT_SUMMARY = "Fee Discount Summary";
    public static final String DAY_BOOK = "Day Book";
    public static final String STUDENT_PAID_SUMMARY = "Student Paid Summary";
    public static final String EXPENSE_SUMMARY = "Expanse Summary";
    public static final String GUARDIAN_WISE_FEE_DEFAULTER = "Guardian wise Fee Defaulter";

    public static final String ACTIVITY = "Activity";
    public static final String FEED = "Feed";
    public static final String NOTIFICATION = "Notification";
    public static final String UPLOAD_ACTIVITY = "Upload Activity";
    public static final String UPLOAD_FEED = "Upload Feed";
    public static final String UPLOAD_ASSIGNMENT = "Upload Assignment";
    public static final String UPLOAD_NOTICE = "Upload Notice";
    public static final String DETAIL = "Detail";


    public static final String QUICK_ACTION = "Quick Actions";
    public static final String COMMUNICATION = "Communication";
    public static final String ACC_FEE_COLLECTION = "Account & Fees Collection";
    public static final String IMPORTANT_LINK = "Important Links";
    public static final String PAY_MODE = "master_api";
}

