package com.smart.school.utils;

/**
 * Created by manoj on 2/12/18.
 */
public class ConstantURL {

    public static final String IMAGE_BASE_URL = "https://digishiksha.in/myapp";

    public static final String BASE_URL = "https://digishiksha.in/myapp/mobileapp/parent/app_api.php";
    public static final String VARIFY_SCHOOL = "https://digishiksha.in/mobileapp/app_api.php";
    public static final String CHECK_VERSION_URL = "https://digishiksha.in/mobileapp/appversion.php";

    public static final String APP_API = "/app_api.php";
    public static final String PLAY_STORE_APP_URL = "https://play.google.com/store/apps/details?id=com.smart.school";

    public static final String ATTENDANCE = "my_attendance";
    public static final String TIME_TABLE = "my_timetable";
    public static final String NOTICE = "my_notice";
    public static final String SCHOOL_FEES = "my_school_fees";
    public static final String SUBJECT_LIST = "subject_list";
    public static final String HOME_WORK = "my_homework";
    public static final String EXAM_RESULT = "exam_result";
    public static final String VIEW_LEAVE = "leave_view";
    public static final String LIBRARY = "library";


    public static final String BUS_ROUTE = "bus_route";
    public static final String CALENDER = "calender";
    public static final String ANNOUNCEMENT = "announcement";
    public static final String GALLERY = "gallery";
    public static final String STUDY_MATERIAL = "study_meterial";
    public static final String CLASS_MATE = "my_classmate";
    public static final String TEACHERS = "my_teacher";
    public static final String MY_ACHIEVEMENTS = "my_achievement";
    public static final String VIEW_COMPLAINTS = "complaint_view";
    public static final String STUDENT_PROFILE = "student_profile";
    public static final String ABOUT_SCHOOL = "web_page";
    public static final String SCHOOL_LOGIN = "teacher_login";
    public static final String CLASS_SUBJECT_LIST = "class_subject_list";
    public static final String HOMEWORK_CREATE = "homework_create";
    public static final String HOMEWORK_VIEW = "homework_view";
    public static final String MARK_ATTANDENCE = "mark_attendance";
    public static final String STUDENT_PHOTO_UPLOAD = "student_photo_upload";
    public static final String ADD_STUDENT = "add_student";
    public static final String EDIT_STUDENT = "edit_student";
    public static final String SEND_SMS = "send_sms";
    public static final String DELETE_STUDENT = "delete_student";
    public static final String SMS_RETURN_DATA = "sms_return_data";
    public static final String SMS_OUT_GOING = "sms_outgoing";
    public static final String ASSIGNMENT = "my_assignment";
    public static final String UPLOAD_NOTICE = "upload_notice";
    public static final String UPLOAD_ASSIGNMENT = "upload_assignment";
    public static final String ACCOUNT_FEES = "account_fee_web_page";
}
