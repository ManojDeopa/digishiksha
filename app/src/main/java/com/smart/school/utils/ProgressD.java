package com.smart.school.utils;

/**
 * Created by manoj on 9/12/18.
 */


import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.smart.school.R;


public class ProgressD extends Dialog {
    public ProgressD(Context context) {
        super(context);
    }

    public ProgressD(Context context, int theme) {
        super(context, theme);
    }

    public static ProgressD show(Context context, CharSequence message, boolean cancelable) {
        ProgressD dialog = new ProgressD(context, R.style.ProgressD);
        dialog.setTitle("");
        dialog.setContentView(R.layout.progress_hud_bg);
        if (message == null || message.length() == 0) {
            dialog.findViewById(R.id.message).setVisibility(View.GONE);
        } else {
            TextView txt = (TextView) dialog.findViewById(R.id.message);
            txt.setText(message);
        }
        dialog.setCancelable(false);
        /*  dialog.setOnCancelListener(cancelListener);*/
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 0.2f;
        dialog.getWindow().setAttributes(lp);
        //dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        dialog.show();
        return dialog;
    }

}

