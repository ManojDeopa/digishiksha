package com.smart.school.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Manoj.
 */


public class ClassListDatabase extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "ClassListDatabase";
    public static final String TABLE_NAME = "ClassList";
    private static final int DATABASE_VERSION = 1;
    private static final String COLUMN_ID = "id";
    private static final String CLASS_NAME = "CLASS_NAME";
    private static final String CLASS_ID = "CLASS_ID";



    public ClassListDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL(" CREATE TABLE " + TABLE_NAME + " (" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                CLASS_NAME + " TEXT, " +
                CLASS_ID + " TEXT);"
        );

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        String sql = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";
        sqLiteDatabase.execSQL(sql);
        onCreate(sqLiteDatabase);
    }


    public void storeData(int size,
                          String sessionName,
                          String sessionId) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(CLASS_NAME, sessionName);
        contentValues.put(CLASS_ID, sessionId);

        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_NAME, null, contentValues);
        db.close();

    }

    public Cursor getAllData() {
        SQLiteDatabase db = getReadableDatabase();
        return db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
    }

}
