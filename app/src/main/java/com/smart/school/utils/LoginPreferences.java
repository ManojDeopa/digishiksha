package com.smart.school.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by manoj on 8/12/18.
 */
public class LoginPreferences {
    private static LoginPreferences preferences = null;
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor editor;
    private Context context;
    private String id = "id";
    private String fullname = "fullname";
    private String type = "type";
    private String user_email = "user_email";


    private String mobile = "mobile";
    private String userImage = "userImage";
    private String admNo = "admNo";
    private String classSce = "classSce";


    private String schoolId = "schoolId";
    private String branchId = "branchId";
    private String schoolName = "schoolName";
    private String schoolLogo = "schoolLogo";
    private String userBaseUrl = "userBaseUrl";
    private String imgBaseUrl = "imgBaseUrl";


    private String classId = "classId";
    private String sectionId = "sectionId";

    private String isLoggedIn = "isLoggedIn";
    private String isRegistered = "isRegistered";

    private String BASE_URL = "BASE_URL";
    private String sessionId = "sessionId";

    public LoginPreferences(Context context) {
        this.context = context;
        setmPreferences(PreferenceManager.getDefaultSharedPreferences(context));
    }

    public static LoginPreferences getActiveInstance(Context context) {
        if (preferences == null) {
            preferences = new LoginPreferences(context);
        }
        return preferences;
    }

    public SharedPreferences getmPreferences() {
        return mPreferences;
    }

    public void setmPreferences(SharedPreferences mPreferences) {
        this.mPreferences = mPreferences;
    }

    public String getId() {
        return mPreferences.getString(this.id, "");
    }

    public void setId(String id) {
        editor = mPreferences.edit();
        editor.putString(this.id, id);
        editor.commit();
    }


    public String getClassId() {
        return mPreferences.getString(this.classId, "");
    }

    public void setClassId(String classId) {
        editor = mPreferences.edit();
        editor.putString(this.classId, classId);
        editor.commit();
    }


    public String getSectionId() {
        return mPreferences.getString(this.sectionId, "");
    }

    public void setSectionId(String sectionId) {
        editor = mPreferences.edit();
        editor.putString(this.sectionId, sectionId);
        editor.commit();
    }


    public String getType() {
        return mPreferences.getString(this.type, "");
    }

    public void setType(String type) {
        editor = mPreferences.edit();
        editor.putString(this.type, type);
        editor.commit();
    }


    public String getFullname() {
        return mPreferences.getString(this.fullname, "");
    }

    public void setFullname(String fullname) {
        editor = mPreferences.edit();
        editor.putString(this.fullname, fullname);
        editor.commit();
    }

    public String getUser_email() {
        return mPreferences.getString(this.user_email, "");
    }

    public void setUser_email(String user_email) {
        editor = mPreferences.edit();
        editor.putString(this.user_email, user_email);
        editor.commit();
    }


    public boolean getIsLoggedIn() {
        return mPreferences.getBoolean(this.isLoggedIn, false);
    }

    public void setIsLoggedIn(boolean isLoggedin) {
        editor = mPreferences.edit();
        editor.putBoolean(this.isLoggedIn, isLoggedin);
        editor.commit();
    }

    public boolean getIsRegistered() {
        return mPreferences.getBoolean(this.isRegistered, false);
    }

    public void setIsRegistered(boolean isRegistered) {
        editor = mPreferences.edit();
        editor.putBoolean(this.isRegistered, isRegistered);
        editor.commit();
    }

    public String getSchoolId() {
        return mPreferences.getString(this.schoolId, "");
    }

    public void setSchoolId(String schoolId) {
        editor = mPreferences.edit();
        editor.putString(this.schoolId, schoolId);
        editor.commit();

    }

    public String getUserBaseUrl() {
        return mPreferences.getString(this.userBaseUrl, "");
    }

    public void setUserBaseUrl(String userBaseUrl) {
        editor = mPreferences.edit();
        editor.putString(this.userBaseUrl, userBaseUrl);
        editor.commit();
    }


    public String getImgBaseUrl() {
        return mPreferences.getString(this.imgBaseUrl, "");
    }

    public void setImgBaseUrl(String imgBaseUrl) {
        editor = mPreferences.edit();
        editor.putString(this.imgBaseUrl, imgBaseUrl);
        editor.commit();
    }

    public String getMobile() {
        return mPreferences.getString(this.mobile, "");
    }

    public void setMobile(String mobile) {
        editor = mPreferences.edit();
        editor.putString(this.mobile, mobile);
        editor.commit();
    }

    public String getUserImage() {
        return mPreferences.getString(this.userImage, "");
    }

    public void setUserImage(String userImage) {
        editor = mPreferences.edit();
        editor.putString(this.userImage, userImage);
        editor.commit();
    }

    public String getAdmissionNo() {
        return mPreferences.getString(this.admNo, "");
    }

    public void setAdmissionNo(String admNo) {
        editor = mPreferences.edit();
        editor.putString(this.admNo, admNo);
        editor.commit();
    }

    public String getClassSection() {
        return mPreferences.getString(this.classSce, "");
    }

    public void setClassSection(String classSce) {
        editor = mPreferences.edit();
        editor.putString(this.classSce, classSce);
        editor.commit();
    }

    public String getBranchId() {
        return mPreferences.getString(this.branchId, "");
    }

    public void setBranchId(String branchId) {
        editor = mPreferences.edit();
        editor.putString(this.branchId, branchId);
        editor.commit();
    }

    public String getSchoolName() {
        return mPreferences.getString(this.schoolName, "");
    }

    public void setSchoolName(String schoolName) {
        editor = mPreferences.edit();
        editor.putString(this.schoolName, schoolName);
        editor.commit();
    }

    public String getSchoolLogo() {
        return mPreferences.getString(this.schoolLogo, "");
    }

    public void setSchoolLogo(String schoolLogo) {
        editor = mPreferences.edit();
        editor.putString(this.schoolLogo, schoolLogo);
        editor.commit();
    }

    public String getBASE_URL() {
        return mPreferences.getString(this.BASE_URL, "");
    }

    public void setBASE_URL(String BASE_URL) {
        editor = mPreferences.edit();
        editor.putString(this.BASE_URL, BASE_URL);
        editor.commit();
    }

    public String getSessionId() {
        return mPreferences.getString(this.sessionId, "");
    }

    public void setSessionId(String sessionId) {
        editor = mPreferences.edit();
        editor.putString(this.sessionId, sessionId);
        editor.commit();
    }

    public void logOut() {
        LoginPreferences.getActiveInstance(context).setId("");
        LoginPreferences.getActiveInstance(context).setUser_email("");
        LoginPreferences.getActiveInstance(context).setFullname("");
        LoginPreferences.getActiveInstance(context).setUserImage("");
        LoginPreferences.getActiveInstance(context).setIsLoggedIn(false);
    }


}