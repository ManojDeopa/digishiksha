package com.smart.school;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by manoj on 24/3/19.
 */
public class AppVersionResponse {

    @SerializedName("result")
    @Expose
    public Boolean result;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public Data data;

    public class Data {

        @SerializedName("version_name")
        @Expose
        public String versionName;
        @SerializedName("version_code")
        @Expose
        public Integer versionCode;
        @SerializedName("text")
        @Expose
        public String text;


    }

}
