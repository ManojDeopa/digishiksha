package com.smart.school.masterAdmin.response;

import android.net.Uri;

/**
 * Created by manoj on 8/6/19.
 */
public class UploadFileModel {
    private final Uri uri;
    private final String type;
    private final byte[] fileData;
    private String fileName;


    public UploadFileModel(String type, Uri uri, byte[] fileData) {
        this.type = type;
        this.uri = uri;
        this.fileData = fileData;


    }

    public Uri getUri() {
        return uri;
    }

    public String getType() {
        return type;
    }

    public byte[] getFileData() {
        return fileData;
    }

    public String getFileName() {
        return fileName;
    }
}
