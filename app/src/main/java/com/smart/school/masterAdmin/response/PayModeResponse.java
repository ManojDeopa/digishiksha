package com.smart.school.masterAdmin.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by manoj on 8/6/19.
 */
public class PayModeResponse {

    @SerializedName("result")
    @Expose
    public Boolean result;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;

    public class Datum {

        @SerializedName("paymode_id")
        @Expose
        public String paymodeId;
        @SerializedName("paymode")
        @Expose
        public String paymode;

    }

}
