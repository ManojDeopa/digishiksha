package com.smart.school.masterAdmin.adaptersMA;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.masterAdmin.response.SmsReturnData;

import java.util.List;

import static com.smart.school.masterAdmin.fragmentsMA.SendNotification.middle_text;


/**
 * Created by Manoj Singh Deopa.
 */

public class TemplateAdapter extends RecyclerView.Adapter<TemplateAdapter.ViewHolder> {

    private AlertDialog alertdialog;
    private List<SmsReturnData.TextTemplate> list;
    private Context context;


    public TemplateAdapter(List<SmsReturnData.TextTemplate> textTemplates, AlertDialog alertDialog) {
        this.list = textTemplates;
        this.alertdialog = alertDialog;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_footer_adapter, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int i) {
        holder.header.setText(list.get(i).textTemplateTitle);
        holder.footer.setText(list.get(i).textTemplate);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView header, footer;

        public ViewHolder(View view) {
            super(view);
            header = view.findViewById(R.id.header);
            footer = view.findViewById(R.id.footer);
            itemView.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onClick(View view) {
                    middle_text.setText(list.get(getAdapterPosition()).textTemplate);
                    alertdialog.dismiss();
                }
            });

        }
    }
}