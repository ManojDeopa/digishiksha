package com.smart.school.masterAdmin.fragmentsMA;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.smart.school.BuildConfig;
import com.smart.school.R;
import com.smart.school.masterAdmin.adaptersMA.UploadFilesAdapter;
import com.smart.school.masterAdmin.response.UploadFileModel;
import com.smart.school.teacher.responsesT.CommonResponse;
import com.smart.school.utils.AppData;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.ConstantURL;
import com.smart.school.utils.LoginPreferences;
import com.smart.school.utils.ProgressD;
import com.smart.school.utils.VolleyMultipartRequest;
import com.smart.school.utils.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;


/**
 * Created by Manoj Singh Deopa on 2/4/18.
 */

@SuppressLint("ValidFragment")

public class UploadFeed extends Fragment implements View.OnClickListener {

    EditText et_text;


    ArrayList<UploadFileModel> fileList = new ArrayList<>();

    RecyclerView recycler_view;
    ImageView gallery_action, video_action, camera_action;
    private View view;
    private Context context;
    private Button btn_submit;
    private File cameraFile, videoFile;
    private TextView class_name;
    private Uri cameraUri, VideoUri;

    public UploadFeed() {
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.upload_feed, container, false);

        initViews();

        return view;
    }

    private void initViews() {

        context = getActivity();

        gallery_action = view.findViewById(R.id.gallery_action);
        video_action = view.findViewById(R.id.video_action);
        camera_action = view.findViewById(R.id.camera_action);

        btn_submit = view.findViewById(R.id.btn_submit);
        et_text = view.findViewById(R.id.et_text);

        recycler_view = view.findViewById(R.id.recycler_view);


        class_name = view.findViewById(R.id.class_name);


        gallery_action.setOnClickListener(this);
        video_action.setOnClickListener(this);
        camera_action.setOnClickListener(this);


        class_name.setOnClickListener(this);
        btn_submit.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {

        if (v == class_name) {
            CommonMethod.showClassDialog(context, class_name);
        }


        if (v == gallery_action) {
            Intent intent = new Intent();
            intent.setType("*/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), 4);
        }


        if (v == video_action) {

          /*  File mediaFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/myvideo.mp4");
            Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
            Uri videoUri = Uri.fromFile(mediaFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, videoUri);
            startActivityForResult(intent, 5);*/


            Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
            videoFile = getPhotoFileUri("/myvideo.mp4");
            VideoUri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", videoFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, VideoUri);

            if (intent.resolveActivity(context.getPackageManager()) != null) {
                startActivityForResult(intent, 5);
            }

        }

        if (v == camera_action) {
            onLaunchCamera();
        }


        if (v == btn_submit) {

            if (TextUtils.isEmpty(class_name.getText().toString())) {
                CommonMethod.showAlert("Please Select Class", context);
            } else if (TextUtils.isEmpty(et_text.getText().toString().trim())) {
                et_text.requestFocus();
                et_text.setError(getString(R.string.field_required));
            } else if (fileList.size() == 0) {
                CommonMethod.showAlert("Please Select Files", context);
            } else {
                createNotice();
            }

        }

    }


    public void onLaunchCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraFile = getPhotoFileUri("photo.jpg");
        cameraUri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", cameraFile);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, cameraUri);
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            startActivityForResult(intent, 3);
        }
    }


    public File getPhotoFileUri(String fileName) {
        File mediaStorageDir = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "DigiShiksha");
        if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) {
            Log.e("DigiShiksha--", "failed to create directory");
        }

        return new File(mediaStorageDir.getPath() + File.separator + fileName);
    }

    @Override
    @SuppressLint("NewApi")
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.e("requestCode-", String.valueOf(requestCode));
        Log.e("resultCode-", String.valueOf(resultCode));
        Log.e("data-", String.valueOf(data));

        if (resultCode != RESULT_OK) {

            return;
        }

        if (requestCode == 3) {
            getFileData(cameraUri);
        }


        if (requestCode == 5) {
            getFileData(VideoUri);
        }

        if (requestCode == 4 && data != null && data.getData() != null) {
            Uri uri = data.getData();
            getFileData(uri);
        }

    }

    private void getFileData(Uri uri) {

        String type = getMimeType(uri);

        if (TextUtils.isEmpty(type)) {
            CommonMethod.showAlert("Un Supported File Type", context);
            return;
        }

        Log.e("Type---", "--" + type);
        if (type.equals("png") || type.equals("jpg") || type.equals("pdf") || type.equals("mp4") || type.equals("xlsx") || type.equals("excel") || type.equals("txt")) {
            InputStream iStream = null;
            try {
                iStream = context.getContentResolver().openInputStream(uri);
                byte[] inputData = getBytes(iStream);
                fileList.add(new UploadFileModel(type, uri, inputData));
                CommonMethod.setGridRecyclerview(context, recycler_view, 4);
                recycler_view.setAdapter(new UploadFilesAdapter(fileList, false));

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            CommonMethod.showAlert("Un Supported File Type", context);
        }
    }

    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }


    private void createNotice() {

        final ProgressD pd = ProgressD.show(context, getString(R.string.connecting), true);
        String appBaseUrl = LoginPreferences.getActiveInstance(context).getBASE_URL();


        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, appBaseUrl, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                pd.dismiss();
                String resultResponse = new String(response.data);
                try {
                    Log.e("Response---", resultResponse);

                    Gson gson = new Gson();
                    CommonResponse result = gson.fromJson(String.valueOf(response), CommonResponse.class);
                    if (result.result) {
                        CommonMethod.showAlert(result.message, getActivity());
                    } else {
                        CommonMethod.showAlert(result.message, context);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");

                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Log.i("Error", errorMessage);
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("api_page", ConstantURL.UPLOAD_NOTICE);
                params.put("emp_id", LoginPreferences.getActiveInstance(context).getId());
                params.put("school_id", LoginPreferences.getActiveInstance(context).getSchoolId());
                params.put("branch_id", LoginPreferences.getActiveInstance(context).getBranchId());
                params.put("session_id", LoginPreferences.getActiveInstance(context).getSessionId());
                params.put("class_id", AppData.getInstance().getClassId());
                params.put("text", et_text.getText().toString().trim());

                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                params.put("file", new DataPart(fileList.get(0).getUri().getPath(), fileList.get(0).getFileData(), fileList.get(0).getType()));
                return params;
            }
        };

        VolleySingleton.getInstance(context).addToRequestQueue(multipartRequest);
    }

    private String getMimeType(Uri uri) {
        ContentResolver cR = context.getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }
}
