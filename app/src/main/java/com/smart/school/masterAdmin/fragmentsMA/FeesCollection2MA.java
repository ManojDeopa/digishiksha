package com.smart.school.masterAdmin.fragmentsMA;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.smart.school.R;
import com.smart.school.masterAdmin.activitiesMA.MADetails;
import com.smart.school.masterAdmin.response.PayModeResponse;
import com.smart.school.utils.AppData;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.ConstantURL;
import com.smart.school.utils.Constants;
import com.smart.school.utils.LoginPreferences;
import com.smart.school.utils.ProgressD;
import com.smart.school.utils.VolleySingleton;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Manoj Singh Deopa on 2/4/18.
 */

@SuppressLint("ValidFragment")

public class FeesCollection2MA extends Fragment {

    TextView start_date, end_date, class_section, pay_up_to_month, pay_status, pay_mode;
    LinearLayout start_date_layout, end_date_layout, pay_mode_layout, pay_status_layout, class_section_layout, pay_up_to_month_layout, instrument_layout;
    CheckBox cb_fee_collection, cb_expense;
    private String type;
    private Context context;
    private View view;
    private String page_id;
    private String expense = "1", fee_collection = "1";
    private EditText et_instrument_no;
    private String[] pay_mode_array;
    private List<PayModeResponse.Datum> payModeList = new ArrayList<>();
    private String pay_mode_id;


    public FeesCollection2MA(String type) {
        this.type = type;
    }

    private void initViews() {
        start_date = view.findViewById(R.id.start_date);
        end_date = view.findViewById(R.id.end_date);
        start_date_layout = view.findViewById(R.id.start_date_layout);
        end_date_layout = view.findViewById(R.id.end_date_layout);
        pay_mode_layout = view.findViewById(R.id.pay_mode_layout);
        pay_mode = view.findViewById(R.id.pay_mode);
        pay_status_layout = view.findViewById(R.id.pay_status_layout);
        class_section_layout = view.findViewById(R.id.class_section_layout);
        class_section = view.findViewById(R.id.class_section);
        pay_up_to_month = view.findViewById(R.id.pay_up_to_month);
        pay_up_to_month_layout = view.findViewById(R.id.pay_up_to_month_layout);
        cb_fee_collection = view.findViewById(R.id.cb_fee_collection);
        cb_expense = view.findViewById(R.id.cb_expense);
        pay_status = view.findViewById(R.id.pay_status);
        instrument_layout = view.findViewById(R.id.instrument_layout);
        et_instrument_no = view.findViewById(R.id.et_instrument_no);

        switch (type) {

            case Constants.DAY_BOOK:
                page_id = "2";
                start_date_layout.setVisibility(View.VISIBLE);
                end_date_layout.setVisibility(View.VISIBLE);
                cb_fee_collection.setVisibility(View.VISIBLE);
                cb_expense.setVisibility(View.VISIBLE);
                break;

            case Constants.STUDENT_PAID_SUMMARY:
                page_id = "3";
                start_date_layout.setVisibility(View.VISIBLE);
                end_date_layout.setVisibility(View.VISIBLE);
                pay_mode_layout.setVisibility(View.VISIBLE);
                pay_status_layout.setVisibility(View.VISIBLE);
                instrument_layout.setVisibility(View.VISIBLE);
                getPayModeData();
                break;

            case Constants.STUDENT_FEE_DEFAULTER:
                page_id = "7";
                class_section_layout.setVisibility(View.VISIBLE);
                pay_up_to_month_layout.setVisibility(View.VISIBLE);

                class_section.setText(CommonMethod.getClassListForSchool(context).get(0).className);
                AppData.getInstance().setClassId(CommonMethod.getClassListForSchool(context).get(0).id);

                break;

            case Constants.GUARDIAN_WISE_FEE_DEFAULTER:
                page_id = "8";
                pay_mode_layout.setVisibility(View.VISIBLE);
                break;

        }

    }


    private void getPayModeData() {
        String appBaseUrl = LoginPreferences.getActiveInstance(context).getBASE_URL();
        Log.e("URL---", appBaseUrl);
        final ProgressD pd = ProgressD.show(context, context.getResources().getString(R.string.connecting), true);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, appBaseUrl,
                new Response.Listener<String>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(String response) {
                        pd.dismiss();
                        Log.e("Response--", response);
                        Gson gson = new Gson();
                        PayModeResponse result = gson.fromJson(String.valueOf(response), PayModeResponse.class);
                        if (result.result) {


                            payModeList = result.data;
                            ArrayList<String> payMode = new ArrayList<>();

                            for (int i = 0; i < result.data.size(); i++) {
                                payMode.add(result.data.get(i).paymode);
                            }

                            pay_mode_array = new String[payMode.size()];
                            pay_mode_array = payMode.toArray(pay_mode_array);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                        Log.e("error--", error.toString());
                        Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> map = CommonMethod.MasterAdminParams(context);
                map.put("api_page", Constants.PAY_MODE);

                return map;
            }
        };

        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);

    }


    private void payModeDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("----------Select---------");
        builder.setItems(pay_mode_array, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                String s = pay_mode_array[i];
                pay_mode.setText(s);
                pay_mode_id = payModeList.get(i).paymodeId;
            }
        });

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void payStatusDialog() {
        final String[] arr = {"Paid", "Un Paid", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("----------Select---------");
        builder.setItems(arr, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                String s = arr[i];
                pay_status.setText(s);
            }
        });

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fees_collection2, container, false);

        context = getActivity();

        initViews();

        setCurrentDate();

        onClicks();

        return view;
    }


    private void onClicks() {

        pay_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                payStatusDialog();
            }
        });

        pay_mode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                payModeDialog();
            }
        });


        class_section_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethod.showClassDialog(context, class_section);
            }
        });

        pay_up_to_month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethod.showDatePicker(context, pay_up_to_month);
            }
        });

        cb_fee_collection.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) fee_collection = "1";
                else fee_collection = "0";
            }
        });

        cb_expense.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) expense = "1";
                else expense = "0";
            }
        });


        start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethod.showDatePicker(context, start_date);
            }
        });

        end_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethod.showDatePicker(context, end_date);
            }
        });

        view.findViewById(R.id.btn_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Map<String, String> params = new HashMap<>();
                params.put("api_page", ConstantURL.ACCOUNT_FEES);
                params.put("page_id", page_id);
                params.put("emp_id", LoginPreferences.getActiveInstance(context).getId());
                params.put("school_id", LoginPreferences.getActiveInstance(context).getSchoolId());
                params.put("branch_id", LoginPreferences.getActiveInstance(context).getBranchId());
                params.put("session_id", LoginPreferences.getActiveInstance(context).getSessionId());

                switch (type) {
                    case Constants.DAY_BOOK:
                        params.put("start_date", start_date.getText().toString());
                        params.put("end_date", end_date.getText().toString());
                        params.put("fee_collection", fee_collection);
                        params.put("expense", expense);

                        AppData.getInstance().setFeeParams(params);
                        startActivity(new Intent(context, MADetails.class).putExtra(Constants.TYPE, Constants.DETAIL));

                        break;
                    case Constants.STUDENT_PAID_SUMMARY:

                        params.put("start_date", start_date.getText().toString());
                        params.put("end_date", end_date.getText().toString());
                        params.put("pay_mode", pay_mode_id);
                        params.put("pay_status", pay_status.getText().toString());
                        params.put("instrument_no", et_instrument_no.getText().toString());

                        AppData.getInstance().setFeeParams(params);
                        startActivity(new Intent(context, MADetails.class).putExtra(Constants.TYPE, Constants.DETAIL));
                        break;

                    case Constants.STUDENT_FEE_DEFAULTER:
                        params.put("class_id", AppData.getInstance().getClassId());
                        params.put("pay_up_to_month", pay_up_to_month.getText().toString());

                        AppData.getInstance().setFeeParams(params);
                        startActivity(new Intent(context, MADetails.class).putExtra(Constants.TYPE, Constants.DETAIL));

                        break;
                    case Constants.GUARDIAN_WISE_FEE_DEFAULTER:
                        CommonMethod.showFinishAlert(Constants.SERVER_ERROR, getActivity());

                        break;

                }

           /*     AppData.getInstance().setFeeParams(params);
                startActivity(new Intent(context, MADetails.class).putExtra(Constants.TYPE, Constants.DETAIL));*/

            }
        });

    }


    private void setCurrentDate() {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(c);
        start_date.setText(formattedDate);
        end_date.setText(formattedDate);
        pay_up_to_month.setText(formattedDate);
    }


}
