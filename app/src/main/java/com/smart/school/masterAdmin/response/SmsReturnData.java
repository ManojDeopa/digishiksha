package com.smart.school.masterAdmin.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by manoj on 19/5/19.
 */
public class SmsReturnData {

    @SerializedName("result")
    @Expose
    public Boolean result;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("set_text")
    @Expose
    public SetText setText;
    @SerializedName("text_data")
    @Expose
    public List<TextDatum> textData = null;
    @SerializedName("sms_type")
    @Expose
    public List<SmsType> smsType = null;

    @SerializedName("text_template")
    @Expose
    public List<TextTemplate> textTemplate = null;


    public class SmsType {

        @SerializedName("sms_type_id")
        @Expose
        public Integer smsTypeId;
        @SerializedName("sms_type")
        @Expose
        public String smsType;
        @SerializedName("default")
        @Expose
        public Integer _default;

    }

    public class TextTemplate {

        @SerializedName("text_template_id")
        @Expose
        public String textTemplateId;
        @SerializedName("text_template_title")
        @Expose
        public String textTemplateTitle;
        @SerializedName("text_template")
        @Expose
        public String textTemplate;

    }


    public class SetText {

        @SerializedName("top_text")
        @Expose
        public String topText;
        @SerializedName("bottom_text")
        @Expose
        public String bottomText;

    }

    public class TextDatum {

        @SerializedName("text_id")
        @Expose
        public String textId;
        @SerializedName("top_text_data")
        @Expose
        public String topTextData;
        @SerializedName("bottom_text_data")
        @Expose
        public String bottomTextData;

    }

}
