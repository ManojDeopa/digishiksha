package com.smart.school.masterAdmin.adaptersMA;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.masterAdmin.response.UploadFileModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


/**
 * Created by Manoj Singh Deopa .
 */

public class UploadFilesAdapter extends RecyclerView.Adapter<UploadFilesAdapter.ViewHolder> {
    public static ArrayList<UploadFileModel> list = new ArrayList<>();
    private final boolean isTvShow;
    private Context context;

    public UploadFilesAdapter(ArrayList<UploadFileModel> uploadImageList, boolean b) {
        isTvShow = b;
        list = uploadImageList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.upload_image_adapter, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int i) {
        int placeHolder=R.drawable.ic_file;
        if (list.get(i).getType().equals("mp4")) {
            placeHolder=R.drawable.dummy_icon;
            holder.ic_play.setVisibility(View.VISIBLE);
        } else {
            holder.ic_play.setVisibility(View.GONE);
        }

        Picasso.get().load(list.get(i).getUri())
                .placeholder(placeHolder)
                .resize(350, 350)
                .into(holder.image);
        holder.file_name.setText(list.get(i).getUri().getPath());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView file_name;
        private ImageView image, delete, ic_play;

        public ViewHolder(View view) {
            super(view);
            file_name = view.findViewById(R.id.file_name);
            if (isTvShow) {
                file_name.setVisibility(View.VISIBLE);
            }
            image = view.findViewById(R.id.image);
            ic_play = view.findViewById(R.id.ic_play);
            delete = view.findViewById(R.id.delete);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    list.remove(getAdapterPosition());
                    notifyItemRemoved(getAdapterPosition());
                    notifyItemRangeChanged(getAdapterPosition(), list.size());
                }
            });
        }
    }
}