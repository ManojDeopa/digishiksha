package com.smart.school.masterAdmin.adaptersMA;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.masterAdmin.response.OutNotificationResponse;
import com.smart.school.utils.CommonMethod;

import java.util.List;

/**
 * Created by manoj on 11/5/19.
 */
public class OutInNotificationAdapter extends RecyclerView.Adapter<OutInNotificationAdapter.ViewHolder> {
    private List<OutNotificationResponse.Datum> list;
    private Context context;


    public OutInNotificationAdapter(List<OutNotificationResponse.Datum> data) {
        this.list = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.in_out_notification_adapter, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        holder.receiver.setText("Receiver : " + list.get(i).receiver);
        holder.created_at.setText("Created At : " + list.get(i).createdAt);
        holder.message.setText(list.get(i).outgoingSms);
        holder.status.setText("Sent Status : " + list.get(i).smsDelivery);

        if (list.get(i).files.size() != 0) {
            CommonMethod.setRecyclerView(holder.recycler_view, context);
            holder.recycler_view.setAdapter(new FilesAdapter(list.get(i).files));
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView receiver, created_at, message, status;

        RecyclerView recycler_view;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            recycler_view = itemView.findViewById(R.id.recycler_view);
            receiver = itemView.findViewById(R.id.receiver);
            created_at = itemView.findViewById(R.id.created_at);
            message = itemView.findViewById(R.id.message);
            status = itemView.findViewById(R.id.status);

        }
    }
}
