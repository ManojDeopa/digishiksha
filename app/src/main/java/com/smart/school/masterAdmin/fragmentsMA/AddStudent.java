package com.smart.school.masterAdmin.fragmentsMA;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.smart.school.R;
import com.smart.school.teacher.activityT.QuickActionDetails;
import com.smart.school.teacher.responsesT.CommonResponse;
import com.smart.school.teacher.responsesT.StudentListResponse;
import com.smart.school.utils.AppData;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.ConstantURL;
import com.smart.school.utils.Constants;
import com.smart.school.utils.LoginPreferences;
import com.smart.school.utils.ProgressD;
import com.smart.school.utils.VolleySingleton;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by Manoj Singh Deopa on 2/4/18.
 */

@SuppressLint("ValidFragment")

public class AddStudent extends Fragment {

    String type;
    RadioGroup radio_group;
    Button btn_submit;
    EditText adm_number, student_name, father_name, mother_name, mobile_no, address;
    private Context context;
    private View view;
    private TextView class_name;
    private TextView dob;
    private StudentListResponse.Datum data;
    private String gender = "Male";
    private String student_id;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.add_student, container, false);

        initViews();

        onClicks();

        return view;
    }


    @SuppressLint("SetTextI18n")
    private void initViews() {

        context = getActivity();
        type = QuickActionDetails.activity.type;

        class_name = view.findViewById(R.id.class_name);
        dob = view.findViewById(R.id.date);
        radio_group = view.findViewById(R.id.radio_group);
        btn_submit = view.findViewById(R.id.btn_submit);

        adm_number = view.findViewById(R.id.adm_number);
        student_name = view.findViewById(R.id.student_name);
        father_name = view.findViewById(R.id.father_name);
        mother_name = view.findViewById(R.id.mother_name);
        mobile_no = view.findViewById(R.id.mobile_no);
        address = view.findViewById(R.id.address);

        if (type.equals(Constants.TYPE_EDIT_STUDENT)) {
            data = AppData.getInstance().getStudentData();
            btn_submit.setText(getString(R.string.update));
            student_id = data.t1Id;
            class_name.setText(data.t4CourseName + "-" + data.t5SectionName);
            adm_number.setText(data.t1AdmissionNo);
            student_name.setText(data.t2StudentName);
            mother_name.setText(data.t3_mother_name);
            father_name.setText(data.t3FatherName);
            mobile_no.setText(data.t3FatherMobileNo);
            dob.setText(data.t2_dob);
            address.setText(data.t2_address);

            if (data.t2_gender.equals("female")) {
                gender = "Female";
                radio_group.check(R.id.rb_female);

            } else {
                gender = "Male";
                radio_group.check(R.id.rb_male);
            }

        }

    }

    private void onClicks() {

        radio_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                                   @Override
                                                   public void onCheckedChanged(RadioGroup group, int checkedId) {
                                                       RadioButton radioButton = group.findViewById(checkedId);
                                                       gender = String.valueOf(radioButton.getText());
                                                   }
                                               }
        );

        class_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethod.showClassDialog(context, class_name);
            }
        });

        dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethod.showDatePicker(context, dob);
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate();
            }
        });
    }

    private void validate() {

        if (TextUtils.isEmpty(class_name.getText().toString())) {
            CommonMethod.showAlert("Please Select Class", context);
        } else if (TextUtils.isEmpty(student_name.getText().toString())) {
            student_name.requestFocus();
            student_name.setError(getString(R.string.field_required));
        } else if (TextUtils.isEmpty(dob.getText().toString())) {
            CommonMethod.showAlert("Please Select DOB", context);
        } else if (TextUtils.isEmpty(father_name.getText().toString())) {
            father_name.requestFocus();
            father_name.setError(getString(R.string.field_required));
        } else if (TextUtils.isEmpty(mother_name.getText().toString())) {
            mother_name.requestFocus();
            mother_name.setError(getString(R.string.field_required));
        } else if (TextUtils.isEmpty(mobile_no.getText().toString())) {
            mobile_no.requestFocus();
            mobile_no.setError(getString(R.string.field_required));
        } else if (TextUtils.isEmpty(address.getText().toString())) {
            address.requestFocus();
            address.setError(getString(R.string.field_required));
        } else {
            showAlert();
        }

    }

    private void showAlert() {

        String text = "Are you sure to Add ";
        if (type.equals(Constants.TYPE_EDIT_STUDENT)) {
            text = "Are you sure to Edit ";
        }

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(text + student_name.getText().toString() + " ? ");

        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                callApi();
                dialog.dismiss();
            }
        });


        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }


    private void callApi() {
        String appBaseUrl = LoginPreferences.getActiveInstance(context).getBASE_URL();
        Log.e("URL---", appBaseUrl);
        final ProgressD pd = ProgressD.show(context, context.getResources().getString(R.string.connecting), true);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, appBaseUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.dismiss();

                        Log.e("Response--", response);

                        Gson gson = new Gson();
                        CommonResponse result = gson.fromJson(String.valueOf(response), CommonResponse.class);

                        if (result.result) {
                            CommonMethod.showFinishAlert(result.message, getActivity());

                        } else {
                            CommonMethod.showAlert(result.message, context);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                        Log.e("error--", error.toString());
                        Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                Log.e("Class ID--", AppData.getInstance().getClassId());
                Log.e("EMP ID--", LoginPreferences.getActiveInstance(context).getId());
                Log.e("school ID--", LoginPreferences.getActiveInstance(context).getSchoolId());
                Log.e("branch ID--", LoginPreferences.getActiveInstance(context).getBranchId());

                Map<String, String> params = new HashMap<>();

                if (type.equals(Constants.TYPE_EDIT_STUDENT)) {
                    params.put("student_id", student_id);
                    params.put("api_page", ConstantURL.EDIT_STUDENT);
                } else {
                    params.put("api_page", ConstantURL.ADD_STUDENT);
                }

                params.put("emp_id", LoginPreferences.getActiveInstance(context).getId());
                params.put("school_id", LoginPreferences.getActiveInstance(context).getSchoolId());
                params.put("branch_id", LoginPreferences.getActiveInstance(context).getBranchId());
                params.put("session_id", LoginPreferences.getActiveInstance(context).getSessionId());

                params.put("class_id", AppData.getInstance().getClassId());
                params.put("admission_no", adm_number.getText().toString());
                params.put("student_name", student_name.getText().toString());
                params.put("gender", gender);
                params.put("dob", dob.getText().toString());
                params.put("father_name", father_name.getText().toString());
                params.put("mother_name", mother_name.getText().toString());
                params.put("mobile_no", mobile_no.getText().toString());
                params.put("address", address.getText().toString());

                return params;
            }
        };

        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);

    }


}
