package com.smart.school.masterAdmin.activitiesMA;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.smart.school.R;
import com.smart.school.masterAdmin.fragmentsMA.AcFeeWebview;
import com.smart.school.masterAdmin.fragmentsMA.AssignmentTabMA;
import com.smart.school.masterAdmin.fragmentsMA.FeesCollection2MA;
import com.smart.school.masterAdmin.fragmentsMA.FeesCollectionMA;
import com.smart.school.masterAdmin.fragmentsMA.NoticeTabFragment;
import com.smart.school.masterAdmin.fragmentsMA.SendNotification;
import com.smart.school.masterAdmin.fragmentsMA.SendNotificationTab;
import com.smart.school.masterAdmin.fragmentsMA.UploadFeed;
import com.smart.school.utils.AppUtils;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.Constants;

public class MADetails extends AppCompatActivity {

    private static final String TAG = "MADetails";
    public static MADetails activity;
    public String type;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quick_action_details);
        context = MADetails.this;
        activity = MADetails.this;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        type = getIntent().getStringExtra(Constants.TYPE);
        getSupportActionBar().setTitle(type);
        setFragment(type);
    }


    private void setFragment(String type) {

        Fragment fragment = new Fragment();

        switch (type) {

            case Constants.TYPE_FEES_COLLECTION:
                fragment = new FeesCollectionMA(type);
                break;

            case Constants.ACCOUNT_BALANCE:
                fragment = new FeesCollectionMA(type);
                break;

            case Constants.FEE_DISCOUNT_SUMMARY:
                fragment = new FeesCollectionMA(type);
                break;

            case Constants.EXPENSE_SUMMARY:
                fragment = new FeesCollectionMA(type);
                break;


            case Constants.STUDENT_FEE_DEFAULTER:
                fragment = new FeesCollection2MA(type);
                break;

            case Constants.DAY_BOOK:
                fragment = new FeesCollection2MA(type);
                break;

            case Constants.STUDENT_PAID_SUMMARY:
                fragment = new FeesCollection2MA(type);
                break;

            case Constants.GUARDIAN_WISE_FEE_DEFAULTER:
                fragment = new FeesCollection2MA(type);
                break;


            case Constants.TYPE_SEND_NOTIFICATION:
                fragment = new SendNotificationTab();
                break;

            case Constants.TYPE_SEND_NOTIFICATION2:
                fragment = new SendNotification();
                break;

            case Constants.UPLOAD_NOTICE:
                fragment = new NoticeTabFragment();
                break;

            case Constants.UPLOAD_ASSIGNMENT:
                fragment = new AssignmentTabMA();
                break;

            case Constants.DETAIL:
                fragment = new AcFeeWebview();
                break;

            case Constants.UPLOAD_FEED:
                fragment = new UploadFeed();
                break;

            default:
                CommonMethod.showFinishAlert(Constants.SERVER_ERROR, this);
                break;
        }

        AppUtils.setFragment(fragment, true, MADetails.this, R.id.container);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        finish();
    }


}
