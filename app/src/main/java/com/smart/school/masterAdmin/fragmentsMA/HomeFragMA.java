package com.smart.school.masterAdmin.fragmentsMA;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smart.school.R;
import com.smart.school.masterAdmin.adaptersMA.AccountFeesAdapter;
import com.smart.school.masterAdmin.adaptersMA.CommunicationAdapter;
import com.smart.school.masterAdmin.adaptersMA.QuickActionAdapterMA;
import com.smart.school.utils.CommonMethod;


/**
 * Created by Manoj Singh Deopa on 2/4/18.
 */

@SuppressLint("ValidFragment")

public class HomeFragMA extends Fragment {
    private RecyclerView quick_action_rv, rv_ac_fees, rv_communication;
    private Context context;


    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.home_frag_ma, container, false);
        context = getActivity();


        quick_action_rv = view.findViewById(R.id.rv_quick_action);
        CommonMethod.setGridRecyclerview(context, quick_action_rv, 3);
        quick_action_rv.setAdapter(new QuickActionAdapterMA());


        rv_communication = view.findViewById(R.id.rv_communication);
        CommonMethod.setGridRecyclerview(context, rv_communication, 3);
        rv_communication.setAdapter(new CommunicationAdapter());


        rv_ac_fees = view.findViewById(R.id.rv_ac_fees);
        CommonMethod.setGridRecyclerview(context, rv_ac_fees, 3);
        rv_ac_fees.setAdapter(new AccountFeesAdapter());


        return view;
    }

}
