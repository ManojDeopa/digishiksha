package com.smart.school.masterAdmin.adaptersMA;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.masterAdmin.fragmentsMA.SendNotification;
import com.smart.school.masterAdmin.response.SmsReturnData;

import java.util.List;


/**
 * Created by Manoj Singh Deopa.
 */

public class HeaderFooterAdapter extends RecyclerView.Adapter<HeaderFooterAdapter.ViewHolder> {

    private AlertDialog alertdialog;
    private List<SmsReturnData.TextDatum> list;
    private Context context;

    public HeaderFooterAdapter(List<SmsReturnData.TextDatum> data, AlertDialog alertDialog) {
        this.list = data;
        this.alertdialog = alertDialog;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_footer_adapter, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int i) {
        holder.header.setText(list.get(i).topTextData);
        holder.footer.setText(list.get(i).bottomTextData);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView header, footer;

        public ViewHolder(View view) {
            super(view);
            header = view.findViewById(R.id.header);
            footer = view.findViewById(R.id.footer);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    SendNotification.top_text.setText(list.get(getAdapterPosition()).topTextData);
                    SendNotification.bottom_text.setText(list.get(getAdapterPosition()).bottomTextData);
                    alertdialog.dismiss();
                }
            });

        }
    }
}