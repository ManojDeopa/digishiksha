package com.smart.school.masterAdmin.adaptersMA;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.teacher.activityT.QuickActionDetails;
import com.smart.school.utils.AppData;
import com.smart.school.utils.Constants;
import com.squareup.picasso.Picasso;


/**
 * Created by Manoj Singh Deopa .
 */

public class QuickActionAdapterMA extends RecyclerView.Adapter<QuickActionAdapterMA.ViewHolder> {

    private Context context;
    private String[] list = {
            Constants.TYPE_ADD_STUDENT,
            Constants.TYPE_STUDENT_LIST,
            Constants.TYPE_MARK_ATTENDENCE,
            Constants.TYPE_UPLOAD_HOMEWORK,
            Constants.UPLOAD_STUDENT_PHOTO,
            Constants.TYPE_LEAVE_APPROVALS};

    private int[] list_image = {
            R.drawable.student_photo,
            R.drawable.student_list,
            R.drawable.mark_attendance,
            R.drawable.homework_icon,
            R.drawable.student_photo,
            R.drawable.exam_management};

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dashboard_item_layout, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.tv_title.setText(list[position]);
        Picasso.get().load(list_image[position]).into(holder.iv_image);
    }

    @Override
    public int getItemCount() {
        return list.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title;
        private ImageView iv_image;
        private LinearLayout parent;

        public ViewHolder(View view) {
            super(view);
            iv_image = view.findViewById(R.id.iv_image);
            tv_title = view.findViewById(R.id.tv_title);
            parent = view.findViewById(R.id.parent);

            parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppData.getInstance().setType(Constants.TYPE_MASTER_ADMIN);
                    String type = list[getAdapterPosition()].trim();
                    context.startActivity(new Intent(context, QuickActionDetails.class).putExtra(Constants.TYPE, type));
                }
            });

        }
    }

}