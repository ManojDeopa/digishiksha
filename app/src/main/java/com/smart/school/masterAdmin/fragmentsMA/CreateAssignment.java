package com.smart.school.masterAdmin.fragmentsMA;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.smart.school.BuildConfig;
import com.smart.school.R;
import com.smart.school.teacher.responsesT.CommonResponse;
import com.smart.school.utils.AppData;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.ConstantURL;
import com.smart.school.utils.GetSubjects;
import com.smart.school.utils.LoginPreferences;
import com.smart.school.utils.ProgressD;
import com.smart.school.utils.VolleyMultipartRequest;
import com.smart.school.utils.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;


/**
 * Created by Manoj Singh Deopa on 2/4/18.
 */

@SuppressLint("ValidFragment")

public class CreateAssignment extends Fragment implements View.OnClickListener {

    EditText et_title, et_description;
    ArrayList<byte[]> files = new ArrayList<byte[]>();
    TextView assigned_on, submitted_on;
    private View view;
    private Context context;
    private Button btn_submit;
    private LinearLayout upload;
    private File cameraFile;
    private TextView class_name, subject_name;


    public CreateAssignment() {
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.create_assignment, container, false);

        initViews();

        return view;
    }

    private void initViews() {

        context = getActivity();
        btn_submit = view.findViewById(R.id.btn_submit);
        upload = view.findViewById(R.id.upload);


        et_title = view.findViewById(R.id.et_title);
        et_description = view.findViewById(R.id.et_description);

        assigned_on = view.findViewById(R.id.assigned_on);
        submitted_on = view.findViewById(R.id.submitted_on);

        class_name = view.findViewById(R.id.class_name);
        subject_name = view.findViewById(R.id.subject_name);


        class_name.setOnClickListener(this);
        subject_name.setOnClickListener(this);

        btn_submit.setOnClickListener(this);

        upload.setOnClickListener(this);

        assigned_on.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethod.showDatePicker(context, assigned_on);
            }
        });

        submitted_on.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethod.showDatePicker(context, submitted_on);
            }
        });

        setCurrentDate();

    }

    private void setCurrentDate() {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(c);
        assigned_on.setText(formattedDate);
        submitted_on.setText(formattedDate);
    }


    @Override
    public void onClick(View v) {

        if (v == class_name) {
            CommonMethod.showClassDialog(context, class_name);

        }


        if (v == subject_name) {
            if (!TextUtils.isEmpty(AppData.getInstance().getClassId())) {
                new GetSubjects(context, subject_name);
            } else {
                CommonMethod.showAlert("Please Select Class First", context);
            }
        }

        if (v == upload) {
            selectImage();
        }

        if (v == btn_submit) {

            if (TextUtils.isEmpty(class_name.getText().toString())) {
                CommonMethod.showAlert("Please Select Class", context);
            } else if (TextUtils.isEmpty(et_title.getText().toString().trim())) {
                et_title.requestFocus();
                et_title.setError(getString(R.string.field_required));
            } else if (TextUtils.isEmpty(et_description.getText().toString().trim())) {
                et_description.requestFocus();
                et_description.setError(getString(R.string.field_required));
            } else if (files.size() == 0) {
                CommonMethod.showAlert("Please Select Files", context);
            } else {
                createAssignment();
            }

        }

    }


    public void selectImage() {
        final CharSequence[] items = {"Take Photo", "Select From Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    onLaunchCamera();
                } else if (items[item].equals("Select From Gallery")) {
                    Intent intent = new Intent();
                    intent.setType("*/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), 4);
                }
            }
        });
        builder.show();
    }


    public void onLaunchCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraFile = getPhotoFileUri("photo.jpg");

        // wrap File object into a content provider
        // required for API >= 24
        // See https://guides.codepath.com/android/Sharing-Content-with-Intents#sharing-files-with-api-24-or-higher

        Uri fileProvider = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", cameraFile);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileProvider);

        // If you call startActivityForResult() using an intent that no app can handle, your app will crash.
        // So as long as the result is not null, it's safe to use the intent.
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            // Start the image capture intent to take photo
            startActivityForResult(intent, 3);
        }
    }


    public File getPhotoFileUri(String fileName) {
        // Get safe storage directory for photos
        // Use `getExternalFilesDir` on Context to access package-specific directories.
        // This way, we don't need to request external read/write runtime permissions.
        File mediaStorageDir = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "DigiShiksha");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) {
            Log.e("DigiShiksha--", "failed to create directory");
        }

        return new File(mediaStorageDir.getPath() + File.separator + fileName);
    }

    @Override
    @SuppressLint("NewApi")
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.e("requestCode-", String.valueOf(requestCode));
        Log.e("resultCode-", String.valueOf(resultCode));
        Log.e("data-", String.valueOf(data));

        if (resultCode != RESULT_OK) {

            return;
        }

        if (requestCode == 3) {
            Uri uri = Uri.fromFile(cameraFile);
            getFileData(uri);

        }
        if (requestCode == 4 && data != null && data.getData() != null) {
            Uri uri = data.getData();
            getFileData(uri);
        }

    }

    private void getFileData(Uri uri) {
        InputStream iStream = null;
        try {
            iStream = context.getContentResolver().openInputStream(uri);
            byte[] inputData = getBytes(iStream);
            files.add(inputData);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }


    private void createAssignment() {

        final ProgressD pd = ProgressD.show(context, getString(R.string.connecting), true);
        String appBaseUrl = LoginPreferences.getActiveInstance(context).getBASE_URL();


        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, appBaseUrl, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                pd.dismiss();
                String resultResponse = new String(response.data);
                try {
                    Log.e("Response---", resultResponse);

                    Gson gson = new Gson();
                    CommonResponse result = gson.fromJson(String.valueOf(response), CommonResponse.class);
                    if (result.result) {
                        CommonMethod.showAlert(result.message, getActivity());
                    } else {
                        CommonMethod.showAlert(result.message, context);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");

                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Log.i("Error", errorMessage);
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("api_page", ConstantURL.UPLOAD_NOTICE);
                params.put("emp_id", LoginPreferences.getActiveInstance(context).getId());
                params.put("school_id", LoginPreferences.getActiveInstance(context).getSchoolId());
                params.put("branch_id", LoginPreferences.getActiveInstance(context).getBranchId());
                params.put("session_id", LoginPreferences.getActiveInstance(context).getSessionId());
                params.put("class_id", AppData.getInstance().getClassId());
                params.put("title", et_title.getText().toString().trim());
                params.put("description", et_description.getText().toString().trim());

                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                params.put("file", new DataPart("file_avatar.jpg", files.get(0), "image/jpeg"));
                return params;
            }
        };

        VolleySingleton.getInstance(context).addToRequestQueue(multipartRequest);
    }


}
