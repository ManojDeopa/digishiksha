package com.smart.school.masterAdmin.fragmentsMA;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.smart.school.BuildConfig;
import com.smart.school.R;
import com.smart.school.masterAdmin.adaptersMA.HeaderFooterAdapter;
import com.smart.school.masterAdmin.adaptersMA.TemplateAdapter;
import com.smart.school.masterAdmin.response.SmsReturnData;
import com.smart.school.teacher.adaptersT.UploadImageAdapterT;
import com.smart.school.teacher.responsesT.CommonResponse;
import com.smart.school.utils.AppData;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.ConstantURL;
import com.smart.school.utils.LoginPreferences;
import com.smart.school.utils.ProgressD;
import com.smart.school.utils.VolleySingleton;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.app.Activity.RESULT_OK;


/**
 * Created by Manoj Singh Deopa on 2/4/18.
 */

@SuppressLint("ValidFragment")

public class SendNotification extends Fragment implements View.OnClickListener {

    public static ArrayList<String> uploadImageList = new ArrayList<>();
    public static EditText top_text, bottom_text, middle_text;
    Button btn_submit;
    LinearLayout upload;
    TextView class_name;
    TextView header_footer, select_template;
    private Context context;
    private RecyclerView rv_upload_image;
    private File cameraFile;
    private String classId;
    private View view;
    private String sms_type;
    private String language = "English";
    private List<SmsReturnData.SmsType> sms_type_list;
    private String sms_employee = "1", sms_duplicate = "1";

    public SendNotification() {
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.send_notification, container, false);

        initViews();

        getWrittenData();

        getselectedClass();

        return view;
    }

    private void initViews() {
        context = getActivity();
        btn_submit = view.findViewById(R.id.btn_submit);
        header_footer = view.findViewById(R.id.header_footer);
        header_footer.setVisibility(View.GONE);
        class_name = view.findViewById(R.id.class_name);
        upload = view.findViewById(R.id.upload);
        rv_upload_image = view.findViewById(R.id.recycler_view);
        top_text = view.findViewById(R.id.top_text);
        middle_text = view.findViewById(R.id.middle_text);
        bottom_text = view.findViewById(R.id.bottom_text);

        select_template = view.findViewById(R.id.select_template);

        btn_submit.setOnClickListener(this);
        upload.setOnClickListener(this);
    }

    private void getselectedClass() {

        List<String> classList = new ArrayList<>();
        String Sclass = "";
        for (int i = 0; i < CreateNotification.classlist.size(); i++) {
            if (CreateNotification.classlist.get(i).isChecked) {
                Sclass = CreateNotification.classlist.get(i).className + "," + Sclass;
                classId = CreateNotification.classlist.get(i).id + "," + classId;
                classList.add(CreateNotification.classlist.get(i).className);

            }
        }

        String[] mStringArray = new String[classList.size()];
        mStringArray = classList.toArray(mStringArray);

        String classtext = classList.size() + " Class-Section Selected ";
        class_name.setText(classtext);
        /*class_name.setText(Sclass);*/

        final String[] finalMStringArray = mStringArray;
        class_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedClassDialog(finalMStringArray);
            }
        });

    }

    private void selectedClassDialog(String[] classArr) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("              Selected Class          ");
        builder.setItems(classArr, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                }
            }
        });

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }


    @Override
    public void onClick(View v) {

        if (v == upload) {
            selectImage();
        }

        if (v == btn_submit) {
            showPreviewDialog();
        }
    }


    public void selectImage() {
        final CharSequence[] items = {"Take Photo", "Select From Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    onLaunchCamera();

                } else if (items[item].equals("Select From Gallery")) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), 4);
                }
            }
        });
        builder.show();
    }


    public void onLaunchCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraFile = getPhotoFileUri("photo.jpg");

        // wrap File object into a content provider
        // required for API >= 24
        // See https://guides.codepath.com/android/Sharing-Content-with-Intents#sharing-files-with-api-24-or-higher

        Uri fileProvider = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", cameraFile);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileProvider);

        // If you call startActivityForResult() using an intent that no app can handle, your app will crash.
        // So as long as the result is not null, it's safe to use the intent.
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            // Start the image capture intent to take photo
            startActivityForResult(intent, 3);
        }
    }


    public File getPhotoFileUri(String fileName) {
        // Get safe storage directory for photos
        // Use `getExternalFilesDir` on Context to access package-specific directories.
        // This way, we don't need to request external read/write runtime permissions.
        File mediaStorageDir = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "DigiShiksha");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) {
            Log.e("DigiShiksha--", "failed to create directory");
        }

        return new File(mediaStorageDir.getPath() + File.separator + fileName);
    }

    @Override
    @SuppressLint("NewApi")
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.e("requestCode-", String.valueOf(requestCode));
        Log.e("resultCode-", String.valueOf(resultCode));
        Log.e("data-", String.valueOf(data));

        if (resultCode != RESULT_OK) {
            return;
        }
        if (requestCode == 3) {

            Uri uri = Uri.fromFile(cameraFile);
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (bitmap != null) {
                setImageData(bitmap);
            }

        }
        if (requestCode == 4 && data != null && data.getData() != null) {
            Uri uri = data.getData();
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (bitmap != null) {
                setImageData(bitmap);
            }
        }

    }


    private void setImageData(Bitmap bitmap) {
        Bitmap resized = Bitmap.createScaledBitmap(bitmap, 900, 900, true);
        AppData.getInstance().setfrom("AddHomework");
        uploadImageList.add(CommonMethod.convertBitmaptoString(resized));

        CommonMethod.setGridRecyclerview(context, rv_upload_image, 4);
        rv_upload_image.setAdapter(new UploadImageAdapterT(uploadImageList));

    }


    private void getWrittenData() {
        String appBaseUrl = LoginPreferences.getActiveInstance(context).getBASE_URL();
        Log.e("URL---", appBaseUrl);
        final ProgressD pd = ProgressD.show(context, context.getResources().getString(R.string.connecting), true);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, appBaseUrl,
                new Response.Listener<String>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(String response) {
                        pd.dismiss();
                        Log.e("Response--", response);
                        Gson gson = new Gson();
                        final SmsReturnData result = gson.fromJson(String.valueOf(response), SmsReturnData.class);
                        if (result.result) {

                            top_text.setText(result.setText.topText);
                            bottom_text.setText(result.setText.bottomText);

                            if (result.textData.size() != 0) {
                                header_footer.setVisibility(View.VISIBLE);
                                header_footer.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        showHFDialog(result.textData);
                                    }
                                });
                            }


                            if (result.textTemplate.size() != 0) {
                                select_template.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        showTemplateDialog(result.textTemplate);
                                    }
                                });
                            }

                            sms_type_list = result.smsType;


                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                        Log.e("error--", error.toString());
                        Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                Log.e("EMP ID--", LoginPreferences.getActiveInstance(context).getId());
                Log.e("school ID--", LoginPreferences.getActiveInstance(context).getSchoolId());
                Log.e("branch ID--", LoginPreferences.getActiveInstance(context).getBranchId());

                Map<String, String> params = new HashMap<>();
                params.put("api_page", ConstantURL.SMS_RETURN_DATA);
                params.put("emp_id", LoginPreferences.getActiveInstance(context).getId());
                params.put("school_id", LoginPreferences.getActiveInstance(context).getSchoolId());
                params.put("branch_id", LoginPreferences.getActiveInstance(context).getBranchId());
                params.put("session_id", LoginPreferences.getActiveInstance(context).getSessionId());

                return params;
            }
        };

        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);

    }


    private void showHFDialog(List<SmsReturnData.TextDatum> textData) {
        ViewGroup viewGroup = getActivity().findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_list_layout, viewGroup, false);

        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(context);
        builder.setView(dialogView);

        final android.support.v7.app.AlertDialog alertDialog = builder.create();

        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.show();

        RecyclerView recycler_view = dialogView.findViewById(R.id.recycler_view);
        CommonMethod.setRecyclerView(recycler_view, context);
        recycler_view.setAdapter(new HeaderFooterAdapter(textData, alertDialog));

        dialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });


    }


    private void showTemplateDialog(List<SmsReturnData.TextTemplate> textTemplates) {
        ViewGroup viewGroup = getActivity().findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_list_layout, viewGroup, false);

        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(context);
        builder.setView(dialogView);

        final android.support.v7.app.AlertDialog alertDialog = builder.create();

        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        alertDialog.show();

        RecyclerView recycler_view = dialogView.findViewById(R.id.recycler_view);
        CommonMethod.setRecyclerView(recycler_view, context);
        recycler_view.setAdapter(new TemplateAdapter(textTemplates, alertDialog));

        dialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });


    }


    private void showPreviewDialog() {

        if (TextUtils.isEmpty(top_text.getText().toString())) {
            top_text.requestFocus();
            top_text.setError(getString(R.string.field_required));
            return;

        } else if (TextUtils.isEmpty(middle_text.getText().toString())) {
            middle_text.requestFocus();
            middle_text.setError(getString(R.string.field_required));
            return;

        } else if (TextUtils.isEmpty(bottom_text.getText().toString())) {
            bottom_text.requestFocus();
            bottom_text.setError(getString(R.string.field_required));
            return;

        }

        ViewGroup viewGroup = getActivity().findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(context).inflate(R.layout.preview_dialog_layout, viewGroup, false);

        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(context);
        builder.setView(dialogView);

        final android.support.v7.app.AlertDialog alertDialog = builder.create();

        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.show();

        final RadioGroup rg_language = dialogView.findViewById(R.id.rg_language);
        Spinner spinner_sms_type = dialogView.findViewById(R.id.spinner_sms_type);
        CheckBox cb_all_duplicate = dialogView.findViewById(R.id.cb_all_duplicate);
        CheckBox cb_employee = dialogView.findViewById(R.id.cb_employee);

        TextView texts = dialogView.findViewById(R.id.texts);

        String confm_text = top_text.getText().toString() + "\n" + middle_text.getText().toString() + "\n" + bottom_text.getText().toString();

        texts.setText(confm_text);


        rg_language.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                                   @Override
                                                   public void onCheckedChanged(RadioGroup group, int checkedId) {
                                                       RadioButton radioButton = group.findViewById(checkedId);
                                                       language = String.valueOf(radioButton.getText());
                                                   }
                                               }
        );

        cb_all_duplicate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    sms_duplicate = "1";
                } else {
                    sms_duplicate = "";
                }
            }
        });

        cb_employee.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    sms_employee = "1";
                } else {
                    sms_employee = "";
                }
            }
        });

        List<String> sms_type_arr = new ArrayList<String>();
        for (int i = 0; i < sms_type_list.size(); i++) {
            sms_type_arr.add(sms_type_list.get(i).smsType);
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_spinner_item, sms_type_arr);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_sms_type.setAdapter(dataAdapter);

        spinner_sms_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sms_type = String.valueOf(sms_type_list.get(position).smsTypeId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        dialogView.findViewById(R.id.btn_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                callApi();
            }
        });


    }


    private void callApi() {
        final ProgressD pd = ProgressD.show(context, getString(R.string.connecting), true);
        String appBaseUrl = LoginPreferences.getActiveInstance(context).getBASE_URL();
        Log.e("URL---", appBaseUrl);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, appBaseUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.dismiss();
                        Log.e("Response--", response);
                        Gson gson = new Gson();
                        CommonResponse result = gson.fromJson(String.valueOf(response), CommonResponse.class);
                        if (result.result) {
                            CommonMethod.showFinishAlert(result.message, getActivity());
                        } else {
                            CommonMethod.showAlert(result.message, context);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                        Log.e("error--", error.toString());
                        Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {


                CommonMethod.print("classId--" + classId);

                Map<String, String> params = new HashMap<>();
                params.put("api_page", ConstantURL.SEND_SMS);
                params.put("emp_id", LoginPreferences.getActiveInstance(context).getId());
                params.put("school_id", LoginPreferences.getActiveInstance(context).getSchoolId());
                params.put("branch_id", LoginPreferences.getActiveInstance(context).getBranchId());
                params.put("session_id", LoginPreferences.getActiveInstance(context).getSessionId());
                params.put("class_id", classId);

                uploadImageList = UploadImageAdapterT.list;
                if (uploadImageList.size() != 0) {
                    Log.e("Images List ", String.valueOf(UploadImageAdapterT.list.size()));
                    params.put("files", UploadImageAdapterT.list.toString().replace("[", "").replace("]", ""));
                }

                params.put("top_text", top_text.getText().toString().trim());
                params.put("middle_text", middle_text.getText().toString().trim());
                params.put("bottom_text", bottom_text.getText().toString().trim());

                params.put("sms_type", sms_type);
                params.put("language", language);
                params.put("sms_copy", sms_duplicate);
                params.put("employee_copy", sms_employee);

                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }

}
