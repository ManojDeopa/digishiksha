package com.smart.school.masterAdmin.fragmentsMA;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.smart.school.R;
import com.smart.school.parent.responsesP.AboutSchoolResponse;
import com.smart.school.utils.AppData;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.LoginPreferences;
import com.smart.school.utils.ProgressD;
import com.smart.school.utils.VolleySingleton;

import java.util.Map;


/**
 * Created by Manoj Singh Deopa on 2/4/18.
 */

@SuppressLint("ValidFragment")

public class AcFeeWebview extends Fragment {

    WebView webView;
    private String type;
    private Context context;
    private View view;

    public AcFeeWebview() {
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.about_school_fragment, container, false);
        context = getActivity();
        webView = view.findViewById(R.id.text);
        callApi();
        return view;

    }


    private void callApi() {
        String appBaseUrl;
        appBaseUrl = LoginPreferences.getActiveInstance(context).getBASE_URL();
        CommonMethod.print("AboutSchoolFragP---" + appBaseUrl);
        final ProgressD pd = ProgressD.show(context, context.getResources().getString(R.string.connecting), true);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, appBaseUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.dismiss();
                        Log.e("Response--", response);

                        Gson gson = new Gson();
                        AboutSchoolResponse result = gson.fromJson(String.valueOf(response), AboutSchoolResponse.class);

                        if (result.result) {
                            webView.loadData(result.data, "webView/html", "UTF-8");
                            webView.clearCache(true);
                            webView.clearHistory();
                            webView.getSettings().setJavaScriptEnabled(true);
                            webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                        Log.e("error--", error.toString());
                        Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Log.e("Data---",AppData.getInstance().getFeeParams().toString());
                return AppData.getInstance().getFeeParams();
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }


}
