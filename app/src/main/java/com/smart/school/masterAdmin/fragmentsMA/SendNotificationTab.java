package com.smart.school.masterAdmin.fragmentsMA;

/**
 * Created by manoj on 26/1/19.
 */


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smart.school.R;

import java.util.ArrayList;
import java.util.List;


public class SendNotificationTab extends Fragment implements ViewPager.OnPageChangeListener, TabLayout.OnTabSelectedListener {
    View view;
    Context context;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    public SendNotificationTab() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.bottom_tab_layout, container, false);
        context = getActivity();

        setUpView();

        return view;
    }


    private void setUpView() {
        viewPager = view.findViewById(R.id.pager);
        createViewPager(viewPager);
        viewPager.addOnPageChangeListener(this);

        tabLayout = view.findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
        createTabIcons();
        tabLayout.getTabAt(0).getCustomView().setBackgroundColor(getResources().getColor(R.color.back_color));
        tabLayout.addOnTabSelectedListener(this);
    }


    private void createTabIcons() {

        TextView tabOne = (TextView) LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        tabOne.setText("Create");
        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_add_black, 0, 0);
        tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        tabTwo.setText("Outgoing");
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_outgoing, 0, 0);
        tabLayout.getTabAt(1).setCustomView(tabTwo);

        TextView tabThree = (TextView) LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        tabThree.setText("Incoming");
        tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_incoming, 0, 0);
        tabLayout.getTabAt(2).setCustomView(tabThree);
    }


    private void createViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        adapter.addFrag(new CreateNotification(), "Create");
        adapter.addFrag(new OutgoingNotification(), "Outgoing");
        adapter.addFrag(new OutgoingNotification(), "Incoming");
        viewPager.setAdapter(adapter);
    }


    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {
        tabLayout.getTabAt(i).select();
    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
        tab.getCustomView().setBackgroundColor(getResources().getColor(R.color.back_color));
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        tab.getCustomView().setBackgroundColor(getResources().getColor(R.color.white));

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


}

