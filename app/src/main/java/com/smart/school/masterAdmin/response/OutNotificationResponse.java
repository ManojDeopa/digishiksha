package com.smart.school.masterAdmin.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by manoj on 25/5/19.
 */
public class OutNotificationResponse {

    @SerializedName("result")
    @Expose
    public Boolean result;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;


    public class Datum {

        @SerializedName("outgoing_sms_id")
        @Expose
        public String outgoingSmsId;
        @SerializedName("receiver")
        @Expose
        public String receiver;
        @SerializedName("mobile_no")
        @Expose
        public String mobileNo;
        @SerializedName("outgoing_sms")
        @Expose
        public String outgoingSms;
        @SerializedName("sms_delivery")
        @Expose
        public String smsDelivery;
        @SerializedName("created_at")
        @Expose
        public String createdAt;
        @SerializedName("files")
        @Expose
        public List<File> files = null;

        public class File {

            @SerializedName("file_name")
            @Expose
            public String fileName;
            @SerializedName("file_path")
            @Expose
            public String filePath;

        }

    }

}
