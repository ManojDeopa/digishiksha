package com.smart.school.masterAdmin.fragmentsMA;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.smart.school.R;
import com.smart.school.masterAdmin.activitiesMA.MADetails;
import com.smart.school.masterAdmin.adaptersMA.CheckStudentAdapter;
import com.smart.school.teacher.activityT.QuickActionDetails;
import com.smart.school.teacher.responsesT.LoginResponse;
import com.smart.school.utils.ClassListDatabase;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.Constants;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Manoj Singh Deopa on 2/4/18.
 */

@SuppressLint("ValidFragment")

public class CreateNotification extends Fragment {

    public static List<LoginResponse.ClassVal> classlist;
    CheckBox chk_all;
    private RecyclerView recyclerView;
    private Context context;
    private View view;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.create_notification, container, false);
        context = getActivity();
        chk_all = view.findViewById(R.id.chk_all);

        fetchClassData();

        chk_all.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                recyclerView.setAdapter(new CheckStudentAdapter(b, classlist));
            }
        });


        view.findViewById(R.id.next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean isSelected = false;
                for (int i = 0; i < classlist.size(); i++) {

                    if (classlist.get(i).isChecked) {
                        isSelected = true;
                        Log.e("Selected--", classlist.get(i).className);
                    }
                }

                if (isSelected)
                    startActivity(new Intent(context, MADetails.class).putExtra(Constants.TYPE, Constants.TYPE_SEND_NOTIFICATION2));
                else
                    CommonMethod.showAlert("Please Select Class", context);
            }
        });

        return view;
    }

    private void setList() {
        recyclerView = view.findViewById(R.id.recycler_view);
        CommonMethod.setRecyclerView(recyclerView, context);
        recyclerView.setAdapter(new CheckStudentAdapter(false, classlist));
    }


    private void fetchClassData() {

        ClassListDatabase mDatabase = new ClassListDatabase(context);
        classlist = new ArrayList<>();
        Cursor cursor = mDatabase.getAllData();
        if (cursor.moveToFirst()) {
            do {

                classlist.add(new LoginResponse.ClassVal(
                        cursor.getString(1),
                        cursor.getString(2)));
            } while (cursor.moveToNext());
        }

        setList();
    }
}
