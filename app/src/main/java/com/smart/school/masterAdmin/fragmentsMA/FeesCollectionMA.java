package com.smart.school.masterAdmin.fragmentsMA;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.masterAdmin.activitiesMA.MADetails;
import com.smart.school.utils.AppData;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.ConstantURL;
import com.smart.school.utils.Constants;
import com.smart.school.utils.LoginPreferences;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by Manoj Singh Deopa on 2/4/18.
 */

@SuppressLint("ValidFragment")

public class FeesCollectionMA extends Fragment {

    TextView start_date, end_date;
    private String page_id;
    private Context context;
    private View view;

    public FeesCollectionMA(String type) {
        switch (type) {
            case Constants.TYPE_FEES_COLLECTION:
                page_id = "1";
                break;
            case Constants.FEE_DISCOUNT_SUMMARY:
                page_id = "4";
                break;
            case Constants.EXPENSE_SUMMARY:
                page_id = "5";
                break;
            case Constants.ACCOUNT_BALANCE:
                page_id = "6";
                break;
        }

    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fees_collection, container, false);
        start_date = view.findViewById(R.id.start_date);
        end_date = view.findViewById(R.id.end_date);

        context = getActivity();

        setCurrentDate();

        onClicks();

        return view;
    }

    private void onClicks() {
        start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethod.showDatePicker(context, start_date);
            }
        });

        end_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethod.showDatePicker(context, end_date);
            }
        });

        view.findViewById(R.id.btn_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Map<String, String> params = new HashMap<>();
                params.put("api_page", ConstantURL.ACCOUNT_FEES);
                params.put("page_id", page_id);
                params.put("emp_id", LoginPreferences.getActiveInstance(context).getId());
                params.put("school_id", LoginPreferences.getActiveInstance(context).getSchoolId());
                params.put("branch_id", LoginPreferences.getActiveInstance(context).getBranchId());
                params.put("session_id", LoginPreferences.getActiveInstance(context).getSessionId());
                params.put("start_date", start_date.getText().toString());
                params.put("end_date", end_date.getText().toString());
                AppData.getInstance().setFeeParams(params);
                startActivity(new Intent(context, MADetails.class).putExtra(Constants.TYPE, Constants.DETAIL));

            }
        });


    }


    private void setCurrentDate() {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(c);
        start_date.setText(formattedDate);
        end_date.setText(formattedDate);
    }


}
