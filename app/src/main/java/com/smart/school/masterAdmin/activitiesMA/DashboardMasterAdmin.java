package com.smart.school.masterAdmin.activitiesMA;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.smart.school.R;
import com.smart.school.masterAdmin.fragmentsMA.HomeFragMA;
import com.smart.school.teacher.activityT.LoginActivity;
import com.smart.school.teacher.activityT.QuickActionDetails;
import com.smart.school.teacher.adaptersT.ExpandableListAdapterT;
import com.smart.school.teacher.interfacesT.DashboardBottomItemClickListener;
import com.smart.school.teacher.responsesT.MenuModel;
import com.smart.school.utils.AppUtils;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.Constants;
import com.smart.school.utils.LoginPreferences;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DashboardMasterAdmin extends AppCompatActivity implements DashboardBottomItemClickListener {

    private static final int TIME_INTERVAL = 2000;
    boolean flag = false;
    ExpandableListAdapterT expandableListAdapterT;
    ExpandableListView expandableListView;
    List<MenuModel> headerList = new ArrayList<>();
    HashMap<MenuModel, List<MenuModel>> childList = new HashMap<>();
    private Toolbar toolbar;
    private DashboardMasterAdmin context;
    private long mBackPressed;
    private TextView tv_title;
    private DrawerLayout drawer;
    private int lastExpandedPosition = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        initializeViews();

        setDrawerLayout();

        _callHomeFragment();

    }

    private void initializeViews() {
        context = DashboardMasterAdmin.this;
        toolbar = findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        TextView app_version = findViewById(R.id.app_version);
        CommonMethod.setVersionName(app_version);
        setSupportActionBar(toolbar);
        findViewById(R.id.logout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
            }
        });
    }

    private void setDrawerLayout() {

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.white));

        NavigationView navigationView = findViewById(R.id.nav_view);

        LinearLayout header_layout = navigationView.getHeaderView(0).findViewById(R.id.header_layout);
        header_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // callProfile();
            }
        });


        TextView user_name = navigationView.getHeaderView(0).findViewById(R.id.user_name);
        ImageView user_image = navigationView.getHeaderView(0).findViewById(R.id.user_image);


        user_name.setText(LoginPreferences.getActiveInstance(context).getFullname());


        String BaseUrl = LoginPreferences.getActiveInstance(context).getImgBaseUrl();
        String userImage = BaseUrl + LoginPreferences.getActiveInstance(context).getUserImage();


        Log.e("UserImage---", userImage);


        Picasso.get().load(userImage).placeholder(R.drawable.shool_logo).into(user_image);


        expandableListView = findViewById(R.id.expandableListView);
        prepareMenuData();
        populateExpandableList();

    }


    private void prepareMenuData() {

        MenuModel menuModel = new MenuModel(Constants.HOME, true, false);
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }


        ArrayList<MenuModel> childModelsList;
        MenuModel childModel;

        menuModel = new MenuModel(Constants.QUICK_ACTION, true, true);
        headerList.add(menuModel);

        childModelsList = new ArrayList<>();

        childModel = new MenuModel(Constants.TYPE_ADD_STUDENT, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.TYPE_STUDENT_LIST, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.TYPE_MARK_ATTENDENCE, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.TYPE_UPLOAD_HOMEWORK, false, false);
        childModelsList.add(childModel);


        childModel = new MenuModel(Constants.UPLOAD_STUDENT_PHOTO, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.TYPE_LEAVE_APPROVALS, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.TYPE_SEND_NOTIFICATION, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.TYPE_FEES_COLLECTION, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.STUDENT_FEE_DEFAULTER, false, false);
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }


        menuModel = new MenuModel(Constants.COMMUNICATION, true, true);
        headerList.add(menuModel);

        childModelsList = new ArrayList<>();

        childModel = new MenuModel(Constants.TYPE_SEND_NOTIFICATION, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.UPLOAD_NOTICE, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.UPLOAD_ASSIGNMENT, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.UPLOAD_FEED, false, false);
        childModelsList.add(childModel);


        childModel = new MenuModel(Constants.UPLOAD_ACTIVITY, false, false);
        childModelsList.add(childModel);


        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }

        menuModel = new MenuModel(Constants.ACC_FEE_COLLECTION, true, true);
        headerList.add(menuModel);

        childModelsList = new ArrayList<>();

        childModel = new MenuModel(Constants.TYPE_FEES_COLLECTION, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.DAY_BOOK, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.STUDENT_PAID_SUMMARY, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.FEE_DISCOUNT_SUMMARY, false, false);
        childModelsList.add(childModel);


        childModel = new MenuModel(Constants.EXPENSE_SUMMARY, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.ACCOUNT_BALANCE, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.STUDENT_FEE_DEFAULTER, false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.GUARDIAN_WISE_FEE_DEFAULTER, false, false);
        childModelsList.add(childModel);


        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }


        menuModel = new MenuModel(Constants.SHARE, true, false);
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }


        menuModel = new MenuModel(Constants.PRIVACY_POLICY, true, false);
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }

        menuModel = new MenuModel(Constants.ABOUT, true, false);
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }

        menuModel = new MenuModel(Constants.APP_TUTORIAL, true, false);
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }

        menuModel = new MenuModel(Constants.LOG_OUT, true, false);
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }

    }


    private void populateExpandableList() {

        expandableListAdapterT = new ExpandableListAdapterT(this, headerList, childList);
        expandableListView.setAdapter(expandableListAdapterT);

        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                if (headerList.get(groupPosition).isGroup) {

                    if (!headerList.get(groupPosition).hasChildren) {
                        switch (headerList.get(groupPosition).menuName) {
                            case Constants.HOME:
                                _callHomeFragment();
                                break;
                            case Constants.LOG_OUT:
                                showLogoutDialog();
                                break;
                            case Constants.ABOUT:
                                CommonMethod.callBrowserIntent(context, Constants.DIGI_SHIKSHA_URL);
                                break;
                            case Constants.PRIVACY_POLICY:
                                CommonMethod.callBrowserIntent(context, Constants.PRIVACY_POLICY_URL);
                                break;
                            case Constants.SHARE:
                                CommonMethod._shareIntent(context);
                                break;
                        }

                        drawer.closeDrawer(GravityCompat.START);
                    }
                }

                return false;
            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                if (childList.get(headerList.get(groupPosition)) != null) {
                    MenuModel model = childList.get(headerList.get(groupPosition)).get(childPosition);
                    String type = model.menuName;
                    switch (headerList.get(groupPosition).menuName) {
                        case Constants.QUICK_ACTION:
                            startActivity(new Intent(context, QuickActionDetails.class).putExtra(Constants.TYPE, type));
                            break;

                        case Constants.COMMUNICATION:
                            startActivity(new Intent(context, MADetails.class).putExtra(Constants.TYPE, type));
                            break;

                        case Constants.ACC_FEE_COLLECTION:
                            startActivity(new Intent(context, MADetails.class).putExtra(Constants.TYPE, type));
                            break;

                    }
                }

                return false;
            }
        });

        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                if (lastExpandedPosition != -1
                        && groupPosition != lastExpandedPosition) {
                    expandableListView.collapseGroup(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;
            }
        });

    }

    private void _callHomeFragment() {
        tv_title.setText("Home");
        HomeFragMA homeFragMA = new HomeFragMA();
        AppUtils.setFragment(homeFragMA, true, DashboardMasterAdmin.this, R.id.container);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0)
                getSupportFragmentManager().popBackStackImmediate();
            else if (flag) {
                flag = false;
                _callHomeFragment();
            } else {
                if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
                    moveTaskToBack(true);
                    return;
                } else {
                    Toast.makeText(getBaseContext(), "Press Again to Exit", Toast.LENGTH_SHORT).show();
                }
                mBackPressed = System.currentTimeMillis();
            }
        }
    }


    @Override
    public void bottomItemClicked(int position) {
        flag = true;
    }

    public void logout() {
        showLogoutDialog();
    }


    private void showLogoutDialog() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

        alertDialog.setTitle(getString(R.string.sure_to_logout));

        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finishAffinity();
                LoginPreferences.getActiveInstance(context).logOut();
                Intent intent = new Intent(context, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(intent);
            }
        });

        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alertDialog.show();
    }
}
