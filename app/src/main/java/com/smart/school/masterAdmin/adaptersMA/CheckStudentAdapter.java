package com.smart.school.masterAdmin.adaptersMA;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.smart.school.R;
import com.smart.school.masterAdmin.fragmentsMA.CreateNotification;
import com.smart.school.teacher.responsesT.LoginResponse;

import java.util.List;

/**
 * Created by manoj on 11/5/19.
 */
public class CheckStudentAdapter extends RecyclerView.Adapter<CheckStudentAdapter.ViewHolder> {
    public static boolean isCheckAll;
    List<LoginResponse.ClassVal> classList;
    private Context context;

    public CheckStudentAdapter(boolean ischeckAll, List<LoginResponse.ClassVal> classlist) {
        isCheckAll = ischeckAll;
        classList = classlist;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.check_student_adapter, parent, false);
        context = view.getContext();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        if (isCheckAll) {
            holder.checkbox.setChecked(true);
            CreateNotification.classlist.get(i).isChecked = true;
        } else {
            CreateNotification.classlist.get(i).isChecked = false;
        }

        holder.class_name.setText(classList.get(i).className);
    }

    @Override
    public int getItemCount() {
        return classList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CheckBox checkbox;
        TextView class_name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            checkbox = itemView.findViewById(R.id.checkbox);
            class_name = itemView.findViewById(R.id.class_name);

            checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    CreateNotification.classlist.get(getAdapterPosition()).isChecked = b;
                }
            });
        }
    }
}
