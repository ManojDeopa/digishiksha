package com.smart.school.masterAdmin.fragmentsMA;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.smart.school.R;
import com.smart.school.masterAdmin.adaptersMA.OutInNotificationAdapter;
import com.smart.school.masterAdmin.response.OutNotificationResponse;
import com.smart.school.parent.adaptersP.NoticeAdapter;
import com.smart.school.parent.responsesP.NoticeResponse;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.ConstantURL;
import com.smart.school.utils.LoginPreferences;
import com.smart.school.utils.ProgressD;
import com.smart.school.utils.VolleySingleton;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Manoj Singh Deopa on 2/4/18.
 */

@SuppressLint("ValidFragment")

public class OutgoingNotification extends Fragment {

    private RecyclerView recyclerView;
    private Context context;
    private View view;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.common_recycler_view, container, false);
        context = getActivity();

        getData();

        return view;
    }


    private void getData() {
        String appBaseUrl = LoginPreferences.getActiveInstance(context).getBASE_URL();
        Log.e("URL---", appBaseUrl);
        final ProgressD pd = ProgressD.show(context, context.getResources().getString(R.string.connecting), true);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, appBaseUrl,
                new Response.Listener<String>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(String response) {
                        pd.dismiss();
                        Log.e("Response--", response);
                        Gson gson = new Gson();
                        final OutNotificationResponse result = gson.fromJson(String.valueOf(response), OutNotificationResponse.class);
                        if (result.result) {
                            setList(result.data);
                        } else {
                            CommonMethod.showAlert(result.message, context);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                        Log.e("error--", error.toString());
                        Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                Log.e("EMP ID--", LoginPreferences.getActiveInstance(context).getId());
                Log.e("school ID--", LoginPreferences.getActiveInstance(context).getSchoolId());
                Log.e("branch ID--", LoginPreferences.getActiveInstance(context).getBranchId());

                Map<String, String> params = new HashMap<>();
                params.put("api_page", ConstantURL.SMS_OUT_GOING);
                params.put("emp_id", LoginPreferences.getActiveInstance(context).getId());
                params.put("school_id", LoginPreferences.getActiveInstance(context).getSchoolId());
                params.put("branch_id", LoginPreferences.getActiveInstance(context).getBranchId());
                params.put("session_id", LoginPreferences.getActiveInstance(context).getSessionId());

                return params;
            }
        };

        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);

    }


    private void setList(List<OutNotificationResponse.Datum> data) {

        if (data.size() == 0) {
            LinearLayout linearLayout = view.findViewById(R.id.empty_layout);
            CommonMethod.showEmpty(linearLayout);
            return;
        }

        recyclerView = view.findViewById(R.id.recycler_view);
        CommonMethod.setRecyclerView(recyclerView, context);
        recyclerView.setAdapter(new OutInNotificationAdapter(data));
    }

}
