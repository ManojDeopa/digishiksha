package com.smart.school;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.smart.school.geteKeeper.DashboardGk;
import com.smart.school.masterAdmin.activitiesMA.DashboardMasterAdmin;
import com.smart.school.parent.activityP.ChildAccounts;
import com.smart.school.parent.activityP.ParentDashboard;
import com.smart.school.teacher.activityT.DashboardActivity;
import com.smart.school.teacher.activityT.LoginActivity;
import com.smart.school.utils.CommonMethod;
import com.smart.school.utils.ConstantURL;
import com.smart.school.utils.Constants;
import com.smart.school.utils.LoginPreferences;
import com.smart.school.utils.VolleySingleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.os.Build.VERSION_CODES.M;


public class SplashActivity extends AppCompatActivity {


    private static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getSupportActionBar().hide();

        animate();

        if (Build.VERSION.SDK_INT >= M) {
            if (checkAndRequestPermissions()) {
                startApp();

            }
        } else {
            startApp();
        }

    }

    private void animate() {
        ImageView bottom_logo = findViewById(R.id.bottom_logo);
        ImageView logo = findViewById(R.id.logo);
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.bounce);
        Animation animation2 = AnimationUtils.loadAnimation(this, R.anim.slide_up_in);
        logo.startAnimation(animation);
        bottom_logo.startAnimation(animation2);
    }

    private boolean checkAndRequestPermissions() {
        int CAMERA = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int WRITE_EXTERNAL_STORAGE = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int READ_EXTERNAL_STORAGE = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int ACCESS_FINE_LOCATION = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int CALL_PHONE = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);

        List<String> listPermissionsNeeded = new ArrayList<>();

        if (CAMERA != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (WRITE_EXTERNAL_STORAGE != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (READ_EXTERNAL_STORAGE != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (ACCESS_FINE_LOCATION != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (CALL_PHONE != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CALL_PHONE);
        }


        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        Log.d("msg", "Permission callback called-------");
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions

                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CALL_PHONE, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions

                    if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED
                    ) {
                        Log.e("msg", "All Permissions granted");
                        startApp();
                    } else {
                        Log.e("msg", "Some permissions are not granted ask again ");
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)
                                && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                                && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)
                                && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CALL_PHONE)
                        ) {
                            showDialogOK(
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkAndRequestPermissions();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    // proceed with logic by disabling the related features or quit the app.
                                                    break;
                                            }
                                        }
                                    });
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            finish();
                            Toast.makeText(this, "Please Enable All Permissions !", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent();
                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                            intent.setData(uri);
                            startActivity(intent);

                        }
                    }
                }
            }
        }

    }

    private void showDialogOK(DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage("Permissions Required For This App")
                .setPositiveButton("Ok", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }


    private void checkVersion() {
        /*final ProgressD pd = ProgressD.show(this, getResources().getString(R.string.connecting), true);*/
        StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantURL.CHECK_VERSION_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // pd.dismiss();

                        Log.e("Response--", response);

                        Gson gson = new Gson();
                        AppVersionResponse result = gson.fromJson(String.valueOf(response), AppVersionResponse.class);

                        if (result.result) {

                            int versionCode = BuildConfig.VERSION_CODE;
                            String versionName = BuildConfig.VERSION_NAME;
                            Log.e("Current Version Code--", String.valueOf(versionCode));
                            Log.e("App Version Code--", String.valueOf(result.data.versionCode));

                            if (versionCode < result.data.versionCode) {
                                showAlert(result.data.text);
                            } else {
                                callTimer();
                            }

                        } else {
                            finish();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // pd.dismiss();
                        Log.e("error--", error.toString());
                    }
                });

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);

    }

    private void showAlert(String mtext) {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.app_version_dialog, viewGroup, false);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.setCancelable(false);

        TextView text = dialogView.findViewById(R.id.text);
        text.setText(mtext);


        dialogView.findViewById(R.id.skip).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                callTimer();
            }
        });

        dialogView.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                finish();
                CommonMethod.callBrowserIntent(SplashActivity.this, ConstantURL.PLAY_STORE_APP_URL);
            }
        });


    }


    private void startApp() {
        if (CommonMethod.isOnline(SplashActivity.this)) {
            checkVersion();
        }

    }

    private void callTimer() {
        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {

                    checkSession();

                }
            }
        };
        timer.start();
    }

    private void checkSession() {
        switch (LoginPreferences.getActiveInstance(SplashActivity.this).getType()) {
            case Constants.TYPE_TEACHER:
                if (LoginPreferences.getActiveInstance(SplashActivity.this).getIsLoggedIn()) {
                    CommonMethod.startIntent(this, DashboardActivity.class);
                } else {
                    CommonMethod.startIntent(this, LoginActivity.class);
                }
                break;

            case Constants.TYPE_MASTER_ADMIN:
                if (LoginPreferences.getActiveInstance(SplashActivity.this).getIsLoggedIn()) {
                    CommonMethod.startIntent(this, DashboardMasterAdmin.class);
                } else {
                    CommonMethod.startIntent(this, LoginActivity.class);
                }
                break;

            case Constants.TYPE_GATEKEEPER:
                if (LoginPreferences.getActiveInstance(SplashActivity.this).getIsLoggedIn()) {
                    CommonMethod.startIntent(this, DashboardGk.class);
                } else {
                    CommonMethod.startIntent(this, LoginActivity.class);
                }
                break;
            case Constants.TYPE_PARENT:
                if (LoginPreferences.getActiveInstance(SplashActivity.this).getIsLoggedIn()) {
                    CommonMethod.startIntent(this, ParentDashboard.class);
                } else {
                    CommonMethod.startIntent(this, ChildAccounts.class);
                }
                break;
            default:
                CommonMethod.startIntent(this, VerifySchoolActivity.class);
                break;
        }
    }

    @Override
    protected void onStop() {
        finish();
        super.onStop();
    }

}
